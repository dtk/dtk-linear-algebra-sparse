// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkVectorTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testFactoryCreation(void);
    void testResize(void);
    void testSetAtAt(void);
    void testFill(void);
    void testOperatorGet(void);
    void testOperatorPlus(void);

    void testFactoryCreation_data(void);
    void testResize_data(void);
    void testSetAtAt_data(void);
    void testFill_data(void);
    void testOperatorGet_data(void);
    void testOperatorPlus_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


// 
// dtkVectorTest.h ends here
