// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkVectorTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

void dtkVectorTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkVectorTestCase::init(void)
{
}

void dtkVectorTestCase::testFactoryCreation_data(void) { setVectorImplementation(); }
void dtkVectorTestCase::testResize_data(void) { setVectorImplementation(); }
void dtkVectorTestCase::testSetAtAt_data(void) { setVectorImplementation(); }
void dtkVectorTestCase::testFill_data(void) { setVectorImplementation(); }
void dtkVectorTestCase::testOperatorGet_data(void) { setVectorImplementation(); }
void dtkVectorTestCase::testOperatorPlus_data(void) { setVectorImplementation(); }

void dtkVectorTestCase::testFactoryCreation(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    QVERIFY(vec);
    QVERIFY(vec->empty());
    QCOMPARE(vec->size(), 0LL);
    
    delete vec;
}

void dtkVectorTestCase::testResize(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    vec->resize(5);
    QVERIFY(!vec->empty());
    QCOMPARE(vec->size(), 5LL);

    vec->resize(101);
    QVERIFY(!vec->empty());
    QCOMPARE(vec->size(), 101LL);
    
    delete vec;
}

void dtkVectorTestCase::testSetAtAt(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    vec->resize(101);

    for(int i = 0; i < 101; ++i) {
        vec->setAt(i, i * 3.14159);
    }

    for(int i = 0; i < 101; ++i) {
        QCOMPARE(vec->at(i), i * 3.14159);
    }

    QCOMPARE(vec->first(), 0.);
    QCOMPARE(vec->last(), 100 * 3.14159);

    delete vec;
}

void dtkVectorTestCase::testFill(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    vec->resize(101);

    vec->fill(3.14159);
    for(int i = 0; i < 101; ++i) {
        QCOMPARE(vec->at(i), 3.14159);
    }

    delete vec;
}

void dtkVectorTestCase::testOperatorGet(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    vec->resize(101);

    for(int i = 0; i < 101; ++i) {
        vec->setAt(i, i * 3.14159);
    }

    dtkVectorProxy<double> proxy = (*vec)[0];
    proxy = (*vec)[57];
    
    delete vec;
}

void dtkVectorTestCase::testOperatorPlus(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
    dtkVector<double> *vec2 = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    vec->resize(4);
    vec->fill(1.1);

    vec2->resize(4);
    vec2->fill(1);

    *vec2 += *vec;

    QCOMPARE(vec2->at(0), 2.1);
    QCOMPARE(vec->asum(), 4.4);

    *vec2 -= *vec;
    QCOMPARE(vec2->at(0), 1.0);
    QCOMPARE(vec2->at(1), 1.0);
    QCOMPARE(vec2->at(2), 1.0);
    QCOMPARE(vec2->at(3), 1.0);

    delete vec;
    delete vec2;
}

void dtkVectorTestCase::cleanupTestCase(void)
{

}

void dtkVectorTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkVectorTest, dtkVectorTestCase)

// 
// dtkVectorTest.cpp ends here
