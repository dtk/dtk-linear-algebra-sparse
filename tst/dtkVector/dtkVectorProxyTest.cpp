// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include <dtkLinearAlgebraSparse>

#include "dtkVectorProxyTest.h"

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class dtkVectorProxyTestCasePrivate
{
public:
    dtkVector<double> *createVector(void);
};

// ///////////////////////////////////////////////////////////////////

dtkVector<double> *dtkVectorProxyTestCasePrivate::createVector(void)
{
    QFETCH(QString, vectorImpl);
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    vec->resize(8);

    vec->setAt(0,  1.000e+00);
    vec->setAt(1,  1.050e+01);
    vec->setAt(2,  1.500e-02);
    vec->setAt(3,  2.505e+02);
    vec->setAt(4,  6.000e+00);
    vec->setAt(5, -2.800e+02);
    vec->setAt(6,  3.332e+01);
    vec->setAt(7,  1.200e+01);

    return vec;
}

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

dtkVectorProxyTestCase::dtkVectorProxyTestCase(void) : d(new dtkVectorProxyTestCasePrivate)
{
}

dtkVectorProxyTestCase::~dtkVectorProxyTestCase(void)
{    
    if (d) {
        delete d;
    }
}

void dtkVectorProxyTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkVectorProxyTestCase::init(void)
{
}

void dtkVectorProxyTestCase::testCreation_data(void) { setVectorImplementation();}

void dtkVectorProxyTestCase::testAssign_data(void) { setVectorImplementation();}

void dtkVectorProxyTestCase::testConversion_data(void) { setVectorImplementation();}

void dtkVectorProxyTestCase::testOperations_data(void) { setVectorImplementation();}

void dtkVectorProxyTestCase::testComparison_data(void) { setVectorImplementation();}

void dtkVectorProxyTestCase::testValue_data(void) { setVectorImplementation();}

void dtkVectorProxyTestCase::testCreation(void)
{
    dtkVector<double>& vec = *(d->createVector());

    dtkVectorProxy<double> proxy = vec[3];

    delete &vec;
}

void dtkVectorProxyTestCase::testValue(void)
{
    dtkVector<double>& vec = *(d->createVector());

    dtkVectorProxy<double> proxy = vec[3];
    QCOMPARE(proxy.value(), vec.at(3));

    delete &vec;
}

void dtkVectorProxyTestCase::testAssign(void)
{
    dtkVector<double>& vec = *(d->createVector());

    dtkVectorProxy<double> proxy = vec[3];

    proxy = vec[4];
    QCOMPARE(proxy.value(), vec.at(4));
    
    delete &vec;
}

void dtkVectorProxyTestCase::testConversion(void)
{
    dtkVector<double>& vec = *(d->createVector());

    dtkVectorProxy<double> proxy = vec[3];

    double p_val = proxy;

    QCOMPARE(p_val, vec.at(3));
    
    delete &vec;
}

void dtkVectorProxyTestCase::testOperations(void)
{
    dtkVector<double>& vec = *(d->createVector());
    dtkVectorProxy<double> p = vec[3];
    
    p = vec.at(4);
    QCOMPARE(vec.at(3), vec.at(4));
    
    double val = vec.at(3);
    val += 3.14159;
    p += 3.14159;
    QCOMPARE(p.value(), val);

    val -= 3.14159;
    p -= 3.14159;
    QCOMPARE(p.value(), val);

    val *= 3.14159;
    p *= 3.14159;
    QCOMPARE(p.value(), val);

    val /= 3.14159;
    p /= 3.14159;
    QCOMPARE(p.value(), val);
    
    delete &vec;
}

void dtkVectorProxyTestCase::testComparison(void)
{  
    dtkVector<double>& vec = *(d->createVector());
    dtkVectorProxy<double> p0 = vec[3];
    dtkVectorProxy<double> p1 = vec[4];

    double val = p1;
    p1 = p0; 
    QVERIFY( p1 == p0 );
    p1 = val;

    val = p0;
    QVERIFY( val == p0 );
    QVERIFY( p0 == val );

    QVERIFY( p1 != p0 );
    val = p1;
    QVERIFY( val != p0 );
    QVERIFY( p0 != val );

    QVERIFY( p1 < p0 );
    val = p1;
    QVERIFY( val < p0 );
    val = p0;
    QVERIFY( p1 < val );

    QVERIFY( p0 > p1 );
    val = p1;
    QVERIFY( p0 > val );
    val = p0;
    QVERIFY( val > p1 );

    QVERIFY( p1 <= p0 );
    val = p1;
    QVERIFY( val <= p0 );
    val = p0;
    QVERIFY( p1 <= val );

    QVERIFY( p0 >= p1 );
    val = p1;
    QVERIFY( p0 >= val );
    val = p0;
    QVERIFY( val >= p1 );

    delete &vec;
}


void dtkVectorProxyTestCase::cleanupTestCase(void)
{    
}

void dtkVectorProxyTestCase::cleanup(void)
{
}

DTKTEST_MAIN_NOGUI(dtkVectorProxyTest, dtkVectorProxyTestCase)

// 
// dtkVectorProxyTest.cpp ends here
