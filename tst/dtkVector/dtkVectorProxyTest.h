// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkVectorProxyTestCasePrivate;

class dtkVectorProxyTestCase : public QObject
{
    Q_OBJECT

public:
     dtkVectorProxyTestCase(void);
    ~dtkVectorProxyTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreation(void);
    void testValue(void);
    void testAssign(void);
    void testConversion(void);
    void testOperations(void);
    void testComparison(void);

    void testCreation_data(void);
    void testValue_data(void);
    void testAssign_data(void);
    void testConversion_data(void);
    void testOperations_data(void);
    void testComparison_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);

private:
    dtkVectorProxyTestCasePrivate *d;
};

// 
// dtkVectorProxyTest.h ends here
