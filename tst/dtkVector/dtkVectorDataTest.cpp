// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkVectorDataTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

void dtkVectorDataTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkVectorDataTestCase::init(void)
{
}

void dtkVectorDataTestCase::testFactoryCreation_data(void) { setVectorImplementation();}

void dtkVectorDataTestCase::testFactoryCreation(void)
{

    QFETCH(QString, vectorImpl);

    dtkVectorAbstractData<double> *v_d = dtkLinearAlgebraSparse::vectorData::pluginFactory<double>().create(vectorImpl);

    QVERIFY(v_d);
    QVERIFY(v_d->empty());
    QCOMPARE(v_d->size(), 0LL);

    delete v_d;
}

void dtkVectorDataTestCase::testResize_data(void) { setVectorImplementation();}

void dtkVectorDataTestCase::testResize(void)
{
    QFETCH(QString, vectorImpl);
    dtkVectorAbstractData<double> *v_d = dtkLinearAlgebraSparse::vectorData::pluginFactory<double>().create(vectorImpl);

    v_d->resize(5);
    QVERIFY(!v_d->empty());
    QCOMPARE(v_d->size(), 5LL);

    v_d->resize(101);
    QVERIFY(!v_d->empty());
    QCOMPARE(v_d->size(), 101LL);

    delete v_d;
}

void dtkVectorDataTestCase::testSetAtAt_data(void) { setVectorImplementation(); }

void dtkVectorDataTestCase::testSetAtAt(void)
{
    QFETCH(QString, vectorImpl);
    dtkVectorAbstractData<double> *v_d = dtkLinearAlgebraSparse::vectorData::pluginFactory<double>().create(vectorImpl);

    v_d->resize(101);

    for(int i = 0; i < 101; ++i) {
        v_d->setAt(i, i * 3.14159);
    }

    for(int i = 0; i < 101; ++i) {
        QCOMPARE(v_d->at(i), i * 3.14159);
    }

    delete v_d;
}

void dtkVectorDataTestCase::testFill_data(void) { setVectorImplementation(); }

void dtkVectorDataTestCase::testFill(void)
{
    QFETCH(QString, vectorImpl);
    dtkVectorAbstractData<double> *v_d = dtkLinearAlgebraSparse::vectorData::pluginFactory<double>().create(vectorImpl);

    v_d->resize(101);

    v_d->fill(3.14159);
    for(int i = 0; i < 101; ++i) {
        QCOMPARE(v_d->at(i), 3.14159);
    }

    delete v_d;
}

void dtkVectorDataTestCase::cleanupTestCase(void)
{

}

void dtkVectorDataTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkVectorDataTest, dtkVectorDataTestCase)

// 
// dtkVectorDataTest.cpp ends here
