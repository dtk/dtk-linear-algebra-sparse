// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkVectorDataTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testFactoryCreation_data(void);
    void testFactoryCreation(void);
    void testResize_data(void);
    void testResize(void);
    void testSetAtAt_data(void);
    void testSetAtAt(void);
    void testFill_data(void);
    void testFill(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);

};


// 
// dtkVectorDataTest.h ends here
