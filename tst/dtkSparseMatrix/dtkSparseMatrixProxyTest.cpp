// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include <dtkLinearAlgebraSparse>

#include "dtkSparseMatrixProxyTest.h"
#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixProxyTestCasePrivate
{
public:
    dtkSparseMatrix<double> *createSparseMatrix(void);
};

// ///////////////////////////////////////////////////////////////////

dtkSparseMatrix<double> *dtkSparseMatrixProxyTestCasePrivate::createSparseMatrix(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *smat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    smat->resize(5);

    smat->insert(0, 0,  1.000e+00);
    smat->insert(1, 1,  1.050e+01);
    smat->insert(2, 2,  1.500e-02);
    smat->insert(0, 3,  6.000e+00);
    smat->insert(3, 1,  2.505e+02);
    smat->insert(3, 3, -2.800e+02);
    smat->insert(3, 4,  3.332e+01);
    smat->insert(4, 4,  1.200e+01);

    smat->assemble();

    return smat;
}

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

dtkSparseMatrixProxyTestCase::dtkSparseMatrixProxyTestCase(void) : d(new dtkSparseMatrixProxyTestCasePrivate)
{
}

dtkSparseMatrixProxyTestCase::~dtkSparseMatrixProxyTestCase(void)
{    
    if (d) {
        delete d;
    }
}

void dtkSparseMatrixProxyTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkSparseMatrixProxyTestCase::init(void)
{
}

void dtkSparseMatrixProxyTestCase::testCreation_data(void) {setMatrixImplementation();}
void dtkSparseMatrixProxyTestCase::testValue_data(void) {setMatrixImplementation();}
void dtkSparseMatrixProxyTestCase::testAssign_data(void) {setMatrixImplementation();}
void dtkSparseMatrixProxyTestCase::testConversion_data(void) {setMatrixImplementation();}
void dtkSparseMatrixProxyTestCase::testOperations_data(void) {setMatrixImplementation();}
void dtkSparseMatrixProxyTestCase::testComparison_data(void) {setMatrixImplementation();}

void dtkSparseMatrixProxyTestCase::testCreation(void)
{
    dtkSparseMatrix<double>& smat = *(d->createSparseMatrix());

    dtkSparseMatrixProxy<double> proxy = smat(3, 1);
    
    delete &smat;
}

void dtkSparseMatrixProxyTestCase::testValue(void)
{
    dtkSparseMatrix<double>& smat = *(d->createSparseMatrix());

    dtkSparseMatrixProxy<double> proxy = smat(3, 1);
    QCOMPARE(proxy.value(), smat.at(3, 1));
    
    delete &smat;
}

void dtkSparseMatrixProxyTestCase::testAssign(void)
{
    dtkSparseMatrix<double>& smat = *(d->createSparseMatrix());

    dtkSparseMatrixProxy<double> proxy = smat(3, 1);

    proxy = smat(3, 3);
    QCOMPARE(proxy.value(), smat.at(3, 3));
    
    delete &smat;
}

void dtkSparseMatrixProxyTestCase::testConversion(void)
{
    dtkSparseMatrix<double>& smat = *(d->createSparseMatrix());

    dtkSparseMatrixProxy<double> proxy = smat(3, 1);

    double p_val = proxy;

    QCOMPARE(p_val, smat.at(3, 1));
    
    delete &smat;
}

void dtkSparseMatrixProxyTestCase::testOperations(void)
{
    dtkSparseMatrix<double>& smat = *(d->createSparseMatrix());
    dtkSparseMatrixProxy<double> p = smat(3, 1);

    p = smat.at(3, 3);
    QCOMPARE(smat.at(3, 1), smat.at(3, 3));

    double val = smat.at(3, 1);
    val += 3.14159;
    p += 3.14159;
    QCOMPARE(p.value(), val);

    val -= 3.14159;
    p -= 3.14159;
    QCOMPARE(p.value(), val);

    val *= 3.14159;
    p *= 3.14159;
    QCOMPARE(p.value(), val);

    val /= 3.14159;
    p /= 3.14159;
    QCOMPARE(p.value(), val);
    
    delete &smat;
}

void dtkSparseMatrixProxyTestCase::testComparison(void)
{  
    dtkSparseMatrix<double>& smat = *(d->createSparseMatrix());
    dtkSparseMatrixProxy<double> p0 = smat(3, 1);
    dtkSparseMatrixProxy<double> p1 = smat(3, 3);

    double val = p1;
    p1 = p0; 
    QVERIFY( p1 == p0 );
    p1 = val;

    val = p0;
    QVERIFY( val == p0 );
    QVERIFY( p0 == val );

    QVERIFY( p1 != p0 );
    val = p1;
    QVERIFY( val != p0 );
    QVERIFY( p0 != val );

    QVERIFY( p1 < p0 );
    val = p1;
    QVERIFY( val < p0 );
    val = p0;
    QVERIFY( p1 < val );

    QVERIFY( p0 > p1 );
    val = p1;
    QVERIFY( p0 > val );
    val = p0;
    QVERIFY( val > p1 );

    QVERIFY( p1 <= p0 );
    val = p1;
    QVERIFY( val <= p0 );
    val = p0;
    QVERIFY( p1 <= val );

    QVERIFY( p0 >= p1 );
    val = p1;
    QVERIFY( p0 >= val );
    val = p0;
    QVERIFY( val >= p1 );

    delete &smat;
}


void dtkSparseMatrixProxyTestCase::cleanupTestCase(void)
{    
}

void dtkSparseMatrixProxyTestCase::cleanup(void)
{
}

DTKTEST_MAIN_NOGUI(dtkSparseMatrixProxyTest, dtkSparseMatrixProxyTestCase)

// 
// dtkSparseMatrixProxyTest.cpp ends here
