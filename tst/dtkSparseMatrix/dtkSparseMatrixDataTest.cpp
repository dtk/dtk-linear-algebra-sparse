// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkSparseMatrixDataTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

#include <dtkDistributed>
#include <dtkDistributed/dtkDistributedPolicy>


class totoTask: public QRunnable
{
public:
    void run(void) {
        
    //QFETCH(QString, matrixImpl);
    dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create("dtkMaphysSparseMatrixData");
    h_d->setObjectName("dtkMaphysSparseMatrixData");
    
    QVector<QMap<qlonglong, QVector<qlonglong> > > m_FE_neighbours;
    m_FE_neighbours.resize(4);

    m_FE_neighbours[0][0 ] << 0 ;
    m_FE_neighbours[0][0 ] << 1 ;
    m_FE_neighbours[0][0 ] << 3 ;
    m_FE_neighbours[0][0 ] << 8 ;
    m_FE_neighbours[0][1 ] << 0 ;
    m_FE_neighbours[0][1 ] << 1 ;
    m_FE_neighbours[0][1 ] << 2 ;
    m_FE_neighbours[0][1 ] << 3 ;
    m_FE_neighbours[0][1 ] << 4 ;
    m_FE_neighbours[0][2 ] << 1 ;
    m_FE_neighbours[0][2 ] << 2 ;
    m_FE_neighbours[0][2 ] << 3 ;
    m_FE_neighbours[0][2 ] << 4 ;
    m_FE_neighbours[0][2 ] << 5 ;
    m_FE_neighbours[0][2 ] << 6 ;
    m_FE_neighbours[0][3 ] << 0 ;
    m_FE_neighbours[0][3 ] << 1 ;
    m_FE_neighbours[0][3 ] << 2 ;
    m_FE_neighbours[0][3 ] << 3 ;
    m_FE_neighbours[0][3 ] << 6 ;
    m_FE_neighbours[0][3 ] << 7 ;
    m_FE_neighbours[0][3 ] << 8 ;
    m_FE_neighbours[0][4 ] << 1 ;
    m_FE_neighbours[0][4 ] << 2 ;
    m_FE_neighbours[0][4 ] << 4 ;
    m_FE_neighbours[1][4 ] << 4 ;
    m_FE_neighbours[0][4 ] << 5 ;
    m_FE_neighbours[1][4 ] << 5 ;
    m_FE_neighbours[1][4 ] << 9 ;
    m_FE_neighbours[0][5 ] << 2 ;
    m_FE_neighbours[0][5 ] << 4 ;
    m_FE_neighbours[1][5 ] << 4 ;
    m_FE_neighbours[0][5 ] << 5 ;
    m_FE_neighbours[1][5 ] << 5 ;
    m_FE_neighbours[0][5 ] << 6 ;
    m_FE_neighbours[1][5 ] << 6 ;
    m_FE_neighbours[1][5 ] << 9 ;
    m_FE_neighbours[1][5 ] << 10;
    m_FE_neighbours[0][6 ] << 2 ;
    m_FE_neighbours[0][6 ] << 3 ;
    m_FE_neighbours[0][6 ] << 5 ;
    m_FE_neighbours[1][6 ] << 5 ;
    m_FE_neighbours[0][6 ] << 6 ;
    m_FE_neighbours[1][6 ] << 6 ;
    m_FE_neighbours[3][6 ] << 6 ;
    m_FE_neighbours[0][6 ] << 7 ;
    m_FE_neighbours[3][6 ] << 7 ;
    m_FE_neighbours[1][6 ] << 10;
    m_FE_neighbours[1][6 ] << 14;
    m_FE_neighbours[3][6 ] << 14;
    m_FE_neighbours[0][7 ] << 3 ;
    m_FE_neighbours[0][7 ] << 6 ;
    m_FE_neighbours[3][7 ] << 6 ;
    m_FE_neighbours[0][7 ] << 7 ;
    m_FE_neighbours[3][7 ] << 7 ;
    m_FE_neighbours[0][7 ] << 8 ;
    m_FE_neighbours[3][7 ] << 8 ;
    m_FE_neighbours[3][7 ] << 14;
    m_FE_neighbours[3][7 ] << 20;
    m_FE_neighbours[3][7 ] << 21;
    m_FE_neighbours[3][7 ] << 22;
    m_FE_neighbours[0][8 ] << 0 ;
    m_FE_neighbours[0][8 ] << 3 ;
    m_FE_neighbours[0][8 ] << 7 ;
    m_FE_neighbours[3][8 ] << 7 ;
    m_FE_neighbours[0][8 ] << 8 ;
    m_FE_neighbours[3][8 ] << 8 ;
    m_FE_neighbours[3][8 ] << 22;

    m_FE_neighbours[1][9 ] << 4 ;
    m_FE_neighbours[1][9 ] << 5 ;
    m_FE_neighbours[1][9 ] << 9 ;
    m_FE_neighbours[1][9 ] << 10;
    m_FE_neighbours[1][9 ] << 11;
    m_FE_neighbours[1][10] << 5 ;
    m_FE_neighbours[1][10] << 6 ;
    m_FE_neighbours[1][10] << 9 ;
    m_FE_neighbours[1][10] << 10;
    m_FE_neighbours[1][10] << 11;
    m_FE_neighbours[1][10] << 13;
    m_FE_neighbours[1][10] << 14;
    m_FE_neighbours[1][11] << 9 ;
    m_FE_neighbours[1][11] << 10;
    m_FE_neighbours[1][11] << 11;
    m_FE_neighbours[1][11] << 12;
    m_FE_neighbours[1][11] << 13;
    m_FE_neighbours[1][12] << 11;
    m_FE_neighbours[1][12] << 12;
    m_FE_neighbours[2][12] << 12;
    m_FE_neighbours[1][12] << 13;
    m_FE_neighbours[2][12] << 13;
    m_FE_neighbours[2][12] << 15;
    m_FE_neighbours[1][13] << 10;
    m_FE_neighbours[1][13] << 11;
    m_FE_neighbours[1][13] << 12;
    m_FE_neighbours[2][13] << 12;
    m_FE_neighbours[1][13] << 13;
    m_FE_neighbours[2][13] << 13;
    m_FE_neighbours[1][13] << 14;
    m_FE_neighbours[2][13] << 14;
    m_FE_neighbours[2][13] << 15;
    m_FE_neighbours[2][13] << 17;
    m_FE_neighbours[1][14] << 6 ;
    m_FE_neighbours[3][14] << 6 ;
    m_FE_neighbours[3][14] << 7 ;
    m_FE_neighbours[1][14] << 10;
    m_FE_neighbours[1][14] << 13;
    m_FE_neighbours[2][14] << 13;
    m_FE_neighbours[1][14] << 14;
    m_FE_neighbours[2][14] << 14;
    m_FE_neighbours[3][14] << 14;
    m_FE_neighbours[2][14] << 17;
    m_FE_neighbours[2][14] << 18;
    m_FE_neighbours[3][14] << 18;
    m_FE_neighbours[3][14] << 20;

    m_FE_neighbours[2][15] << 12;
    m_FE_neighbours[2][15] << 13;
    m_FE_neighbours[2][15] << 15;
    m_FE_neighbours[2][15] << 16;
    m_FE_neighbours[2][15] << 17;
    m_FE_neighbours[2][16] << 15;
    m_FE_neighbours[2][16] << 16;
    m_FE_neighbours[2][16] << 17;
    m_FE_neighbours[2][16] << 19;
    m_FE_neighbours[2][17] << 13;
    m_FE_neighbours[2][17] << 14;
    m_FE_neighbours[2][17] << 15;
    m_FE_neighbours[2][17] << 16;
    m_FE_neighbours[2][17] << 17;
    m_FE_neighbours[2][17] << 18;
    m_FE_neighbours[2][17] << 19;
    m_FE_neighbours[2][18] << 14;
    m_FE_neighbours[3][18] << 14;
    m_FE_neighbours[2][18] << 17;
    m_FE_neighbours[2][18] << 18;
    m_FE_neighbours[3][18] << 18;
    m_FE_neighbours[2][18] << 19;
    m_FE_neighbours[3][18] << 19;
    m_FE_neighbours[3][18] << 20;
    m_FE_neighbours[2][19] << 16;
    m_FE_neighbours[2][19] << 17;
    m_FE_neighbours[2][19] << 18;
    m_FE_neighbours[3][19] << 18;
    m_FE_neighbours[2][19] << 19;
    m_FE_neighbours[3][19] << 19;
    m_FE_neighbours[3][19] << 20;
    m_FE_neighbours[3][19] << 21;

    m_FE_neighbours[3][20] << 7 ;
    m_FE_neighbours[3][20] << 14;
    m_FE_neighbours[3][20] << 18;
    m_FE_neighbours[3][20] << 19;
    m_FE_neighbours[3][20] << 20;
    m_FE_neighbours[3][20] << 21;
    m_FE_neighbours[3][21] << 7 ;
    m_FE_neighbours[3][21] << 19;
    m_FE_neighbours[3][21] << 20;
    m_FE_neighbours[3][21] << 21;
    m_FE_neighbours[3][21] << 22;
    m_FE_neighbours[3][22] << 7 ;
    m_FE_neighbours[3][22] << 8 ;
    m_FE_neighbours[3][22] << 21;
    m_FE_neighbours[3][22] << 22;

    
    dtkDistributedCommunicator *comm = dtkDistributed::app()->communicator();
    if (comm->size() != 4 ) {
        qWarning() << "bad communicator size, should be equal to 4"  ;
        return;
    }
    qlonglong N = 23;

    dtkArray<double> factor(N);
    factor.fill(1.);
    factor[4] = 2.;
    factor[5] = 2.;
    factor[6] = 3.;
    factor[7] = 2.;
    factor[8] = 2.;
    factor[12] = 2.;
    factor[13] = 2.;
    factor[14] = 3.;
    factor[18] = 2.;
    factor[19] = 2.;
    
    dtkDistributedMapper *m = new dtkDistributedMapper;
    m->initMap(N, comm->size());
    m->setMap( 0, 0);
    m->setMap( 9, 1);
    m->setMap(15, 2);
    m->setMap(20, 3);

    dtkDistributedGraphTopology topo(N, m);

    h_d->setGraph(&topo, 1LL);

    const QMap<qlonglong, QVector<qlonglong> >& local_fe_neighbours = m_FE_neighbours[comm->wid()];

    auto it  = local_fe_neighbours.cbegin();
    auto ite = local_fe_neighbours.cend();
    for(; it != ite; ++it) {
        qlonglong i = it.key();
        const QVector<qlonglong>& n = *it;
        for (int j = 0; j < n.count(); ++j) {
            if (i != n[j])
                h_d->append(i, n[j], n[j] + 0.1);
            else {
                h_d->append(i, n[j], (n[j] + 0.1) / factor[i]);
            }
        }
    }
    
    h_d->build();
    
    for (int rank = 0; rank < comm->size(); ++rank) {
        if (comm->wid() == rank) {
            h_d->rlock();
            int count = 0;
            for(qlonglong i = 0; i < N; ++i) {
                for(qlonglong j = 0; j < N; ++j) {
                    if (h_d->at(i,j) != 0) {
                        ++count;
                        //qDebug() << i << j << h_d->at(i,j);
                        QCOMPARE(h_d->at(i,j), (j + 0.1));
                    }
                }
            }
            h_d->unlock();
            QCOMPARE(count, h_d->graph()->edgeCount());
        }
        comm->barrier();
    }

    dtkSparseMatrixAbstractData<double> *h_c = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create("dtkDistributedSparseMatrixData");
    h_c->setObjectName("dtkDistributedSparseMatrixData");

    h_c->copy(h_d);
    
    
    for (int rank = 0; rank < comm->size(); ++rank) {
        if (comm->wid() == rank) {
            h_c->rlock();
            int count = 0;
            for(qlonglong i = 0; i < N; ++i) {
                for(qlonglong j = 0; j < N; ++j) {
                    if (h_c->at(i,j) != 0) {
                        ++count;
                        //qDebug() << rank << i << j;
                        QCOMPARE(h_c->at(i,j), (j + 0.1));
                    }
                }
            }
            h_c->unlock();
            QCOMPARE(count, h_c->graph()->edgeCount());
        }
        comm->barrier();
    }


    h_c->begin();

    delete h_d;
    delete h_c;
    }
};

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

QHash<qlonglong, double> getReferenceSparseMatrixData(void)
{
    QHash<qlonglong, double> smat;

    smat.insert(0 * 5 + 0,  1.000e+00);
    smat.insert(1 * 5 + 1,  1.050e+01);
    smat.insert(2 * 5 + 2,  1.500e-02);
    smat.insert(0 * 5 + 3,  6.000e+00);
    smat.insert(3 * 5 + 1,  2.505e+02);
    smat.insert(3 * 5 + 3, -2.800e+02);
    smat.insert(3 * 5 + 4,  3.332e+01);
    smat.insert(4 * 5 + 4,  1.200e+01);

    return smat;
}

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

void dtkSparseMatrixDataTestCase::initTestCase(void)
{
    initPluginManager();

    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
    if (dtkDistributed::policy()->types().contains("mpi3"))
        dtkDistributed::policy()->setType("mpi3");
    else
        dtkDistributed::policy()->setType("qthread");
    for (int i = 0; i < 3; ++i) {
        dtkDistributed::policy()->addHost("localhost");
    }
}

void dtkSparseMatrixDataTestCase::init(void)
{
}

// void dtkSparseMatrixDataTestCase::testFactoryCreation_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testResizeReserve_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testAppendBuild_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testAt_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testInsert_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testRemove_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testSetAt_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testFill_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testClear_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testReset_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testReadMM_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testReadBigMM_data(void){ setMatrixImplementation();}
// void dtkSparseMatrixDataTestCase::testGetLine_data(void){ setMatrixImplementation();}

void dtkSparseMatrixDataTestCase::testToto(void)
{
    totoTask task;
    dtkDistributed::spawn();
    dtkDistributed::exec(&task);
    dtkDistributed::unspawn();
}

// void dtkSparseMatrixDataTestCase::testFactoryCreation(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     QVERIFY(h_d);
//     QCOMPARE(h_d->rowCount(), 0LL);
//     QCOMPARE(h_d->colCount(), 0LL);
//     QCOMPARE(h_d->nonZeroCount(), 0LL);
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testResizeReserve(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);
//     QCOMPARE(h_d->rowCount(), 5LL);
//     QCOMPARE(h_d->colCount(), 5LL);

//     h_d->resize(101, 101);
//     QCOMPARE(h_d->rowCount(), 101LL);
//     QCOMPARE(h_d->colCount(), 101LL);

//     h_d->reserve(155);
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testAppendBuild(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     h_d->append(0, 0, smat[0 * 5 + 0]);
//     h_d->append(1, 1, smat[1 * 5 + 1]);
//     h_d->append(2, 2, smat[2 * 5 + 2]);
//     h_d->append(0, 3, smat[0 * 5 + 3]);
//     h_d->append(3, 1, smat[3 * 5 + 1]);
//     h_d->append(3, 3, smat[3 * 5 + 3]);
//     h_d->append(3, 4, smat[3 * 5 + 4]);
//     h_d->append(4, 4, smat[4 * 5 + 4]);

//     h_d->build();

//     QCOMPARE(h_d->nonZeroCount(), (qlonglong)(smat.count()));
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testAt(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     h_d->append(0, 0, smat[0 * 5 + 0]);
//     h_d->append(1, 1, smat[1 * 5 + 1]);
//     h_d->append(2, 2, smat[2 * 5 + 2]);
//     h_d->append(0, 3, smat[0 * 5 + 3]);
//     h_d->append(3, 1, smat[3 * 5 + 1]);
//     h_d->append(3, 3, smat[3 * 5 + 3]);
//     h_d->append(3, 4, smat[3 * 5 + 4]);
//     h_d->append(4, 4, smat[4 * 5 + 4]);

//     h_d->build();

//     for(int i = 0; i < 5; ++i) {
//         for(int j = 0; j < 5; ++j) {
//             QCOMPARE(h_d->at(i, j), smat.value(i * 5 + j, 0.0));
//         }
//     }
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testInsert(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     // h_d->insert(0, 0, smat[0 * 5 + 0]);
//     // h_d->insert(1, 1, smat[1 * 5 + 1]);
//     // h_d->insert(2, 2, smat[2 * 5 + 2]);
//     // h_d->insert(0, 3, smat[0 * 5 + 3]);
//     // h_d->insert(3, 1, smat[3 * 5 + 1]);
//     // h_d->insert(3, 3, smat[3 * 5 + 3]);
//     // h_d->insert(3, 4, smat[3 * 5 + 4]);
//     // h_d->insert(4, 4, smat[4 * 5 + 4]);

//     // for(int i = 0; i < 5; ++i) {
//     //     for(int j = 0; j < 5; ++j) {
//     //         QCOMPARE(h_d->at(i, j), smat.value(i * 5 + j, 0.0));
//     //     }
//     // }
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testRemove(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     // QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     // h_d->insert(0, 0, smat[0 * 5 + 0]);
//     // h_d->insert(1, 1, smat[1 * 5 + 1]);
//     // h_d->insert(2, 2, smat[2 * 5 + 2]);
//     // h_d->insert(0, 3, smat[0 * 5 + 3]);
//     // h_d->insert(3, 1, smat[3 * 5 + 1]);
//     // h_d->insert(3, 3, smat[3 * 5 + 3]);
//     // h_d->insert(3, 4, smat[3 * 5 + 4]);
//     // h_d->insert(4, 4, smat[4 * 5 + 4]);

//     // for(int i = 0; i < 5; ++i) {
//     //     for(int j = 0; j < 5; ++j) {
//     //         h_d->remove(i, j);
//     //     }
//     // }

//     // QCOMPARE(h_d->nonZeroCount(), 0LL);
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testSetAt(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();
//     h_d->append(0, 0, -1);
//     h_d->append(1, 1, -1);
//     h_d->append(2, 2, -1);
//     h_d->append(0, 3, -1);
//     h_d->append(3, 1, -1);
//     h_d->append(3, 3, -1);
//     h_d->append(3, 4, -1);
//     h_d->append(4, 4, -1);

//     h_d->build();

//     h_d->setAt(0, 0, smat[0 * 5 + 0]);
//     h_d->setAt(1, 1, smat[1 * 5 + 1]);
//     h_d->setAt(2, 2, smat[2 * 5 + 2]);
//     h_d->setAt(0, 3, smat[0 * 5 + 3]);
//     h_d->setAt(3, 1, smat[3 * 5 + 1]);
//     h_d->setAt(3, 3, smat[3 * 5 + 3]);
//     h_d->setAt(3, 4, smat[3 * 5 + 4]);
//     h_d->setAt(4, 4, smat[4 * 5 + 4]);

//     for(int i = 0; i < 5; ++i) {
//         for(int j = 0; j < 5; ++j) {
//             qDebug() << i << j << h_d->at(i, j) << smat.value(i * 5 + j, 0.0);
//             QCOMPARE(h_d->at(i, j), smat.value(i * 5 + j, 0.0));
//         }
//     }
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testReadBigMM(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double>  *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);
//     dtkSparseMatrix<double> mat(h_d);

//     QTime time; time.restart();
//     QString filename = "../laminar_duct3D.mtx.gz";

//     if (QFile(filename).exists()) {
//         QVERIFY(mat.readMatrixMarket(filename));
//         qDebug() << "elapsed:"<<  time.elapsed();
//         QVERIFY(mat.rowCount() == 67173);
//         QVERIFY(mat.colCount() == 67173);
//         QVERIFY(mat.nonZeroCount() == 3833077);
//     } else {
//         qDebug() << "skip test testReadBigMM, no matrix file:" << filename;
//     }
// }

// void dtkSparseMatrixDataTestCase::testReadMM(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double>  *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);
//     dtkSparseMatrix<double> mat(h_d);

//     QVERIFY(mat.readMatrixMarket("../matrix.mtx"));
//     QCOMPARE(mat.rowCount() , 5);
//     QCOMPARE(mat.colCount() , 5);
//     QCOMPARE(mat.nonZeroCount() , 8);
//     qDebug() << 0 << 0 << mat.at(0,0);
//     qDebug() << 0 << 3 << mat.at(0,3);
//     qDebug() << 1 << 1 << mat.at(1,1);
//     qDebug() << 2 << 2 << mat.at(2,2);
//     qDebug() << 3 << 1 << mat.at(3,1);
//     qDebug() << 3 << 3 << mat.at(3,3);
//     qDebug() << 3 << 4 << mat.at(3,4);
//     // qDebug() << 4 << 0 << mat.at(4,0);
//     qDebug() << 4 << 4 << mat.at(4,4);
//     QCOMPARE(mat.at(0,0) , 1.0);
//     QCOMPARE(mat.at(1,1) , 10.5);
//     QCOMPARE(mat.at(2,2) , 0.015);
//     QCOMPARE(mat.at(0,3) , 6.0);
//     QCOMPARE(mat.at(3,3) , -280.0);
//     // QCOMPARE(mat.at(4,0) , 12.0);
//     QCOMPARE(mat.at(4,4) , 12.0);
// }

// void dtkSparseMatrixDataTestCase::testFill(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     h_d->insert(0, 0, smat[0 * 5 + 0]);
//     h_d->insert(1, 1, smat[1 * 5 + 1]);
//     h_d->insert(2, 2, smat[2 * 5 + 2]);
//     h_d->insert(0, 3, smat[0 * 5 + 3]);
//     h_d->insert(3, 1, smat[3 * 5 + 1]);
//     h_d->insert(3, 3, smat[3 * 5 + 3]);
//     h_d->insert(3, 4, smat[3 * 5 + 4]);
//     h_d->insert(4, 4, smat[4 * 5 + 4]);

//     h_d->fill(3.14159);
//     for(int i = 0; i < 5; ++i) {
//         for(int j = 0; j < 5; ++j) {
//             if (h_d->at(i, j) != 0.0)
//                 QCOMPARE(h_d->at(i, j), 3.14159);
//         }
//     }
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testClear(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     h_d->insert(0, 0, smat[0 * 5 + 0]);
//     h_d->insert(1, 1, smat[1 * 5 + 1]);
//     h_d->insert(2, 2, smat[2 * 5 + 2]);
//     h_d->insert(0, 3, smat[0 * 5 + 3]);
//     h_d->insert(3, 1, smat[3 * 5 + 1]);
//     h_d->insert(3, 3, smat[3 * 5 + 3]);
//     h_d->insert(3, 4, smat[3 * 5 + 4]);
//     h_d->insert(4, 4, smat[4 * 5 + 4]);

//     h_d->clear();

//     QCOMPARE(h_d->rowCount(), 0LL);
//     QCOMPARE(h_d->colCount(), 0LL);
//     QCOMPARE(h_d->nonZeroCount(), 0LL);
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testReset(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     h_d->insert(0, 0, smat[0 * 5 + 0]);
//     h_d->insert(1, 1, smat[1 * 5 + 1]);
//     h_d->insert(2, 2, smat[2 * 5 + 2]);
//     h_d->insert(0, 3, smat[0 * 5 + 3]);
//     h_d->insert(3, 1, smat[3 * 5 + 1]);
//     h_d->insert(3, 3, smat[3 * 5 + 3]);
//     h_d->insert(3, 4, smat[3 * 5 + 4]);
//     h_d->insert(4, 4, smat[4 * 5 + 4]);

//     h_d->reset();

//     // QCOMPARE(h_d->rowCount(), 5LL);
//     // QCOMPARE(h_d->colCount(), 5LL);
//     // QCOMPARE(h_d->nonZeroCount(), 0LL);
    
//     delete h_d;
// }

// void dtkSparseMatrixDataTestCase::testGetLine(void)
// {
//     QFETCH(QString, matrixImpl);
//     dtkSparseMatrixAbstractData<double> *h_d = dtkLinearAlgebraSparse::matrixData::pluginFactory<double>().create(matrixImpl);

//     h_d->resize(5, 5);

//     QHash<qlonglong, double> smat = getReferenceSparseMatrixData();

//     h_d->insert(0, 0, smat[0 * 5 + 0]);
//     h_d->insert(1, 1, smat[1 * 5 + 1]);
//     h_d->insert(2, 2, smat[2 * 5 + 2]);
//     h_d->insert(0, 3, smat[0 * 5 + 3]);
//     h_d->insert(3, 1, smat[3 * 5 + 1]);
//     h_d->insert(3, 3, smat[3 * 5 + 3]);
//     h_d->insert(3, 4, smat[3 * 5 + 4]);
//     h_d->insert(4, 4, smat[4 * 5 + 4]);

//     dtkSparseMatrixAbstractData<double>::line_iterator lit  = h_d->begin();
//     dtkSparseMatrixAbstractData<double>::line_iterator lend = h_d->end();
    
//     delete h_d;
// }

void dtkSparseMatrixDataTestCase::cleanupTestCase(void)
{
    
}

void dtkSparseMatrixDataTestCase::cleanup(void)
{
    
}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkSparseMatrixDataTest, dtkSparseMatrixDataTestCase)

// 
// dtkSparseMatrixDataTest.cpp ends here
