// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkSparseMatrixTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

QHash<qlonglong, double> getReferenceSparseMatrix(void)
{
    QHash<qlonglong, double> smat;

    smat.insert(0 * 5 + 0,  1.000e+00);
    smat.insert(1 * 5 + 1,  1.050e+01);
    smat.insert(2 * 5 + 2,  1.500e-02);
    smat.insert(0 * 5 + 3,  6.000e+00);
    smat.insert(3 * 5 + 1,  2.505e+02);
    smat.insert(3 * 5 + 3, -2.800e+02);
    smat.insert(3 * 5 + 4,  3.332e+01);
    smat.insert(4 * 5 + 4,  1.200e+01);

    return smat;
}

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

void dtkSparseMatrixTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkSparseMatrixTestCase::init(void)
{
}


void dtkSparseMatrixTestCase::testFactoryCreation_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testDataType_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testResize_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testInsertRemoveAssemble_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testAt_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testSetAt_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testFill_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testMult_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testAdd_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testSub_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testCopy_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testClear_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testGetLine_data(void){ setMatrixImplementation();}
void dtkSparseMatrixTestCase::testGetGlobalLine_data(void){ setMatrixImplementation();}

void dtkSparseMatrixTestCase::testFactoryCreation(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    QVERIFY(mat);
    QCOMPARE(mat->rowCount(), 0LL);
    QCOMPARE(mat->colCount(), 0LL);
    QCOMPARE(mat->nonZeroCount(), 0LL);

    QVariant v = QVariant::fromValue(mat);
    dtkSparseMatrix<double> *ptr = v.value<dtkSparseMatrix<double> *>();
    QCOMPARE(mat , ptr);

    delete mat;
}

void dtkSparseMatrixTestCase::testDataType(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    QCOMPARE(mat->dataType(), matrixImpl);

    delete mat;
}

void dtkSparseMatrixTestCase::testResize(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);
    QCOMPARE(mat->rowCount(), 5LL);
    QCOMPARE(mat->colCount(), 5LL);

    mat->resize(101);
    QCOMPARE(mat->rowCount(), 101LL);
    QCOMPARE(mat->colCount(), 101LL);
    
    delete mat;
}

void dtkSparseMatrixTestCase::testInsertRemoveAssemble(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->remove(4, 4);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    QCOMPARE(mat->nonZeroCount(), (qlonglong)(smat.count()));
    
    delete mat;
}

void dtkSparseMatrixTestCase::testAt(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            QCOMPARE(mat->at(i, j), smat.value(i * 5 + j, 0.0));
        }
    }
    
    delete mat;
}

void dtkSparseMatrixTestCase::testSetAt(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, -11.11111);
    mat->insert(1, 1, -11.11111);
    mat->insert(2, 2, -11.11111);
    mat->insert(0, 3, -11.11111);
    mat->insert(3, 1, -11.11111);
    mat->insert(3, 3, -11.11111);
    mat->insert(3, 4, -11.11111);
    mat->insert(4, 4, -11.11111);

    mat->assemble();

    mat->setAt(0, 0, smat[0 * 5 + 0]);
    mat->setAt(1, 1, smat[1 * 5 + 1]);
    mat->setAt(2, 2, smat[2 * 5 + 2]);
    mat->setAt(0, 3, smat[0 * 5 + 3]);
    mat->setAt(3, 1, smat[3 * 5 + 1]);
    mat->setAt(3, 3, smat[3 * 5 + 3]);
    mat->setAt(3, 4, smat[3 * 5 + 4]);
    mat->setAt(4, 4, smat[4 * 5 + 4]);


    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            QCOMPARE(mat->at(i, j), smat.value(i * 5 + j, 0.0));
        }
    }

    delete mat;
}

void dtkSparseMatrixTestCase::testFill(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    mat->fill(3.14159);
    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            if (mat->at(i, j) != 0.0)
                QCOMPARE(mat->at(i, j), 3.14159);
        }
    }

    delete mat;
}

void dtkSparseMatrixTestCase::testMult(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    double coef = 2;
    *mat *= coef;
    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            if (mat->at(i, j) != 0.0)
                QCOMPARE(mat->at(i, j), coef * smat.value(i * 5 + j, 0.0) );
        }
    }

    delete mat;
}

void dtkSparseMatrixTestCase::testCopy(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    dtkSparseMatrix<double> *mat2 = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    *mat2 = *mat;

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            if (mat->at(i, j) != 0.0)
                QCOMPARE(mat->at(i, j), mat2->at(i, j));
        }
    }

    delete mat;
    delete mat2;
}

void dtkSparseMatrixTestCase::testAdd(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    dtkSparseMatrix<double> *mat2 = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    *mat2  = *mat;
    *mat2 += *mat;

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            if (mat->at(i, j) != 0.0)
                QCOMPARE(2 * mat->at(i, j), mat2->at(i, j));
        }
    }

    delete mat;
    delete mat2;
}

void dtkSparseMatrixTestCase::testSub(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    dtkSparseMatrix<double> *mat2 = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    *mat2  = *mat;
    *mat2 -= *mat;

    for(int i = 0; i < 5; ++i) {
        for(int j = 0; j < 5; ++j) {
            if (mat->at(i, j) != 0.0)
                QCOMPARE(0.0, mat2->at(i, j));
        }
    }

    delete mat;
    delete mat2;
}

void dtkSparseMatrixTestCase::testClear(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    mat->clear();

    QCOMPARE(mat->rowCount(), 0LL);
    QCOMPARE(mat->colCount(), 0LL);
    QCOMPARE(mat->nonZeroCount(), 0LL);

    delete mat;
}

void dtkSparseMatrixTestCase::testGetLine(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    dtkSparseMatrix<double>::line_iterator lit  = mat->begin();
    dtkSparseMatrix<double>::line_iterator lend = mat->end();

    delete mat;
}

void dtkSparseMatrixTestCase::testGetGlobalLine(void)
{
    QFETCH(QString, matrixImpl);
    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);

    mat->resize(5);

    QHash<qlonglong, double> smat = getReferenceSparseMatrix();

    mat->insert(0, 0, smat[0 * 5 + 0]);
    mat->insert(1, 1, smat[1 * 5 + 1]);
    mat->insert(2, 2, smat[2 * 5 + 2]);
    mat->insert(0, 3, smat[0 * 5 + 3]);
    mat->insert(3, 1, smat[3 * 5 + 1]);
    mat->insert(3, 3, smat[3 * 5 + 3]);
    mat->insert(3, 4, smat[3 * 5 + 4]);
    mat->insert(4, 4, smat[4 * 5 + 4]);

    mat->assemble();

    for (qlonglong i = 0; i < mat->rowCount(); ++i) {
        dtkSparseMatrix<double>::line_iterator lit = mat->lineAt(i);
        QCOMPARE(lit.id(), i);
    }


    delete mat;
}

void dtkSparseMatrixTestCase::cleanupTestCase(void)
{
    
}

void dtkSparseMatrixTestCase::cleanup(void)
{
    
}

DTKTEST_MAIN_NOGUI(dtkSparseMatrixTest, dtkSparseMatrixTestCase)

// 
// dtkSparseMatrixTest.cpp ends here
