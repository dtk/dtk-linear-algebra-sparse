// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkSparseMatrixLineElementTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"
// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixLineElementTestCasePrivate
{
public:
    void initSparseMatrix(void);

public:
    dtkSparseMatrixEngine<double> *smat;
    dtkArray<QList<qlonglong> > elt_ids;
    dtkArray<QList<double> > elt_values;
};

// ///////////////////////////////////////////////////////////////////

void dtkSparseMatrixLineElementTestCasePrivate::initSparseMatrix(void)
{
    QFETCH(QString, matrixImpl);
    smat = dtkLinearAlgebraSparse::matrixEngine::pluginFactory<double>().create(matrixImpl);

    smat->resize(5);

    smat->insert(0, 0,  1.000e+00);
    smat->insert(1, 1,  1.050e+01);
    smat->insert(2, 2,  1.500e-02);
    smat->insert(0, 3,  6.000e+00);
    smat->insert(3, 1,  2.505e+02);
    smat->insert(3, 3, -2.800e+02);
    smat->insert(3, 4,  3.332e+01);
    smat->insert(4, 4,  1.200e+01);

    smat->assemble();

    elt_ids.resize(5);
    elt_ids[0] << 0 << 3;
    elt_ids[1] << 1;
    elt_ids[2] << 2;
    elt_ids[3] << 1 << 3 << 4;
    elt_ids[4] << 4;

    elt_values.resize(5);
    elt_values[0] << 1.000e+00 << 6.000e+00;
    elt_values[1] << 1.050e+01;
    elt_values[2] << 1.500e-02;
    elt_values[3] << 2.505e+02 << -2.800e+02 << 3.332e+01;
    elt_values[4] << 1.200e+01;
}

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

dtkSparseMatrixLineElementTestCase::dtkSparseMatrixLineElementTestCase(void) : d(new dtkSparseMatrixLineElementTestCasePrivate)
{

}

dtkSparseMatrixLineElementTestCase::~dtkSparseMatrixLineElementTestCase(void)
{
    if (d) {
        if (d->smat) {
            delete d->smat;
        }
        delete d;
    }
}

void dtkSparseMatrixLineElementTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkSparseMatrixLineElementTestCase::init(void)
{
    d->initSparseMatrix();
}

void dtkSparseMatrixLineElementTestCase::testCreation_data(void){ setMatrixImplementation();}
void dtkSparseMatrixLineElementTestCase::testIdLineId_data(void){ setMatrixImplementation();}
void dtkSparseMatrixLineElementTestCase::testValue_data(void){ setMatrixImplementation();}
void dtkSparseMatrixLineElementTestCase::testComparison_data(void){ setMatrixImplementation();}
void dtkSparseMatrixLineElementTestCase::testOperation_data(void){ setMatrixImplementation();}

void dtkSparseMatrixLineElementTestCase::testCreation(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it = d->smat->begin();

    dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt = it.begin();
    dtkSparseMatrixEngine<double>::line_iterator::element_iterator eend = it.end();
}

void dtkSparseMatrixLineElementTestCase::testIdLineId(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    for(int i = 0; it != end; ++it, ++i) {
        dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt = it.begin();
        dtkSparseMatrixEngine<double>::line_iterator::element_iterator eend = it.end();

        const QList<qlonglong>& line_elts = d->elt_ids[i];

        for(int j = 0; elt != eend; ++elt, ++j) {
            QCOMPARE(elt.lineId(), it.id());
            QCOMPARE(elt.id(), line_elts.at(j));
        }
    }
}

void dtkSparseMatrixLineElementTestCase::testValue(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    for(int i = 0; it != end; ++it, ++i) {
        dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt = it.begin();
        dtkSparseMatrixEngine<double>::line_iterator::element_iterator eend = it.end();

        const QList<double>& line_elts = d->elt_values[i];

        for(int j = 0; elt != eend; ++elt, ++j) {
            //qDebug() << i << j << *elt << it.id() << elt.id() << line_elts.at(j);
            QCOMPARE(*elt, line_elts.at(j));
        }
    }

    dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt = d->smat->begin().begin();

    QCOMPARE(elt[1], d->elt_values[0].at(1));
}

void dtkSparseMatrixLineElementTestCase::testComparison(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin() + 3;

    dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt = it.begin();
    dtkSparseMatrixEngine<double>::line_iterator::element_iterator eend = it.end();

    dtkSparseMatrixEngine<double>::line_iterator::element_iterator olt = it.begin();
    dtkSparseMatrixEngine<double>::line_iterator::element_iterator oend = it.end();

    QVERIFY(elt == olt);
    QVERIFY(eend == oend);
    QVERIFY(elt != oend);
    QVERIFY(eend != olt);

    ++elt;
    --eend;

    QVERIFY(elt > olt);
    QVERIFY(elt >= olt);
    QVERIFY(eend < oend);
    QVERIFY(eend <= oend);    
}

void dtkSparseMatrixLineElementTestCase::testOperation(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    dtkSparseMatrixEngine<double>::line_iterator o_it;
    dtkSparseMatrixEngine<double>::line_iterator o_end;

    o_it = it++;
    QVERIFY(o_it < it);

    o_end = end--;
    QVERIFY(end < o_end);

    dtkSparseMatrixEngine<double>::line_iterator s_it;
    dtkSparseMatrixEngine<double>::line_iterator s_end;

    s_it = o_it + 5;
    QVERIFY(s_it == o_end);
    s_end = o_end - 5;
    QVERIFY(s_end == o_it);
}

void dtkSparseMatrixLineElementTestCase::cleanupTestCase(void)
{

}

void dtkSparseMatrixLineElementTestCase::cleanup(void)
{
    if (d->smat) {
        delete d->smat;
        d->smat = NULL;
    }
}

DTKTEST_MAIN_NOGUI(dtkSparseMatrixLineElementTest, dtkSparseMatrixLineElementTestCase)

// 
// dtkSparseMatrixLineElementTest.cpp ends here
