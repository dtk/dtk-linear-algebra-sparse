// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkSparseMatrixTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testFactoryCreation(void);
    void testDataType(void);
    void testResize(void);
    void testInsertRemoveAssemble(void);
    void testAt(void);
    void testSetAt(void);
    void testFill(void);
    void testCopy(void);
    void testMult(void);
    void testAdd(void);
    void testSub(void);
    void testClear(void);
    void testGetLine(void);
    void testGetGlobalLine(void);

    void testFactoryCreation_data(void);
    void testDataType_data(void);
    void testResize_data(void);
    void testInsertRemoveAssemble_data(void);
    void testAt_data(void);
    void testSetAt_data(void);
    void testFill_data(void);
    void testMult_data(void);
    void testCopy_data(void);
    void testAdd_data(void);
    void testSub_data(void);
    void testClear_data(void);
    void testGetLine_data(void);
    void testGetGlobalLine_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


// 
// dtkSparseMatrixTest.h ends here
