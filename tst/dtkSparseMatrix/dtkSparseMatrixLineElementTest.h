// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkSparseMatrixLineElementTestCasePrivate;

class dtkSparseMatrixLineElementTestCase : public QObject
{
    Q_OBJECT

public:
     dtkSparseMatrixLineElementTestCase(void);
    ~dtkSparseMatrixLineElementTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreation(void);
    void testIdLineId(void);
    void testValue(void);
    void testComparison(void);
    void testOperation(void);

    void testCreation_data(void);
    void testIdLineId_data(void);
    void testValue_data(void);
    void testComparison_data(void);
    void testOperation_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);

private:
    dtkSparseMatrixLineElementTestCasePrivate *d;
};


// 
// dtkSparseMatrixLineElementTest.h ends here
