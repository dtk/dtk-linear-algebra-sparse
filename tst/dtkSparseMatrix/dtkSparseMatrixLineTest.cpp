// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseMatrixLineTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixLineTestCasePrivate
{
public:
    void initSparseMatrix(void);

public:
    dtkSparseMatrixEngine<double> *smat;
    dtkArray<qlonglong> nnz_array;
    dtkArray<qlonglong> width_array;
};

// ///////////////////////////////////////////////////////////////////

void dtkSparseMatrixLineTestCasePrivate::initSparseMatrix(void)
{
    QFETCH(QString, matrixImpl);

    smat = dtkLinearAlgebraSparse::matrixEngine::pluginFactory<double>().create(matrixImpl);

    smat->resize(5);

    smat->insert(0, 0,  1.000e+00);
    smat->insert(1, 1,  1.050e+01);
    smat->insert(2, 2,  1.500e-02);
    smat->insert(0, 3,  6.000e+00);
    smat->insert(3, 1,  2.505e+02);
    smat->insert(3, 3, -2.800e+02);
    smat->insert(3, 4,  3.332e+01);
    smat->insert(4, 4,  1.200e+01);

    smat->assemble();

    nnz_array.reserve(5);
    nnz_array << 2 << 1 << 1 << 3 << 1;

    width_array.reserve(5);
    width_array << 3 << 0 << 0 << 3 << 0;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkSparseMatrixLineTestCase::dtkSparseMatrixLineTestCase(void) : d(new dtkSparseMatrixLineTestCasePrivate)
{

}

dtkSparseMatrixLineTestCase::~dtkSparseMatrixLineTestCase(void)
{
    if (d) {
        if (d->smat) {
            delete d->smat;
        }
        delete d;
    }
}

void dtkSparseMatrixLineTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkSparseMatrixLineTestCase::init(void)
{
    d->initSparseMatrix();
}

void dtkSparseMatrixLineTestCase::testCreation_data(void) {setMatrixImplementation();}
void dtkSparseMatrixLineTestCase::testId_data(void) {setMatrixImplementation();}
void dtkSparseMatrixLineTestCase::testSize_data(void) {setMatrixImplementation();}
void dtkSparseMatrixLineTestCase::testWidth_data(void) {setMatrixImplementation();}
void dtkSparseMatrixLineTestCase::testComparison_data(void) {setMatrixImplementation();}
void dtkSparseMatrixLineTestCase::testOperation_data(void) {setMatrixImplementation();}
void dtkSparseMatrixLineTestCase::testElementLineCreation_data(void) {setMatrixImplementation();}

void dtkSparseMatrixLineTestCase::testCreation(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();
}

void dtkSparseMatrixLineTestCase::testId(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    for(qlonglong i = 0; it != end; ++it, ++i) {
        QCOMPARE(it.id(), i);
    }
}

void dtkSparseMatrixLineTestCase::testSize(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    for(;it != end; ++it) {
        QCOMPARE(it.size(), d->nnz_array.at(it.id()));
    }
}

void dtkSparseMatrixLineTestCase::testWidth(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    for(;it != end; ++it) {
        QCOMPARE(it.width(), d->width_array.at(it.id()));
    }
}

void dtkSparseMatrixLineTestCase::testComparison(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    dtkSparseMatrixEngine<double>::line_iterator o_it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator o_end = d->smat->end();

    QVERIFY(it == o_it);
    QVERIFY(end == o_end);
    QVERIFY(it != o_end);
    QVERIFY(end != o_it);

    ++it;
    --end;

    QVERIFY(it > o_it);
    QVERIFY(it >= o_it);
    QVERIFY(end < o_end);
    QVERIFY(end <= o_end);
}

void dtkSparseMatrixLineTestCase::testOperation(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();
    dtkSparseMatrixEngine<double>::line_iterator end = d->smat->end();

    dtkSparseMatrixEngine<double>::line_iterator o_it;
    dtkSparseMatrixEngine<double>::line_iterator o_end;

    o_it = it++;
    QVERIFY(o_it < it);

    o_end = end--;
    QVERIFY(end < o_end);

    dtkSparseMatrixEngine<double>::line_iterator s_it;
    dtkSparseMatrixEngine<double>::line_iterator s_end;

    s_it = o_it + 5;
    QVERIFY(s_it == o_end);
    s_end = o_end - 5;
    QVERIFY(s_end == o_it);
}

void dtkSparseMatrixLineTestCase::testElementLineCreation(void)
{
    dtkSparseMatrixEngine<double>::line_iterator it  = d->smat->begin();

    dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt = it.begin();
    dtkSparseMatrixEngine<double>::line_iterator::element_iterator elt_end = it.end();
}

void dtkSparseMatrixLineTestCase::cleanupTestCase(void)
{

}

void dtkSparseMatrixLineTestCase::cleanup(void)
{
    if (d->smat) {
        delete d->smat;
        d->smat = NULL;
    }
}

DTKTEST_MAIN_NOGUI(dtkSparseMatrixLineTest, dtkSparseMatrixLineTestCase)

//
// dtkSparseMatrixLineTest.cpp ends here
