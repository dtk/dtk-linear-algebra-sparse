// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkSparseMatrixLineTestCasePrivate;

class dtkSparseMatrixLineTestCase : public QObject
{
    Q_OBJECT

public:
     dtkSparseMatrixLineTestCase(void);
    ~dtkSparseMatrixLineTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreation(void);
    void testId(void);
    void testSize(void);
    void testWidth(void);
    void testComparison(void);
    void testOperation(void);
    void testElementLineCreation(void);

    void testCreation_data(void);
    void testId_data(void);
    void testSize_data(void);
    void testWidth_data(void);
    void testComparison_data(void);
    void testOperation_data(void);
    void testElementLineCreation_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);

private:
    dtkSparseMatrixLineTestCasePrivate *d;
};


// 
// dtkSparseMatrixLineTest.h ends here
