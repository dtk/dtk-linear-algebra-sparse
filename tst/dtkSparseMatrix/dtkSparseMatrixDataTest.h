// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class dtkSparseMatrixDataTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testToto(void);
    // void testFactoryCreation(void);
    // void testResizeReserve(void);
    // void testAppendBuild(void);
    // void testAt(void);
    // void testInsert(void);
    // void testRemove(void);
    // void testSetAt(void);
    // void testFill(void);
    // void testClear(void);
    // void testReset(void);
    // void testReadMM(void);
    // void testReadBigMM(void);
    // void testGetLine(void);

    // void testFactoryCreation_data(void);
    // void testResizeReserve_data(void);
    // void testAppendBuild_data(void);
    // void testAt_data(void);
    // void testInsert_data(void);
    // void testRemove_data(void);
    // void testSetAt_data(void);
    // void testFill_data(void);
    // void testClear_data(void);
    // void testReset_data(void);
    // void testReadMM_data(void);
    // void testReadBigMM_data(void);
    // void testGetLine_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);

private:
    QString implementation;
};


// 
// dtkSparseMatrixDataTest.h ends here
