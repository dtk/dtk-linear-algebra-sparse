/* @(#)dtkLinearAlgebraSparseTestCase.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2015 - Nicolas Niclausse, Inria.
 * Created: 2015/04/17 07:33:54
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <dtkTest>
#include <dtkLinearAlgebraSparse>

static void initPluginManager(void)
{
    dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
    linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");
    dtkLinearAlgebraSparse::pluginManager::initialize(linear_algebra_sparse_settings.value("plugins").toString());
    linear_algebra_sparse_settings.endGroup();

}

static void setVectorImplementation(void)
{
    QStringList vectorPlugins = dtkLinearAlgebraSparse::vectorData::pluginFactory<double>().keys();
    QTest::addColumn<QString>("vectorImpl");
    foreach(QString plugin, vectorPlugins)
        QTest::newRow(qPrintable(plugin)) << plugin;
}

static void setMatrixImplementation(void)
{
    QStringList matrixPlugins = dtkLinearAlgebraSparse::matrixEngine::pluginFactory<double>().keys();
    QTest::addColumn<QString>("matrixImpl");
    foreach(QString plugin, matrixPlugins)
        QTest::newRow(qPrintable(plugin)) << plugin;
}

static void setAllImplementations(void)
{
    QTest::addColumn<QString>("vectorImpl");
    QTest::addColumn<QString>("matrixImpl");

    //    QTest::newRow("sequential")<< "dtkVectorData";// << "dtkSparseMatrixData";
    QTest::newRow("distributed_CSR") << "dtkDistributedVectorData" << "dtkDistributedSparseMatrixEngineCSR";
    QTest::newRow("distributed_DD") << "dtkDistributedVectorData" << "dtkDistributedSparseMatrixEngineDD";
}
