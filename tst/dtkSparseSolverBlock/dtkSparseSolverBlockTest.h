// Version: $Id$
//
//
#pragma once

#include <dtkTest>

class dtkSparseSolverBlockTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testJacobi(void);
    void testSGS(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


//
// dtkSparseSolverBlockTest.h ends here
