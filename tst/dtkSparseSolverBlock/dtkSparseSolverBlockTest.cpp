// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseSolverBlockTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////
#include <dtkDistributed>
#include <dtkDistributed/dtkDistributedPolicy>

class solveTask: public QRunnable
{
public:
    QString solver_impl;
public:
    void run(void) {
            // System to solve
        //
        // ----       ----    --  --    --  --
        // |  7 -2  1  2 |    | x0 |    |  3 |
        // |  2  8  3  1 |    | x1 |    | -2 |
        // | -1  0  5  2 |  * | x2 |  = |  5 |
        // |  0  2 -1  4 |    | x3 |    |  4 |
        // ----       ----    --  --    --  --

        dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();

        QVector<double> mat_buffer;
        mat_buffer <<
            7 << -2 <<  1 << 2 <<
            2 <<  8 <<  3 << 1 <<
            -1 <<  0 <<  5 << 2 <<
            0 <<  2 << -1 << 4;

        dtkVector<double> *rhs = dtkLinearAlgebraSparse::vectorFactory::create<double>("dtkDistributedVectorData");
        rhs->resize(4);
        rhs->setAt(0,  3);
        rhs->setAt(1, -2);
        rhs->setAt(2,  5);
        rhs->setAt(3,  4);

        dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>("dtkDistributedSparseMatrixData");
        dtkDistributedMapper* mapper = new dtkDistributedMapper;

        qlonglong vertex_count = 2;
        // initMap(nbre_total_de_points, nbre_total_de_proc)
        mapper->initMap(vertex_count, comm->size());

        // setMap(global_id_of_the_first_pt_of_the_partition, id_of_the_partition)
        // stop condition : number of partitions
        mapper->setMap(0, 0);
        mapper->setMap(1, 1);

        qDebug() << " rank is "<< comm->rank() << comm->size();
        // --------------------------------
        // create graph
        dtkDistributedGraphTopology *graph_topo = new dtkDistributedGraphTopology(vertex_count, mapper);
        graph_topo->addEdge(0, 0, true);
        graph_topo->addEdge(0, 1, true);
        graph_topo->addEdge(1, 0, true);
        graph_topo->addEdge(1, 1, true);
        qDebug() << " rank is "<< comm->rank() << "build graph";
        graph_topo->build();
        // creation of graph finished
        // --------------------------------

        // sets the graph to the matrix
        qlonglong block_size = 2;
        mat->setGraph(graph_topo, block_size);

        mat->setAt(0, 0, mat_buffer.at(0));
        mat->setAt(0, 1, mat_buffer.at(1));
        mat->setAt(0, 2, mat_buffer.at(2));
        mat->setAt(0, 3, mat_buffer.at(3));
        mat->setAt(1, 0, mat_buffer.at(4));
        mat->setAt(1, 1, mat_buffer.at(5));
        mat->setAt(1, 2, mat_buffer.at(6));
        mat->setAt(1, 3, mat_buffer.at(7));
        mat->setAt(2, 0, mat_buffer.at(8));
        mat->setAt(2, 2, mat_buffer.at(10));
        mat->setAt(2, 3, mat_buffer.at(11));
        mat->setAt(3, 1, mat_buffer.at(13));
        mat->setAt(3, 2, mat_buffer.at(14));
        mat->setAt(3, 3, mat_buffer.at(15));

        dtkSparseMatrix<double>::line_iterator lit  = mat->begin();
        dtkSparseMatrix<double>::line_iterator lend = mat->end();
        
        qlonglong count = graph_topo->mapper()->firstIndex(comm->wid()) * block_size * mat->colCount();
        for(; lit != lend; ++lit) {
            dtkSparseMatrix<double>::line_iterator::element_iterator eit  = lit.begin();
            dtkSparseMatrix<double>::line_iterator::element_iterator eend = lit.end();
            for(; eit != eend; ++eit) {
                if(mat_buffer.at(count) != 0.) {
                    QCOMPARE(*eit, mat_buffer.at(count));
                } else {
                    //qDebug() << *eit;
                }
                ++count;
            }
        }

        dtkSparseSolver<double> *solver = dtkLinearAlgebraSparse::solverFactory::create<double>(solver_impl);
        solver->setMatrix(mat);
        solver->setRHSVector(rhs);

        QHash<QString, int> parameters_int;
        parameters_int.insert("number_of_iterations", 60);
        solver->setOptionalParameters(parameters_int);

        QHash<QString, double> parameters_real;
        parameters_real.insert("residual_reduction_order", 1.e-13);
        solver->setOptionalParameters(parameters_real);

        solver->run();

        dtkVector<double>& sol = *solver->solutionVector();

        for(int i = 0; i < 4; ++i)
            qDebug() << sol.at(i);

        dtkArray<double> check_rhs(4);

        const dtkSparseMatrix<double>& smat = *mat;

        for (int i = 0; i < 4; ++i) {
            check_rhs[i] = sol[0] * smat(i, 0) + sol[1] * smat(i, 1) + sol[2] * smat(i, 2) + sol[3] * smat(i, 3);
        }

        QCOMPARE(check_rhs[0] , rhs->at(0));
        QCOMPARE(check_rhs[1] , rhs->at(1));
        QCOMPARE(check_rhs[2] , rhs->at(2));
        QCOMPARE(check_rhs[3] , rhs->at(3));

        delete &sol;
        delete rhs;
        delete mat;
        delete solver;
    }
};

void dtkSparseSolverBlockTestCase::initTestCase(void)
{
    initPluginManager();

    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");

    dtkDistributed::policy()->setType("qthread");
    dtkDistributed::policy()->addHost("localhost");
}

void dtkSparseSolverBlockTestCase::init(void)
{
}

// void dtkSparseSolverBlockTestCase::testMeta_data(void)
// {
//     //    setAllImplementations();
// }

void dtkSparseSolverBlockTestCase::testJacobi(void)
{
    solveTask task;
    task.solver_impl = "dtkSparseSolverJacobiGeneric";
    dtkDistributed::spawn();
    dtkDistributed::exec(&task);
    dtkDistributed::unspawn();
}

void dtkSparseSolverBlockTestCase::testSGS(void)
{
    solveTask task;
    task.solver_impl = "dtkSparseSolverSymmetricGaussSeidelGeneric";
    dtkDistributed::spawn();
    dtkDistributed::exec(&task);
    dtkDistributed::unspawn();
}


void dtkSparseSolverBlockTestCase::cleanupTestCase(void)
{

}

void dtkSparseSolverBlockTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkSparseSolverBlockTest, dtkSparseSolverBlockTestCase)

//
// dtkSparseSolverBlockTest.cpp ends here
