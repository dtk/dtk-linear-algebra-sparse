// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseSolverJacobiGenericTest.h"

#include <dtkLinearAlgebraSparse>

#include "../dtkLinearAlgebraSparseTestCase.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkSparseSolverJacobiGenericTestCase::initTestCase(void)
{
    initPluginManager();
}

void dtkSparseSolverJacobiGenericTestCase::init(void)
{
}

void dtkSparseSolverJacobiGenericTestCase::testMeta_data(void)
{
    setAllImplementations();
}

void dtkSparseSolverJacobiGenericTestCase::testMeta(void)
{
    QFETCH(QString, vectorImpl);
    QFETCH(QString, matrixImpl);

    // System to solve
    //
    // ----       ----    --  --    --  --
    // |  7 -2  1  2 |    | x0 |    |  3 |
    // |  2  8  3  1 |    | x1 |    | -2 |
    // | -1  0  5  2 |  * | x2 |  = |  5 |
    // |  0  2 -1  4 |    | x3 |    |  4 |
    // ----       ----    --  --    --  --

    dtkVector<double> *rhs = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
    rhs->resize(4);
    rhs->setAt(0,  3);
    rhs->setAt(1, -2);
    rhs->setAt(2,  5);
    rhs->setAt(3,  4);

    dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);
    mat->resize(4);
    mat->insert(0, 0,  7);
    mat->insert(0, 1, -2);
    mat->insert(0, 2,  1);
    mat->insert(0, 3,  2);
    mat->insert(1, 0,  2);
    mat->insert(1, 1,  8);
    mat->insert(1, 2,  3);
    mat->insert(1, 3,  1);
    mat->insert(2, 0, -1);
    mat->insert(2, 2,  5);
    mat->insert(2, 3,  2);
    mat->insert(3, 1,  2);
    mat->insert(3, 2, -1);
    mat->insert(3, 3,  4);
    mat->assemble();

    dtkSparseSolver<double> *solver = dtkLinearAlgebraSparse::solverFactory::create<double>("dtkSparseSolverJacobiGeneric");
    solver->setMatrix(mat);
    solver->setRHSVector(rhs);

    QHash<QString, int> parameters_int;
    parameters_int.insert("number_of_iterations", 60);
    solver->setOptionalParameters(parameters_int);

    QHash<QString, double> parameters_real;
    parameters_real.insert("residual_reduction_order", 1.e-13);
    solver->setOptionalParameters(parameters_real);

    solver->run();

    dtkVector<double>& sol = *solver->solutionVector();

    dtkArray<double> check_rhs(4);

    const dtkSparseMatrix<double>& smat = *mat;

    for (int i = 0; i < 4; ++i) {
        check_rhs[i] = sol[0] * smat(i, 0) + sol[1] * smat(i, 1) + sol[2] * smat(i, 2) + sol[3] * smat(i, 3);
    }

    QCOMPARE(check_rhs[0] , rhs->at(0));
    QCOMPARE(check_rhs[1] , rhs->at(1));
    QCOMPARE(check_rhs[2] , rhs->at(2));
    QCOMPARE(check_rhs[3] , rhs->at(3));

    delete &sol;
    delete rhs;
    delete mat;
    delete solver;
}

void dtkSparseSolverJacobiGenericTestCase::cleanupTestCase(void)
{

}

void dtkSparseSolverJacobiGenericTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkSparseSolverJacobiGenericTest, dtkSparseSolverJacobiGenericTestCase)

//
// dtkSparseSolverJacobiGenericTest.cpp ends here
