// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

class dtkSparseSolverSymmetricGaussSeidelGenericTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMeta(void);
    void testMeta_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


//
// dtkSparseSolverSymmetricGaussSeidelGenericTest.h ends here
