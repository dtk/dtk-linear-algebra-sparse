// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

class dtkSparseSolverJacobiGenericTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMeta(void);
    void testMeta_data(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


//
// dtkSparseSolverJacobiGenericTest.h ends here
