// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkDistributed/dtkDistributedCommunicator.h>


// ///////////////////////////////////////////////////////////////////
// Ugly stuff
#include "dtkDistributedSparseMatrixData.h"

template < typename T > using dtkMaphysSparseMatrixDataLine = dtkDistributedSparseMatrixDataLine<T>;
template < typename T > using dtkMaphysSparseMatrixDataLineGlobal = dtkDistributedSparseMatrixDataLineGlobal<T>;

// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkMaphysSparseMatrixData : public dtkSparseMatrixAbstractData<T>
{
public:
    dtkMaphysSparseMatrixData(void) : dtkSparseMatrixAbstractData<T>(), m_graph(new dtkDistributedGraphTopology), m_values(0), m_positions(0), m_builded(false) {}
    dtkMaphysSparseMatrixData(const dtkMaphysSparseMatrixData& other) : dtkSparseMatrixAbstractData<T>(), m_graph(new dtkDistributedGraphTopology(*(other.m_graph))), m_values(0), m_positions(0), m_builded(other.m_builded)
    {
        if (m_builded) {
            m_values    = new dtkDistributedArray<T>(*(other.m_values));
            m_positions = new dtkDistributedArray<qlonglong>(*(other.m_positions));
        }
    }
    ~dtkMaphysSparseMatrixData(void)
    {
        // if(m_graph)
        //     delete m_graph;
        m_graph = NULL;

        if (m_values) {
            delete m_values;
        }
        m_values    = NULL;
        m_positions = NULL;
    }

public:
    dtkMaphysSparseMatrixData *clone(void) const { return new dtkMaphysSparseMatrixData<T>(*this); }

public:
    void copy(dtkSparseMatrixAbstractData<T> *other)
    {
        if (other->objectName() == this->objectName()) {
            dtkMaphysSparseMatrixData<T>& o = *static_cast<dtkMaphysSparseMatrixData<T> *>(other);
            *m_graph = *(o.m_graph);
            if (o.m_values) {
                *m_values = *(o.m_values);
            } else {
                m_values = NULL;
            }
            if (o.m_positions) {
                *m_positions = *(o.m_positions);
            } else {
                m_positions = NULL;
            }

        } else {
            dtkError() << Q_FUNC_INFO << "Incoming datatype not handled for copy (datatype: " << other->objectName() << ").";
        }
    }

public:
    dtkDistributedGraphTopology *graph(void) { return m_graph; }

    void setGraph(dtkDistributedGraphTopology *graph, qlonglong block_size)
    {
        if (m_graph)
            delete m_graph;
        if (m_values)
            delete m_values;

        m_values = NULL;

        m_graph = graph;
    }

public:
    bool readMatrixMarket(const QString& filename)
    { 
        dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
        m_values = new dtkDistributedArray<T>(comm->size());
        // dummy array, to be deleted and recreated with the good size in read
        m_builded = m_graph->readWithValues(filename, dtkDistributedGraphTopology::MatrixMarketFormat, m_values);

        

        return m_builded;
    }
    
public:
    void build(void)
    {
        m_builded = true;
        m_graph->buildFEM();
        dtkDistributedMapper *mapper = m_graph->m_fe_mapper;

        if (m_values)
            delete m_values;

        m_positions = m_graph->m_positions;

        m_values = new dtkDistributedArray<T>(mapper->count(), mapper);

        const dtkArray<qlonglong, 0>& local_v_to_e = m_graph->m_local_vertex_to_edge;
        const dtkArray<qlonglong, 0>& local_e_to_v = m_graph->m_local_edge_to_vertex;
        const dtkArray<qlonglong, 0>& loc_to_glob  = m_graph->m_loc_to_glob;

        auto it = m_values->begin();

        qlonglong size_loc = local_v_to_e.size() - 1;

        for (qlonglong i_loc = 0; i_loc < size_loc; ++i_loc) {
            qlonglong i = loc_to_glob.at(i_loc);
            qlonglong start = local_v_to_e.at(i_loc);
            qlonglong end = local_v_to_e.at(i_loc + 1);
            for (qlonglong k = start; k < end; ++k) {
                qlonglong j = local_e_to_v.at(k);
                *it = m_val_map.take(i * N + j);
                ++it;
            }
        }
        
        m_graph->communicator()->barrier();

        auto mit  = m_val_map.cbegin();
        auto mite = m_val_map.cend();
        m_values->wlock(m_graph->wid());
        m_graph->rlock();
        m_positions->rlock();
        for (; mit != mite; ++mit) {
            qlonglong i = mit.key() / N;
            qlonglong j = mit.key() % N;
            qlonglong pos;

            dtkDistributedGraphTopology::Neighbours n = (*m_graph)[i];
            dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
            dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();

            for(;nit != nend; ++nit) {
                if (*nit == j) {
                    pos = m_positions->at(nit.id());
                    break;
                }
            }
            if (pos < 0) {
                qDebug() << Q_FUNC_INFO << "Oups ! Negative position unexpected for (i,j)=" << i << j;
                continue;
            }
            if (i == j) {
                m_values->addAssign(pos, *mit);
            } else {
                m_values->setAt(pos, *mit);
            }
        }
        m_positions->unlock();
        m_graph->unlock();
        m_values->unlock(m_graph->wid());

        m_graph->communicator()->barrier();
    }

public:
    void rlock(void) {
        m_graph->rlock();
        m_values->rlock();
        m_positions->rlock();
    }
    void wlock(void) {
        m_graph->wlock();
        m_values->wlock();
        m_positions->wlock();
    }
    void unlock(void) {
        m_graph->unlock();
        m_values->unlock();
        m_positions->unlock();
    }

public:
    qlonglong     rowCount(void) const { return m_builded ? m_graph->vertexCount() : 0LL; }
    qlonglong     colCount(void) const { return m_builded ? m_graph->vertexCount() : 0LL; }
    qlonglong nonZeroCount(void) const { return m_builded ? m_graph->edgeCount() : 0LL; }
    qlonglong    blockSize(void) const { return 1LL; }

public:
    void clear(void) {
        if (m_values) {
            delete m_values;
            m_values = NULL;
        }
        m_positions = NULL;
        m_builded = false;
        m_val_map.clear();
    }
    void reset(void) {
        if (m_builded) {
            m_values->fill(T(0));
        }
    }

    void resize(qlonglong row_count, qlonglong col_count) {
        m_builded = false;
        if (row_count != col_count) {
            qCritical() << "non squared sparse matrix not yet implemented";
            return;
        }
        m_graph->resize(row_count);
        N = row_count;
    }

    void reserve(qlonglong) {}

    void setBlockSize(qlonglong) {}

    void append(qlonglong i, qlonglong j, const T& value)
    {
        m_graph->addEdgeFEM(i, j);
        //m_val_map[qMakePair<qlonglong, qlonglong>(i, j)] = value;
        m_val_map[i * N + j] = value;
    }

    void insert(qlonglong i, qlonglong j, const T& value) {
        this->append(i, j, value);
        //this->build();
    }

    void remove(qlonglong i, qlonglong j) {
        qlonglong entry = i * N + j;
        //QPair<qlonglong, qlonglong> entry(i, j);
        if (m_val_map.contains(entry)) {
            m_val_map.remove(entry);
            this->build();
        }
    }

    void setAt(qlonglong i, qlonglong j, const T& value)
    {
        if (!m_builded) {
            dtkWarn() << Q_FUNC_INFO << "Matrix has not been built. Nothing is done.";
            return;
        }

        qlonglong position = this->pos(i, j);

        if (position < 0) {
            dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
            return;
        }
        m_values->setAt(position, value);
    }

public:
    void fill(const T& value)
    {
        if (!m_builded) {
            this->build();
        }
        m_values->fill(value);
    }

public:
    T at(qlonglong i, qlonglong j) const
    {
        qlonglong position = this->pos(i, j);
        if (position < 0) {
            return T(0);
        }
        return m_values->at(position);
    }

public:
    void addAssign(qlonglong i, qlonglong j, const T& value)
    {
        if (!m_builded) {
            dtkWarn() << Q_FUNC_INFO << "Matrix has not been built. Nothing is done.";
            return;
        }

        qlonglong position = this->pos(i, j);
        if (position < 0) {
            dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
            return;
        }
        m_values->addAssign(position, value);
    }

    void subAssign(qlonglong i, qlonglong j, const T& value)
    {
        if (!m_builded) {
            dtkWarn() << Q_FUNC_INFO << "Matrix has not been built. Nothing is done.";
            return;
        }

        qlonglong position = this->pos(i, j);
        if (position < 0) {
            dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
            return;
        }
        m_values->addAssign(position, -value);
    }

    void mulAssign(qlonglong i, qlonglong j, const T& value)
    {
        if (!m_builded) {
            dtkWarn() << Q_FUNC_INFO << "Matrix has not been built. Nothing is done.";
            return;
        }

        qlonglong position = this->pos(i, j);
        if (position < 0) {
            dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
            return;
        }
        m_values->mulAssign(position, value);
    }

    void divAssign(qlonglong i, qlonglong j, const T& value)
    {
        if (!m_builded) {
            dtkWarn() << Q_FUNC_INFO << "Matrix has not been built. Nothing is done.";
            return;
        }

        qlonglong position = this->pos(i, j);
        if (position < 0) {
            dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
            return;
        }
        m_values->divAssign(position, value);
    }

public:
    typedef dtkSparseMatrixLine<T> line_iterator;

    line_iterator  begin(void) const { return line_iterator( new dtkMaphysSparseMatrixDataLine<T>(m_values, m_graph, m_graph->mapper()->firstIndex(m_graph->wid()), 1LL) ); }
    line_iterator    end(void) const { return line_iterator( new dtkDistributedSparseMatrixDataLine<T>(m_values, m_graph, 1LL) ); }

public:
    line_iterator lineAt(qlonglong line_id) const { return line_iterator( new dtkMaphysSparseMatrixDataLineGlobal<T>(m_values, m_graph, line_id, 1LL) ); }

public:
    T operator[](qlonglong raw_id) const { return m_values->at(raw_id); }

public:
    const T *data(void) const { return m_values->data(); }
          T *data(void)       { return m_values->data(); }

protected:
    qlonglong pos(qlonglong i, qlonglong j) const
    {
        qlonglong i_loc = m_graph->m_glob_to_loc.value(i, -1LL);

        // if(m_graph->wid() == 1)
        // qDebug() << "loc" << i_loc << i << j;

        if (i_loc > -1) {

            qlonglong offset = m_graph->m_fe_mapper->firstIndex(m_graph->wid());
            const dtkArray<qlonglong, 0>& local_v_to_e = m_graph->m_local_vertex_to_edge;
            const dtkArray<qlonglong, 0>& local_e_to_v = m_graph->m_local_edge_to_vertex;

            qlonglong start = local_v_to_e.at(i_loc);
            qlonglong end = local_v_to_e.at(i_loc + 1);
            for (qlonglong k = start; k < end; ++k) {
                if (j == local_e_to_v.at(k)) {
                    // if(m_graph->wid() == 1)
                    // qDebug() << "pos(i,j):" << i << j << k << start << end ;
                    return k + offset;
                }
            }
        }

        dtkDistributedGraphTopology::Neighbours n = (*m_graph)[i];
        dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
        dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();

        for(;nit != nend; ++nit) {
            if (*nit == j) {
                    // if(m_graph->wid() == 1)
                    //     qDebug() << nit.id() << *nit << m_positions->at(nit.id());
                return m_positions->at(nit.id());
            }
        }

        return -1LL;
    }

protected:
    qlonglong N;
    typedef QHash<qlonglong, T> Map;
    Map m_val_map;

    dtkDistributedGraphTopology    *m_graph;
    dtkDistributedArray<T>         *m_values;
    dtkDistributedArray<qlonglong> *m_positions;

    bool m_builded;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > dtkSparseMatrixAbstractData<T> *dtkMaphysSparseMatrixDataCreator(void)
{
    return new dtkMaphysSparseMatrixData<T>();
}

// // ///////////////////////////////////////////////////////////////////
// // dtkMaphysSparseMatrixDataLine
// // ///////////////////////////////////////////////////////////////////

// template <typename T> class dtkMaphysSparseMatrixDataLine : public dtkSparseMatrixLineBase < T, dtkMaphysSparseMatrixDataLine<T> >
// {
//     qlonglong m_id;
//     dtkDistributedGraphTopology::vertex v_it;
//     typename dtkDistributedArray<T>::const_iterator val_it;
//     qlonglong m_block_size;
//     qlonglong m_line_size;

// public:
//     dtkMaphysSparseMatrixDataLine(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph, qlonglong id, qlonglong block_size);
//     dtkMaphysSparseMatrixDataLine(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph, qlonglong block_size);

// public:
//     dtkMaphysSparseMatrixDataLine(const dtkMaphysSparseMatrixDataLine& o);

// public:
//     ~dtkMaphysSparseMatrixDataLine(void);

// public:
//     dtkMaphysSparseMatrixDataLine& operator = (const dtkMaphysSparseMatrixDataLine& o);

// public:
//     qlonglong id(void) const;

// public:
//     qlonglong  size(void) const;
//     qlonglong width(void) const;

// public:
//     T elementValue(qlonglong id) const;
//     qlonglong elementId(qlonglong id) const;

// public:
//     bool operator == (const dtkMaphysSparseMatrixDataLine& o) const;
//     bool operator != (const dtkMaphysSparseMatrixDataLine& o) const;
//     bool operator <  (const dtkMaphysSparseMatrixDataLine& o) const;
//     bool operator <= (const dtkMaphysSparseMatrixDataLine& o) const;
//     bool operator >  (const dtkMaphysSparseMatrixDataLine& o) const;
//     bool operator >= (const dtkMaphysSparseMatrixDataLine& o) const;

// public:
//     dtkMaphysSparseMatrixDataLine& operator ++ (void);
//     dtkMaphysSparseMatrixDataLine  operator ++ (int);
//     dtkMaphysSparseMatrixDataLine& operator -- (void);
//     dtkMaphysSparseMatrixDataLine  operator -- (int);
//     dtkMaphysSparseMatrixDataLine& operator += (qlonglong j);
//     dtkMaphysSparseMatrixDataLine& operator -= (qlonglong j);
//     dtkMaphysSparseMatrixDataLine  operator +  (qlonglong j) const;
//     dtkMaphysSparseMatrixDataLine  operator -  (qlonglong j) const;

// protected:
//     void advance(void);
//     void advance(qlonglong j);
//     void rewind(void);
//     void rewind(qlonglong j);
// };

// // ///////////////////////////////////////////////////////////////////
// // dtkMaphysSparseMatrixDataLineGlobal
// // ///////////////////////////////////////////////////////////////////

// template <typename T> class dtkMaphysSparseMatrixDataLineGlobal : public dtkSparseMatrixLineBase < T, dtkMaphysSparseMatrixDataLineGlobal<T> >
// {
//     dtkDistributedGraphTopology *m_graph;
//     dtkDistributedArray<T>      *m_array;
//     qlonglong m_vid;
//     qlonglong m_id;
//     qlonglong m_block_size;
//     qlonglong m_line_start_pos;
//     qlonglong m_line_size;

// public:
//     dtkMaphysSparseMatrixDataLineGlobal(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph, qlonglong id, qlonglong block_size);

// public:
//     dtkMaphysSparseMatrixDataLineGlobal(const dtkMaphysSparseMatrixDataLineGlobal& o);

// public:
//     ~dtkMaphysSparseMatrixDataLineGlobal(void);

// public:
//     dtkMaphysSparseMatrixDataLineGlobal& operator = (const dtkMaphysSparseMatrixDataLineGlobal& o);

// public:
//     qlonglong id(void) const;

// public:
//     qlonglong  size(void) const;
//     qlonglong width(void) const;

// public:
//     T elementValue(qlonglong id) const;
//     qlonglong elementId(qlonglong id) const;

// public:
//     bool operator == (const dtkMaphysSparseMatrixDataLineGlobal& o) const;
//     bool operator != (const dtkMaphysSparseMatrixDataLineGlobal& o) const;
//     bool operator <  (const dtkMaphysSparseMatrixDataLineGlobal& o) const;
//     bool operator <= (const dtkMaphysSparseMatrixDataLineGlobal& o) const;
//     bool operator >  (const dtkMaphysSparseMatrixDataLineGlobal& o) const;
//     bool operator >= (const dtkMaphysSparseMatrixDataLineGlobal& o) const;

// public:
//     dtkMaphysSparseMatrixDataLineGlobal& operator ++ (void);
//     dtkMaphysSparseMatrixDataLineGlobal  operator ++ (int);
//     dtkMaphysSparseMatrixDataLineGlobal& operator -- (void);
//     dtkMaphysSparseMatrixDataLineGlobal  operator -- (int);
//     dtkMaphysSparseMatrixDataLineGlobal& operator += (qlonglong j);
//     dtkMaphysSparseMatrixDataLineGlobal& operator -= (qlonglong j);
//     dtkMaphysSparseMatrixDataLineGlobal  operator +  (qlonglong j) const;
//     dtkMaphysSparseMatrixDataLineGlobal  operator -  (qlonglong j) const;

// protected:
//     void advance(void);
//     void advance(qlonglong j);
//     void rewind(void);
//     void rewind(qlonglong j);
// };

//
// dtkMaphysSparseMatrixData.h ends here
