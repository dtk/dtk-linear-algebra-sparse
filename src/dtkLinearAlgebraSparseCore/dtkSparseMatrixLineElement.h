// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <QtCore>

template <typename T> class dtkSparseMatrixAbstractLine;

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixLineElement
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkSparseMatrixLineElement
{
    const dtkSparseMatrixAbstractLine<T> *l;
    qlonglong m_id;

public:
    typedef qlonglong difference_type;
    typedef T value_type;

public:
    //explicit dtkSparseMatrixLineElement(dtkSparseMatrixAbstractLineElement<T> *element = 0);
    explicit dtkSparseMatrixLineElement(const dtkSparseMatrixAbstractLine<T> *line, qlonglong id);

public:
    dtkSparseMatrixLineElement(const dtkSparseMatrixLineElement& o);
    //dtkSparseMatrixLineElement(dtkSparseMatrixLineElement&& o);

public:
    ~dtkSparseMatrixLineElement(void);

public:
    dtkSparseMatrixLineElement& operator = (const dtkSparseMatrixLineElement& o);
    //dtkSparseMatrixLineElement& operator = (dtkSparseMatrixLineElement&& o);

public:
    qlonglong id(void) const;
    qlonglong lineId(void) const;

public:
    T operator *  (void) const;
    T operator [] (qlonglong j) const;

public:
    bool operator == (const dtkSparseMatrixLineElement& o) const;
    bool operator != (const dtkSparseMatrixLineElement& o) const;
    bool operator <  (const dtkSparseMatrixLineElement& o) const;
    bool operator <= (const dtkSparseMatrixLineElement& o) const;
    bool operator >  (const dtkSparseMatrixLineElement& o) const;
    bool operator >= (const dtkSparseMatrixLineElement& o) const;

public:
    dtkSparseMatrixLineElement& operator ++ (void);
    dtkSparseMatrixLineElement  operator ++ (int);
    dtkSparseMatrixLineElement& operator -- (void);
    dtkSparseMatrixLineElement  operator -- (int);
    dtkSparseMatrixLineElement& operator += (qlonglong j);
    dtkSparseMatrixLineElement& operator -= (qlonglong j);
    dtkSparseMatrixLineElement  operator +  (qlonglong j) const;
    dtkSparseMatrixLineElement  operator -  (qlonglong j) const;
};

// ///////////////////////////////////////////////////////////////////

#include "dtkSparseMatrixLineElement.tpp"

// 
// dtkSparseMatrixLineElement.h ends here
