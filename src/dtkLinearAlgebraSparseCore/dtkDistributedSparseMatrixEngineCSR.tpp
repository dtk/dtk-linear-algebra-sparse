// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineCSR implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkDistributedSparseMatrixEngineCSR<T>::dtkDistributedSparseMatrixEngineCSR(void) : dtkSparseMatrixEngine<T>(), is_assembled(false), _N(0), m_graph(NULL), m_values(NULL), m_current_id(-1LL)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSR<T>::dtkDistributedSparseMatrixEngineCSR(const dtkDistributedSparseMatrixEngineCSR<T>& o) : dtkSparseMatrixEngine<T>(), is_assembled(o.is_assembled), _N(o._N), m_graph(o.m_graph), m_values(new dtkDistributedArray<T>(*(o.m_values))), m_current_id(-1LL)
{
    m_graph->ref();
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSR<T>::~dtkDistributedSparseMatrixEngineCSR(void)
{
    this->clear();
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSR<T>::copy(const dtkSparseMatrixEngine<T> *other)
{
    if (other->objectName() == this->objectName()) {
        const dtkDistributedSparseMatrixEngineCSR<T>& o = *static_cast<const dtkDistributedSparseMatrixEngineCSR<T> *>(other);
        m_val_hash = o.m_val_hash;
        _N = o._N;
        is_assembled = o.is_assembled;

        if (o.m_graph) {
            m_graph = o.m_graph;
            m_graph->ref();
        } else {
            m_graph = NULL;
        }
        if (o.m_values) {
            if (m_values) {
                *m_values = *(o.m_values);
            } else {
                m_values = new dtkDistributedArray<T>(*(o.m_values));
            }
        } else {
            m_values = NULL;
        }
        return true;

    } else {
        if (other->graph()) {
            this->setGraph(other->graph());

            dtkDistributedGraphTopology::vertex vit  = m_graph->beginVertex();
            dtkDistributedGraphTopology::vertex vite = m_graph->endVertex();

            auto it = m_values->begin();
            for (; vit != vite; ++vit) {
                auto nit  = vit.begin();
                auto nite = vit.end();
                for (; nit != nite; ++nit, ++it) {
                    *it = other->at(vit.id(), *nit);
                }
            }
            m_graph->communicator()->barrier();
            return true;
        }
    }

    m_current_id = -1LL;

    return false;
}

template < typename T > inline dtkDistributedGraphTopology *dtkDistributedSparseMatrixEngineCSR<T>::graph(void) const
{
    return m_graph;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::setGraph(dtkDistributedGraphTopology *graph)
{
    if (!graph)
        return;

    this->clear();

    m_graph = graph;
    m_graph->ref();

    _N = m_graph->size();

    if (m_graph->isAssembled()) {
        m_values = new dtkDistributedArray<T>(m_graph->edgeCount(), m_graph->edgeMapper());
        is_assembled = true;
    }
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSR<T>::readMatrixMarket(const QString& filename)
{
    this->clear();

    m_graph = new dtkDistributedGraphTopology;

    // dummy array, to be deleted and recreated with the good size in read
    m_values = new dtkDistributedArray<T>(m_graph->communicator()->size());

    is_assembled = m_graph->readWithValues(filename, dtkDistributedGraphTopology::MatrixMarketFormat, m_values);

    if (is_assembled) {
        _N = m_graph->size();

    } else {
        this->clear();
    }

    return is_assembled;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::clear(void)
{
    if(is_assembled) {
        m_graph->unlock();
        is_assembled = false;
    }

    m_current_id = -1LL;
    _N = 0LL;
    m_val_hash.clear();

    if (m_graph && !m_graph->deref()) {
        delete m_graph;
    }
    m_graph = NULL;


    if (m_values) {
        delete m_values;
    }
    m_values = NULL;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::resize(qlonglong line_count)
{
    if (_N == line_count)
        return;

    this->clear();

    _N = line_count;
    m_graph = new dtkDistributedGraphTopology(_N);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::insert(qlonglong i, qlonglong j, const T& value)
{
    if (is_assembled) {
        qDebug() << Q_FUNC_INFO << "Try to insert into an already assembled matrix. Nothing is done.";
        return;
    }

    m_val_hash.insert(i * _N + j, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::remove(qlonglong i, qlonglong j)
{
    if (is_assembled) {
        qDebug() << Q_FUNC_INFO << "Try to remove from an already assembled matrix. Nothing is done.";
        return;
    }

    m_val_hash.remove(i * _N + j);
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSR<T>::assemble(void)
{
    if (is_assembled) {
        return false;
    }
    m_current_id = -1LL;

    if (!m_graph) {
        m_graph = new dtkDistributedGraphTopology(_N);
    } else {
        if (m_graph->size() != _N)
            return false;
    }

    auto it  = m_val_hash.cbegin();
    auto end = m_val_hash.cend();
    for(; it != end; ++it) {
        qlonglong i = it.key() / _N;
        qlonglong j = it.key() % _N;
        m_graph->addEdge(i, j);
    }
    m_graph->assemble();

    if (m_values)
        delete m_values;

    m_values = new dtkDistributedArray<T>(m_graph->edgeCount(), m_graph->edgeMapper());
    m_values->fill(T(0));

    it = m_val_hash.cbegin();
    m_values->wlock(m_graph->wid());
    m_graph->rlock();
    for (; it != end; ++it) {
        qlonglong i = it.key() / _N;
        qlonglong j = it.key() % _N;
        qlonglong pos;

        dtkDistributedGraphTopology::Neighbours n = (*m_graph)[i];
        dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
        dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();

        for(;nit != nend; ++nit) {
            if (*nit == j) {
                pos = nit.id();
                break;
            }
        }
        if (pos < 0) {
            qDebug() << Q_FUNC_INFO << "Oups ! Negative position unexpected for (i,j)=" << i << j;
            continue;
        }
        if (i == j) {
            m_values->addAssign(pos, *it);
        } else{
            m_values->setAt(pos, *it);
        }
    }
    m_graph->unlock();
    m_values->unlock(m_graph->wid());

    m_val_hash.clear();
    m_graph->communicator()->barrier();

    m_graph->rlock();
    is_assembled = true;
    return true;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::fill(const T& value)
{
    if (!is_assembled) {
        dtkWarn() << "Try to fill a not already assembled matrix. Nothing is done.";
        return;
    }
    m_values->fill(value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::mult(const T& value)
{
    if (!is_assembled) {
        dtkWarn()  << "Try to fill a not already assembled matrix. Nothing is done.";
        return;
    }
    *m_values *= value;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::add(dtkSparseMatrixEngine<T>* other)
{
    if (!is_assembled) {
        dtkWarn() << "Try to add a not already assembled matrix. Nothing is done.";
        return;
    }

    // current implementation only work if we have the same graph
    if ( (other->objectName() == this->objectName()) ) {
        const dtkDistributedSparseMatrixEngineCSR<T>& o = *static_cast<const dtkDistributedSparseMatrixEngineCSR<T> *>(other);

        *m_values += *(o.m_values);
    } else {
        qCritical() << "Not the same graph, can't add sparse matrixes";
    }

}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::sub(dtkSparseMatrixEngine<T>* other)
{
    if (!is_assembled) {
        dtkWarn()  << "Try to sub a not already assembled matrix. Nothing is done.";
        return;
    }

    // current implementation only work if we have the same graph
    if ( (other->objectName() == this->objectName()) && (m_graph == other->graph())) {
        const dtkDistributedSparseMatrixEngineCSR<T>& o = *static_cast<const dtkDistributedSparseMatrixEngineCSR<T> *>(other);

        *m_values -= *(o.m_values);
    } else {
        qCritical() << "Not the same graph, can't add sparse matrixes";
    }
}

template < typename T > inline T dtkDistributedSparseMatrixEngineCSR<T>::at(qlonglong i, qlonglong j) const
{
    qlonglong position = this->pos(i, j);
    if (position < 0) {
        return T(0);
    }
    return m_values->at(position);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::assign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->setAt(position, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::addAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        qlonglong index= i * _N + j;
        if (m_val_hash.contains(index)) {
            T old = m_val_hash.value(index);
            m_val_hash.insert(index, old + value);
        } else {
            m_val_hash.insert(index, value);
        }

        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->addAssign(position, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::subAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->addAssign(position, -value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::mulAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->mulAssign(position, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSR<T>::divAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->divAssign(position, value);
}

template < typename T > inline typename dtkDistributedSparseMatrixEngineCSR<T>::line_iterator dtkDistributedSparseMatrixEngineCSR<T>::begin(void) const
{
    return line_iterator( new dtkDistributedSparseMatrixEngineCSRLine<T>(m_values, m_graph, m_graph->mapper()->firstIndex(m_graph->wid())) );
}

template < typename T > inline typename dtkDistributedSparseMatrixEngineCSR<T>::line_iterator dtkDistributedSparseMatrixEngineCSR<T>::end(void) const
{
    return line_iterator( new dtkDistributedSparseMatrixEngineCSRLine<T>(m_values, m_graph) );
}

template < typename T > inline typename dtkDistributedSparseMatrixEngineCSR<T>::line_iterator dtkDistributedSparseMatrixEngineCSR<T>::lineAt(qlonglong line_id) const
{
    return line_iterator( new dtkDistributedSparseMatrixEngineCSRLineGlobal<T>(m_values, m_graph, line_id) );
}

template < typename T > inline const T *dtkDistributedSparseMatrixEngineCSR<T>::data(void) const
{
    return m_values->data();
}

template < typename T > inline T *dtkDistributedSparseMatrixEngineCSR<T>::data(void)
{
    return m_values->data();
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSR<T>::pos(qlonglong i, qlonglong j) const
{
    if (m_current_id != i) {
        m_current_id = i;
        m_current_n = (*m_graph)[i];
        m_current_it = m_current_n.begin();
    }

    dtkDistributedGraphTopology::Neighbours& n = m_current_n;
    dtkDistributedGraphTopology::Neighbours::iterator& nit  = m_current_it;
    dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();

    for(;nit != nend; ++nit) {
        if (*nit == j) {
            return nit.id();
        }
    }

    m_current_it = m_current_n.begin();

    for(;nit != nend; ++nit) {
        if (*nit == j) {
            return nit.id();
        }
    }

    return -1LL;
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineCSRLine implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>::dtkDistributedSparseMatrixEngineCSRLine(dtkDistributedArray<T> *values, dtkDistributedGraphTopology *graph, qlonglong id) : m_id(id), v_it(graph, m_id), val_it(values->cbegin() + v_it.neighbourLocalPos(0)), m_line_size(v_it.neighbourCount())
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>::dtkDistributedSparseMatrixEngineCSRLine(dtkDistributedArray<T> *values, dtkDistributedGraphTopology *graph) : m_id(graph->mapper()->lastIndex(graph->wid())), v_it(graph, m_id), val_it(values->cend()), m_line_size(0)
{
    ++m_id;
    ++v_it;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>::dtkDistributedSparseMatrixEngineCSRLine(const dtkDistributedSparseMatrixEngineCSRLine& o) : v_it(o.v_it), val_it(o.val_it), m_id(o.m_id), m_line_size(o.m_line_size)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>::~dtkDistributedSparseMatrixEngineCSRLine(void)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>& dtkDistributedSparseMatrixEngineCSRLine<T>::operator = (const dtkDistributedSparseMatrixEngineCSRLine& o)
{
    m_id = o.m_id;
    v_it = o.v_it;
    val_it = o.val_it;
    m_line_size = o.m_line_size;
    return *this;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLine<T>::id(void) const
{
    return m_id;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLine<T>::size(void) const
{
    return m_line_size;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLine<T>::width(void) const
{
    if (m_line_size > 1) {
        return  (*(--(v_it.end())) - *(v_it.begin()));
    }

    return 0LL;
}

template < typename T > inline T dtkDistributedSparseMatrixEngineCSRLine<T>::elementValue(qlonglong id) const
{
    return *(val_it + id);
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLine<T>::elementId(qlonglong id) const
{
    return *(v_it.begin() + id);
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLine<T>::operator == (const dtkDistributedSparseMatrixEngineCSRLine& o) const
{
    return m_id == o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLine<T>::operator != (const dtkDistributedSparseMatrixEngineCSRLine& o) const
{
    return m_id != o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLine<T>::operator < (const dtkDistributedSparseMatrixEngineCSRLine& o) const
{
    return m_id < o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLine<T>::operator <= (const dtkDistributedSparseMatrixEngineCSRLine& o) const
{
    return m_id <= o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLine<T>::operator > (const dtkDistributedSparseMatrixEngineCSRLine& o) const
{
    return m_id > o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLine<T>::operator >= (const dtkDistributedSparseMatrixEngineCSRLine& o) const
{
    return m_id >= o.m_id;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>& dtkDistributedSparseMatrixEngineCSRLine<T>::operator ++ (void)
{
    this->advance();
    return *this;
}

 template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T> dtkDistributedSparseMatrixEngineCSRLine<T>::operator ++ (int)
{
    dtkDistributedSparseMatrixEngineCSRLine o(*this);
    this->advance();
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>& dtkDistributedSparseMatrixEngineCSRLine<T>::operator -- (void)
{
    this->rewind();
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T> dtkDistributedSparseMatrixEngineCSRLine<T>::operator -- (int)
{
    dtkDistributedSparseMatrixEngineCSRLine o(*this);
    this->rewind();
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>& dtkDistributedSparseMatrixEngineCSRLine<T>::operator += (qlonglong j)
{
    this->advance(j);
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T>& dtkDistributedSparseMatrixEngineCSRLine<T>::operator -= (qlonglong j)
{
    this->rewind(j);
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T> dtkDistributedSparseMatrixEngineCSRLine<T>::operator + (qlonglong j) const
{
    dtkDistributedSparseMatrixEngineCSRLine o(*this);
    o += j;
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLine<T> dtkDistributedSparseMatrixEngineCSRLine<T>::operator - (qlonglong j) const
{
    dtkDistributedSparseMatrixEngineCSRLine o(*this);
    o -= j;
    return o;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLine<T>::advance(void)
{
    val_it += m_line_size;
    ++m_id;
    ++v_it;
    m_line_size = v_it.neighbourCount();
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLine<T>::rewind(void)
{
    --m_id;
    --v_it;
    m_line_size = v_it.neighbourCount();
    val_it -= m_line_size;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLine<T>::advance(qlonglong j)
{
    for (qlonglong i = 0; i < j; ++i) {
        this->advance();
    }
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLine<T>::rewind(qlonglong j)
{
    for (qlonglong i = 0; i < j; ++i) {
        this->rewind();
    }
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineCSRLineGlobal implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::dtkDistributedSparseMatrixEngineCSRLineGlobal(dtkDistributedArray<T> *values, dtkDistributedGraphTopology *graph, qlonglong id) : m_graph(graph), m_array(values), m_id(id)
{
    m_line_size = m_graph->neighbourCount(m_id);
    m_line_start_pos = m_graph->firstNeighbourPos(m_id);
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::dtkDistributedSparseMatrixEngineCSRLineGlobal(dtkDistributedArray<T> *values, dtkDistributedGraphTopology *graph) : m_graph(graph), m_array(values)
{
    m_id = m_graph->mapper()->lastIndex(m_graph->wid());
    m_line_size = m_graph->neighbourCount(m_id);
    m_line_start_pos = m_graph->firstNeighbourPos(m_id) + m_line_size;
    ++m_id;
    m_line_size = 0;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::dtkDistributedSparseMatrixEngineCSRLineGlobal(const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) : m_graph(o.m_graph), m_array(o.m_array), m_id(o.m_id), m_line_start_pos(o.m_line_start_pos), m_line_size(o.m_line_size)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::~dtkDistributedSparseMatrixEngineCSRLineGlobal(void)
{
    m_graph = NULL;
    m_array = NULL;
    m_id = -1;
    m_line_start_pos = -1;
    m_line_size = -1;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>& dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator = (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o)
{
    m_graph = o.m_graph;
    m_array = o.m_array;
    m_id = o.m_id;
    m_line_start_pos = o.m_line_start_pos;
    m_line_size = o.m_line_size;
    return *this;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::id(void) const
{
    return m_id;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::size(void) const
{
    return m_line_size;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::width(void) const
{
    if (m_line_size > 1) {
        dtkDistributedGraphTopology::Neighbours n = (*m_graph)[m_id];
        return (*(--(n.end())) - *(n.begin()));
    }

    return 0LL;
}

template < typename T > inline T dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::elementValue(qlonglong id) const
{
    return m_array->at(m_line_start_pos + id);
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::elementId(qlonglong id) const
{
    qlonglong pos_J = id % m_line_size;
    dtkDistributedGraphTopology::Neighbours n = (*m_graph)[m_id];
    qlonglong J = *(n.begin() + pos_J);
    return J;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator == (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const
{
    return m_id == o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator != (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const
{
    return m_id != o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator < (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const
{
    return m_id < o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator <= (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const
{
    return m_id <= o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator > (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const
{
    return m_id > o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator >= (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const
{
    return m_id >= o.m_id;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>& dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator ++ (void)
{
    this->advance();
    return *this;
}

 template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T> dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator ++ (int)
{
    dtkDistributedSparseMatrixEngineCSRLineGlobal o(*this);
    this->advance();
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>& dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator -- (void)
{
    this->rewind();
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T> dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator -- (int)
{
    dtkDistributedSparseMatrixEngineCSRLineGlobal o(*this);
    this->rewind();
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>& dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator += (qlonglong j)
{
    this->advance(j);
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T>& dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator -= (qlonglong j)
{
    this->rewind(j);
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T> dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator + (qlonglong j) const
{
    dtkDistributedSparseMatrixEngineCSRLineGlobal o(*this);
    o += j;
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineCSRLineGlobal<T> dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::operator - (qlonglong j) const
{
    dtkDistributedSparseMatrixEngineCSRLineGlobal o(*this);
    o -= j;
    return o;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::advance(void)
{
    m_line_start_pos += m_line_size;
    ++m_id;
    m_line_size = m_id != m_graph->size() ? m_graph->neighbourCount(m_id) : 0LL;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::rewind(void)
{
    --m_id;
    m_line_size = m_id >= 0  ? m_graph->neighbourCount(m_id) : 0LL;
    m_line_start_pos -= m_line_size;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::advance(qlonglong j)
{
    for (qlonglong i = 0; i < j; ++i) {
        this->advance();
    }
}

template < typename T > inline void dtkDistributedSparseMatrixEngineCSRLineGlobal<T>::rewind(qlonglong j)
{
    for (qlonglong i = 0; i < j; ++i) {
        this->rewind();
    }
}


//
// dtkDistributedSparseMatrixEngineCSR.tpp ends here
