// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include "dtkSparseMatrixEngine.h"

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixProxy
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkSparseMatrixProxy
{
    dtkSparseMatrixEngine<T> *data;
    qlonglong m_i;
    qlonglong m_j;

public:
    explicit dtkSparseMatrixProxy(dtkSparseMatrixEngine<T> *d, qlonglong i, qlonglong j);
             dtkSparseMatrixProxy(const dtkSparseMatrixProxy& o);

public:
    ~dtkSparseMatrixProxy(void);

public:
    dtkSparseMatrixProxy& operator = (const dtkSparseMatrixProxy& o);

public:
    T value(void) const;

public:
    dtkSparseMatrixProxy& operator =  (const T& value);
    dtkSparseMatrixProxy& operator += (const T& value);
    dtkSparseMatrixProxy& operator -= (const T& value);
    dtkSparseMatrixProxy& operator *= (const T& value);
    dtkSparseMatrixProxy& operator /= (const T& value);

public:
    operator T() const;
};

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixProxy operators
// ///////////////////////////////////////////////////////////////////

template < typename T > bool operator == (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator == (const dtkSparseMatrixProxy<T>& lhs, const T& rhs);
template < typename T > bool operator == (const T& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator != (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator != (const dtkSparseMatrixProxy<T>& lhs, const T& rhs);
template < typename T > bool operator != (const T& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator <  (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator <  (const dtkSparseMatrixProxy<T>& lhs, const T& rhs);
template < typename T > bool operator <  (const T& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator >  (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator >  (const dtkSparseMatrixProxy<T>& lhs, const T& rhs);
template < typename T > bool operator >  (const T& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator <= (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator <= (const dtkSparseMatrixProxy<T>& lhs, const T& rhs);
template < typename T > bool operator <= (const T& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator >= (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > bool operator >= (const dtkSparseMatrixProxy<T>& lhs, const T& rhs);
template < typename T > bool operator >= (const T& lhs, const dtkSparseMatrixProxy<T>& rhs);
template < typename T > QDebug& operator << (QDebug dbg, const dtkSparseMatrixProxy<T>& proxy);

// ///////////////////////////////////////////////////////////////////

#include "dtkSparseMatrixProxy.tpp"

//
// dtkSparseMatrixProxy.h ends here
