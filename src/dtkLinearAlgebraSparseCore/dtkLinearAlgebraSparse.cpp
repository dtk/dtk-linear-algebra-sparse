// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkConfig.h>

#include "dtkLinearAlgebraSparse.h"
#include "dtkLinearAlgebraSparseConfig.h"
#include <dtkLinearAlgebraSparseCoreExport.h>
#include "dtkLinearAlgebraSparseSettings"
#include "dtkVectorData.h"
#include "dtkSparseSolverJacobi.h"
#include "dtkSparseSolverJacobiSOR.h"
#include "dtkSparseSolverSymmetricGaussSeidelGeneric.h"
#include "dtkSparseSystemBuilder.h"

#if defined(DTK_BUILD_DISTRIBUTED)
#include "dtkDistributedSparseMatrixEngineCSR.h"
#include "dtkDistributedSparseMatrixEngineDD.h"
#include "dtkDistributedVectorData.h"
#endif

namespace dtkLinearAlgebraSparse
{
    namespace pluginManager {

        void initialize(QString path)
        {
            QString realpath = path;

            if (path.isEmpty()) {
                dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
                linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");
                realpath = linear_algebra_sparse_settings.value("plugins").toString();
                linear_algebra_sparse_settings.endGroup();

                if (realpath.isEmpty()) {
                    realpath = QDir(DTK_INSTALL_PREFIX).filePath("plugins/dtkLinearAlgebraSparse");
                    dtkDebug() << "no plugin path configured, use default:" << realpath ;
                }
            }

            dtkLinearAlgebraSparse::matrixEngine::initialize(realpath);
            dtkLinearAlgebraSparse::vectorData::initialize(realpath);
            dtkLinearAlgebraSparse::solver::initialize(realpath);
            dtkLinearAlgebraSparse::preconditioner::initialize(realpath);
        }

        void setVerboseLoading(bool verbose)
        {
            dtkLinearAlgebraSparse::matrixEngine::pluginManager().setVerboseLoading(verbose);
            dtkLinearAlgebraSparse::vectorData::pluginManager().setVerboseLoading(verbose);
            dtkLinearAlgebraSparse::solver::pluginManager().setVerboseLoading(verbose);
            dtkLinearAlgebraSparse::preconditioner::pluginManager().setVerboseLoading(verbose);
        }

        void setAutoLoading(bool auto_load)
        {
            dtkLinearAlgebraSparse::matrixEngine::pluginManager().setAutoLoading(auto_load);
            dtkLinearAlgebraSparse::vectorData::pluginManager().setAutoLoading(auto_load);
            dtkLinearAlgebraSparse::solver::pluginManager().setAutoLoading(auto_load);
            dtkLinearAlgebraSparse::preconditioner::pluginManager().setAutoLoading(auto_load);
        }
    }

    namespace matrixEngine {

        namespace _private {
            dtkSparseMatrixEnginePluginManager manager;
            dtkSparseMatrixEnginePluginFactoryTemplate factoryTemplate;
        }

        void initialize(const QString& path)
        {
#if defined(DTK_BUILD_DISTRIBUTED)
            pluginFactory<double>().record("dtkDistributedSparseMatrixEngineCSR", dtkDistributedSparseMatrixEngineCSRCreator<double>);
            pluginFactory<double>().record("dtkDistributedSparseMatrixEngineDD", dtkDistributedSparseMatrixEngineDDCreator<double>);
#endif
            pluginManager().initialize(path);
            pluginManager().setLayerVersion(QString(DTKLINEARALGEBRASPARSEVERSION_STR));
        }

        dtkSparseMatrixEnginePluginManager& pluginManager(void) {
            return _private::manager;
        }

        dtkSparseMatrixEnginePluginFactoryTemplate& pluginFactoryTemplate(void) {
            return _private::factoryTemplate;
        }
    }

    namespace vectorData {

        namespace _private {
            dtkVectorAbstractDataPluginManager manager;
            dtkVectorAbstractDataPluginFactoryTemplate factoryTemplate;
        }

        void initialize(const QString& path) {
            pluginFactory<double>().record("dtkVectorData", dtkVectorDataCreator<double>);
#if defined(DTK_BUILD_DISTRIBUTED)
            pluginFactory<double>().record("dtkDistributedVectorData", dtkDistributedVectorDataCreator<double>);
#endif
            pluginManager().initialize(path);
            pluginManager().setLayerVersion(QString(DTKLINEARALGEBRASPARSEVERSION_STR));
        }

        dtkVectorAbstractDataPluginManager& pluginManager(void) {
            return _private::manager;
        }

        dtkVectorAbstractDataPluginFactoryTemplate& pluginFactoryTemplate(void) {
            return _private::factoryTemplate;
        }
    }

    namespace solver {

        namespace _private {
            dtkSparseSolverPluginManager manager;
            dtkSparseSolverPluginFactoryTemplate factoryTemplate;
        }

        void initialize(const QString& path) {
            pluginFactory<double>().record("dtkSparseSolverJacobiGeneric", dtkSparseSolverJacobiGenericCreator);
            pluginFactory<double>().record("dtkSparseSolverJacobiSORGeneric", dtkSparseSolverJacobiSORGenericCreator);
            pluginFactory<double>().record("dtkSparseSolverSymmetricGaussSeidelGeneric", dtkSparseSolverSymmetricGaussSeidelGenericGenericCreator);
            pluginManager().initialize(path);
            pluginManager().setLayerVersion(QString(DTKLINEARALGEBRASPARSEVERSION_STR));
        }

        dtkSparseSolverPluginManager& pluginManager(void) {
            return _private::manager;
        }

        dtkSparseSolverPluginFactoryTemplate& pluginFactoryTemplate(void) {
            return _private::factoryTemplate;
        }
    }

    namespace preconditioner {

        namespace _private {
            dtkSparsePreconditionerPluginManager manager;
            dtkSparsePreconditionerPluginFactoryTemplate factoryTemplate;
        }

        void initialize(const QString& path) {
            pluginManager().initialize(path);
            pluginManager().setLayerVersion(QString(DTKLINEARALGEBRASPARSEVERSION_STR));
        }


        dtkSparsePreconditionerPluginManager& pluginManager(void) {
            return _private::manager;
        }

        dtkSparsePreconditionerPluginFactoryTemplate& pluginFactoryTemplate(void) {
            return _private::factoryTemplate;
        }
    }
}

template<> dtkSparseSystemBuilder<int> *dtkSparseSystemBuilder<int>::_instance = nullptr;
template<> dtkSparseSystemBuilder<long long int> *dtkSparseSystemBuilder<long long int>::_instance = nullptr;
template<> dtkSparseSystemBuilder<float> *dtkSparseSystemBuilder<float>::_instance = nullptr;
template<> dtkSparseSystemBuilder<double> *dtkSparseSystemBuilder<double>::_instance = nullptr;

//
// dtkLinearAlgebraSparse.cpp ends here
