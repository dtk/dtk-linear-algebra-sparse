// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#if defined(DTK_HAVE_ZLIB)
#include <dtkCore/dtkIOCompressor>
#endif

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkVectorDataElement<T>::dtkVectorDataElement(typename dtkArray<T, 0>::const_iterator it, qlonglong id) : m_it(it), m_id(id)
{

}

template < typename T > inline dtkVectorDataElement<T>::dtkVectorDataElement(const dtkVectorDataElement& o) : m_it(o.m_it), m_id(o.m_id)
{

}

template < typename T > inline dtkVectorDataElement<T>::~dtkVectorDataElement(void)
{

}

template < typename T > inline dtkVectorDataElement<T>& dtkVectorDataElement<T>::operator = (const dtkVectorDataElement& o)
{
    m_it = o.m_it;
    m_id = o.m_id;
    return *this;
}

template < typename T > inline qlonglong dtkVectorDataElement<T>::id(void) const
{
    return m_id;
}

template < typename T > inline T dtkVectorDataElement<T>::operator * (void) const
{
    return *m_it;
}

template < typename T > inline T dtkVectorDataElement<T>::operator [] (qlonglong j) const
{
    return m_it[j];
}

template < typename T > inline bool dtkVectorDataElement<T>::operator == (const dtkVectorDataElement& o) const
{
    return (m_id == o.m_id);
}

template < typename T > inline bool dtkVectorDataElement<T>::operator != (const dtkVectorDataElement& o) const
{
    return (m_id != o.m_id);
}

template < typename T > inline bool dtkVectorDataElement<T>::operator < (const dtkVectorDataElement& o) const
{
    return (m_id < o.m_id);
}

template < typename T > inline bool dtkVectorDataElement<T>::operator <= (const dtkVectorDataElement& o) const
{
    return (m_id <= o.m_id);
}

template < typename T > inline bool dtkVectorDataElement<T>::operator > (const dtkVectorDataElement& o) const
{
    return (m_id > o.m_id);
}

template < typename T > inline bool dtkVectorDataElement<T>::operator >= (const dtkVectorDataElement& o) const
{
    return (m_id >= o.m_id);
}

template < typename T > inline dtkVectorDataElement<T>& dtkVectorDataElement<T>::operator ++ (void)
{
    ++m_it;
    ++m_id;
    return *this;
}

template < typename T > inline dtkVectorDataElement<T> dtkVectorDataElement<T>::operator ++ (int)
{
    dtkVectorDataElement<T> o(*this);
    ++m_it;
    ++m_id;
    return o;    
}

template < typename T > inline dtkVectorDataElement<T>& dtkVectorDataElement<T>::operator -- (void)
{
    --m_it;
    --m_id;
    return *this;
}

template < typename T > inline dtkVectorDataElement<T> dtkVectorDataElement<T>::operator -- (int)
{
    dtkVectorDataElement<T> o(*this);
    --m_it;
    --m_id;
    return o; 
}

template < typename T > inline dtkVectorDataElement<T>& dtkVectorDataElement<T>::operator += (qlonglong j)
{
    m_it += j;
    m_id += j;
    return *this;
}

template < typename T > inline dtkVectorDataElement<T>& dtkVectorDataElement<T>::operator -= (qlonglong j)
{
    m_it -= j;
    m_id -= j;
    return *this;
}

template < typename T > inline dtkVectorDataElement<T> dtkVectorDataElement<T>::operator + (qlonglong j) const
{
    dtkVectorDataElement<T> o(*this);
    o += j;
    return o;
}

template < typename T > inline dtkVectorDataElement<T> dtkVectorDataElement<T>::operator - (qlonglong j) const
{
    dtkVectorDataElement<T> o(*this);
    o -= j;
    return o;
}

// ///////////////////////////////////////////////////////////////////
// 
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkVectorData<T>::dtkVectorData(void) : dtkVectorAbstractData<T>()
{

}

template < typename T > inline dtkVectorData<T>::dtkVectorData(qlonglong size) : dtkVectorAbstractData<T>(), m_array(size)
{

}

template < typename T > inline dtkVectorData<T>::dtkVectorData(qlonglong size, const T& value): dtkVectorAbstractData<T>(), m_array(size, value)
{

}

template < typename T > inline dtkVectorData<T>::dtkVectorData(const dtkVectorData& other) : dtkVectorAbstractData<T>(), m_array(other.m_array)
{

}

template < typename T > inline dtkVectorData<T>::~dtkVectorData(void)
{

}

template < typename T > inline dtkVectorData<T> *dtkVectorData<T>::clone(void) const
{
    return new dtkVectorData<T>(*this);
}

template < typename T > inline void dtkVectorData<T>::copy(dtkVectorAbstractData<T> *other)
{
    m_array = static_cast<dtkVectorData*>(other)->m_array;
}

template < typename T > inline void dtkVectorData<T>::add(dtkVectorAbstractData<T> *other)
{
    dtkArray<T,0> other_array = static_cast<dtkVectorData*>(other)->m_array;
    for (qlonglong i = 0; i < this->size(); ++i) {
        m_array[i] += other_array.at(i);
    }
}

template < typename T > inline void dtkVectorData<T>::sub(dtkVectorAbstractData<T> *other)
{
    dtkArray<T,0> other_array = static_cast<dtkVectorData*>(other)->m_array;
    for (qlonglong i = 0; i < this->size(); ++i) {
        m_array[i] -= other_array.at(i);
    }
}

template < typename T > inline bool dtkVectorData<T>::empty(void) const
{
    return m_array.empty();
}

template < typename T > inline qlonglong dtkVectorData<T>::size(void) const
{
    return m_array.size();
}

template < typename T > inline T dtkVectorData<T>::asum(void)
{
    T sum = T(0);
    for (qlonglong i = 0; i < this->size(); ++i) {
        sum += fabs(m_array.at(i));
    }
    return sum;
}

template < typename T > inline void dtkVectorData<T>::resize(const qlonglong& size)
{
    m_array.resize(size);
}

template < typename T > inline void dtkVectorData<T>::fill(const T& value)
{
    m_array.fill(value);
}

template < typename T > inline void dtkVectorData<T>::setAt(const qlonglong& index, const T& value)
{
    m_array.setAt(index, value);
}

template < typename T > inline T dtkVectorData<T>::at(const qlonglong& index) const
{
    return m_array.at(index);
}

template < typename T > inline void dtkVectorData<T>::addAssign(const qlonglong& index, const T& value)
{
    m_array[index] += value;
}

template < typename T > inline void dtkVectorData<T>::subAssign(const qlonglong& index, const T& value)
{
    m_array[index] -= value;
}

template < typename T > inline void dtkVectorData<T>::mulAssign(const qlonglong& index, const T& value)
{
    m_array[index] *= value;
}

template < typename T > inline void dtkVectorData<T>::divAssign(const qlonglong& index, const T& value)
{
    m_array[index] /= value;
}

template < typename T > inline dtkVectorElement<T> dtkVectorData<T>::begin(void) const
{
    return dtkVectorElement<T>(new dtkVectorDataElement<T>(m_array.begin(), 0));
}

template < typename T > inline dtkVectorElement<T> dtkVectorData<T>::end(void) const
{
    return dtkVectorElement<T>(new dtkVectorDataElement<T>(m_array.end(), size()));
}

template < typename T > inline const T *dtkVectorData<T>::data(void) const
{
    return m_array.constData();
}

template < typename T > inline T *dtkVectorData<T>::data(void)
{
    return m_array.data();
}

template < typename T > inline const T *dtkVectorData<T>::constData(void) const
{
    return m_array.constData();
}


// read MatrixMarket Extension format
template < typename T > bool dtkVectorData<T>::readMatrixMarket(const QString& filename)
{
    QFile file(filename);
    QIODevice *in;

    if(filename.isEmpty() || (!file.exists())) {
        qWarning() << "input file is empty/does not exist" << filename << "Current dir is" << QDir::currentPath();
        return false;
    }

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    QFileInfo info = QFileInfo(filename);
#if defined(DTK_HAVE_ZLIB)
    if (info.suffix() == "gz") {
        in = new dtkIOCompressor(&file);
    } else {
        in = &file;
        mode |= QIODevice::Text;
    }
#else
    in = &file;
    mode |= QIODevice::Text;
#endif

    if (!in->open(mode))
        return false;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    QStringList header = QString(in->readLine()).split(' ');
    // %%MatrixMarket matrix coordinate real general
    if (header.at(1) != "matrix" && header.at(2) != "array")  {
        in->close();
        qWarning() << "not a matrix array format, can't read" << filename;
        return false;
    }
    QString datatype = header.at(3);
    QString format = header.at(4).trimmed();

    bool is_double = true;
    if (datatype  == "integer") {
        is_double = false;
    } else if ((datatype == "complex") || (datatype  == "pattern")) {
        qWarning() << datatype << " format not supported, can't read matrix" << filename;
        return false;
    }

    if (format != "general") {
        qWarning()<< format << "Only general format supported, can't read matrix from file" << filename;
        return false;
    }

    QString line;
    QStringList data;

    qlonglong linesCount = 0;
    qlonglong nnz = 0;
    qlonglong colCount = 0;
    QRegExp re = QRegExp("\\s+");

    while (linesCount == 0) {
        line = in->readLine().trimmed();

        if (line.startsWith("%"))
            continue;
        data = line.split(re);
        linesCount = data[0].toLongLong();
        colCount = data[1].toLongLong();
    }

    this->resize(linesCount);
    T val;

    qlonglong i = 0;
    std::string linestd;

    while (!in->atEnd()) {
        const QByteArray array = in->readLine();
        linestd = std::string(array.constData(), array.length());
        try {
            val = std::stod (linestd);
            this->setAt(i,val);
            ++i;
        } catch (...) {
            // qDebug()<< "skip line";
        }
    }
    in->close();
    return true;
}

// 
// dtkVectorData.tpp ends here
