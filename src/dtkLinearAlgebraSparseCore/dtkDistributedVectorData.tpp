// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include<dtkDistributed/dtkDistributedItem.h>

template < typename T > inline dtkDistributedVectorDataElement<T>::dtkDistributedVectorDataElement(typename dtkDistributedArray<T>::const_iterator it, qlonglong id) : m_it(it), m_id(id)
{

}

template < typename T > inline dtkDistributedVectorDataElement<T>::dtkDistributedVectorDataElement(const dtkDistributedVectorDataElement& o) : m_it(o.m_it), m_id(o.m_id)
{

}

template < typename T > inline dtkDistributedVectorDataElement<T>::~dtkDistributedVectorDataElement(void)
{

}

template < typename T > inline dtkDistributedVectorDataElement<T>& dtkDistributedVectorDataElement<T>::operator = (const dtkDistributedVectorDataElement& o)
{
    m_it = o.m_it;
    m_id = o.m_id;
    return *this;
}

template < typename T > inline qlonglong dtkDistributedVectorDataElement<T>::id(void) const
{
    return m_id;
}

template < typename T > inline T dtkDistributedVectorDataElement<T>::operator * (void) const
{
    return *m_it;
}

template < typename T > inline T dtkDistributedVectorDataElement<T>::operator [] (qlonglong j) const
{
    return m_it[j];
}

template < typename T > inline bool dtkDistributedVectorDataElement<T>::operator == (const dtkDistributedVectorDataElement& o) const
{
    return (m_id == o.m_id);
}

template < typename T > inline bool dtkDistributedVectorDataElement<T>::operator != (const dtkDistributedVectorDataElement& o) const
{
    return (m_id != o.m_id);
}

template < typename T > inline bool dtkDistributedVectorDataElement<T>::operator < (const dtkDistributedVectorDataElement& o) const
{
    return (m_id < o.m_id);
}

template < typename T > inline bool dtkDistributedVectorDataElement<T>::operator <= (const dtkDistributedVectorDataElement& o) const
{
    return (m_id <= o.m_id);
}

template < typename T > inline bool dtkDistributedVectorDataElement<T>::operator > (const dtkDistributedVectorDataElement& o) const
{
    return (m_id > o.m_id);
}

template < typename T > inline bool dtkDistributedVectorDataElement<T>::operator >= (const dtkDistributedVectorDataElement& o) const
{
    return (m_id >= o.m_id);
}

template < typename T > inline dtkDistributedVectorDataElement<T>& dtkDistributedVectorDataElement<T>::operator ++ (void)
{
    ++m_it;
    ++m_id;
    return *this;
}

template < typename T > inline dtkDistributedVectorDataElement<T> dtkDistributedVectorDataElement<T>::operator ++ (int)
{
    dtkDistributedVectorDataElement<T> o(*this);
    ++m_it;
    ++m_id;
    return o;
}

template < typename T > inline dtkDistributedVectorDataElement<T>& dtkDistributedVectorDataElement<T>::operator -- (void)
{
    --m_it;
    --m_id;
    return *this;
}

template < typename T > inline dtkDistributedVectorDataElement<T> dtkDistributedVectorDataElement<T>::operator -- (int)
{
    dtkDistributedVectorDataElement<T> o(*this);
    --m_it;
    --m_id;
    return o;
}

template < typename T > inline dtkDistributedVectorDataElement<T>& dtkDistributedVectorDataElement<T>::operator += (qlonglong j)
{
    m_it += j;
    m_id += j;
    return *this;
}

template < typename T > inline dtkDistributedVectorDataElement<T>& dtkDistributedVectorDataElement<T>::operator -= (qlonglong j)
{
    m_it -= j;
    m_id -= j;
    return *this;
}

template < typename T > inline dtkDistributedVectorDataElement<T> dtkDistributedVectorDataElement<T>::operator + (qlonglong j) const
{
    dtkDistributedVectorDataElement<T> o(*this);
    o += j;
    return o;
}

template < typename T > inline dtkDistributedVectorDataElement<T> dtkDistributedVectorDataElement<T>::operator - (qlonglong j) const
{
    dtkDistributedVectorDataElement<T> o(*this);
    o -= j;
    return o;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkDistributedVectorData<T>::dtkDistributedVectorData(void) : dtkVectorAbstractData<T>()
{
    m_array = NULL;
}

template < typename T > inline dtkDistributedVectorData<T>::dtkDistributedVectorData(qlonglong size) : dtkVectorAbstractData<T>(), m_array(new dtkDistributedArray<T>(size))
{
}

template < typename T > inline dtkDistributedVectorData<T>::dtkDistributedVectorData(qlonglong size, const T& value): dtkVectorAbstractData<T>(), m_array(new dtkDistributedArray<T>(size, value))
{

}

template < typename T > inline dtkDistributedVectorData<T>::dtkDistributedVectorData(const dtkDistributedVectorData& other) : dtkVectorAbstractData<T>(), m_array(new dtkDistributedArray<T>(*other.m_array))
{

}

template < typename T > inline dtkDistributedVectorData<T>::~dtkDistributedVectorData(void)
{
    if (m_array)
        delete m_array;
}

template < typename T > inline dtkDistributedVectorData<T> *dtkDistributedVectorData<T>::clone(void) const
{
    return new dtkDistributedVectorData<T>(*this);
}

template < typename T > inline void dtkDistributedVectorData<T>::copy(dtkVectorAbstractData<T> *other)
{
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
    qlonglong size = other->size() ;
    if (this->size() != size) {
        this->resize(size);
    } else {
        comm->barrier();
    }

    if (dtkVectorAbstractData<T>::dataType() == other->dataType()) {
        dtkDistributedVectorData<T> *other_v = static_cast<dtkDistributedVectorData<T> *>(other);
        *m_array = *(other_v->m_array);

    } else {
        dtkDistributedMapper *mapper = m_array->mapper();
        qlonglong i = mapper->firstIndex(m_array->wid());
        qlonglong end = mapper->lastIndex(m_array->wid())+1;
        other->rlock();
        this->wlock();
        for (; i < end; ++i) {
            this->setAt(i, other->at(i));
        }
        other->unlock();
        this->unlock();
    }
    comm->barrier();
}

template < typename T > inline void dtkDistributedVectorData<T>::add(dtkVectorAbstractData<T> *other)
{
    dtkDistributedMapper *mapper = m_array->mapper();
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();

    qlonglong i = mapper->firstIndex(m_array->wid());
    qlonglong end = mapper->lastIndex(m_array->wid())+1;
    comm->barrier();
    this->wlock();
    m_array->addAssign(i, other->data(), end - i);
    this->unlock();
    comm->barrier();
}

template < typename T > inline void dtkDistributedVectorData<T>::sub(dtkVectorAbstractData<T> *other)
{
    dtkDistributedMapper *mapper = m_array->mapper();
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();

    qlonglong i = mapper->firstIndex(m_array->wid());
    qlonglong end = mapper->lastIndex(m_array->wid())+1;
    comm->barrier();
    this->wlock();
    m_array->subAssign(i, other->data(), end - i);
    this->unlock();
    comm->barrier();
}

template < typename T > inline T dtkDistributedVectorData<T>::asum(void)
{
    dtkDistributedMapper *mapper = m_array->mapper();
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();

    dtkDistributedItem<T> sum(comm);
    sum =T(0);
    qlonglong i = mapper->firstIndex(m_array->wid());
    qlonglong end = mapper->lastIndex(m_array->wid())+1;
    this->wlock();
    for (; i < end; ++i) {
        sum += fabs(m_array->at(i));
    }
    this->unlock();
    return *sum;
}

template < typename T > inline bool dtkDistributedVectorData<T>::empty(void) const
{
    if (!m_array)
        return true;
    return m_array->empty();
}

template < typename T > inline qlonglong dtkDistributedVectorData<T>::size(void) const
{
    if (!m_array)
        return 0;
    return m_array->size();
}

template < typename T > inline void dtkDistributedVectorData<T>::resize(const qlonglong& size)
{
    //FIXME:; we need to implement a resize method in dtkDistributedArray
    if (m_array)
        delete m_array;
    m_array = new dtkDistributedArray<T>(size);
}

template < typename T > inline void dtkDistributedVectorData<T>::fill(const T& value)
{
    m_array->fill(value);
}

template < typename T > inline void dtkDistributedVectorData<T>::clearCache(void)
{
    m_array->clearCache();
}

template < typename T > inline void dtkDistributedVectorData<T>::rlock(void)
{
    m_array->rlock();
}

template < typename T > inline void dtkDistributedVectorData<T>::wlock(void)
{
    m_array->wlock(m_array->wid());
}

template < typename T > inline void dtkDistributedVectorData<T>::unlock(void)
{
    m_array->unlock();
}

template < typename T > inline void dtkDistributedVectorData<T>::setAt(const qlonglong& index, const T& value)
{
    m_array->setAt(index, value);
}

template < typename T > inline T dtkDistributedVectorData<T>::at(const qlonglong& index) const
{
    return m_array->at(index);
}

template < typename T > inline void dtkDistributedVectorData<T>::addAssign(const qlonglong& index, const T& value)
{
    m_array->addAssign(index, value);
}

template < typename T > inline void dtkDistributedVectorData<T>::subAssign(const qlonglong& index, const T& value)
{
    m_array->subAssign(index, value);
    //m_array->addAssign(index, -value);
}

template < typename T > inline void dtkDistributedVectorData<T>::mulAssign(const qlonglong& index, const T& value)
{
    m_array->mulAssign(index, value);
}

template < typename T > inline void dtkDistributedVectorData<T>::divAssign(const qlonglong& index, const T& value)
{
    m_array->divAssign(index, value);
    //m_array->mulAssign(index, 1 / value);
}

template < typename T > inline dtkVectorElement<T> dtkDistributedVectorData<T>::begin(void) const
{
    return dtkVectorElement<T>(new dtkDistributedVectorDataElement<T>(m_array->cbegin(), m_array->mapper()->firstIndex(m_array->wid())));
}

template < typename T > inline dtkVectorElement<T> dtkDistributedVectorData<T>::end(void) const
{
    return dtkVectorElement<T>(new dtkDistributedVectorDataElement<T>(m_array->cend(), m_array->mapper()->lastIndex(m_array->wid()) + 1));
}

template < typename T > inline const T *dtkDistributedVectorData<T>::data(void) const
{
    return m_array->constData();
}

template < typename T > inline T *dtkDistributedVectorData<T>::data(void)
{
    return m_array->data();
}

template < typename T > inline const T *dtkDistributedVectorData<T>::constData(void) const
{
    return m_array->constData();
}

template <typename T> void dtkDistributedVectorData<T>::setMapper(dtkDistributedMapper *mapper)
{
    m_array->remap(mapper);
}

template < typename T > bool dtkDistributedVectorData<T>::readMatrixMarket(const QString& filename)
{
    QFile file(filename);
    qlonglong edges_count = 0;
    QTime time;
    QIODevice *in;

    qlonglong linesCount = 0;
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();

    if (comm->wid() == 0) {
        time.start();

        if(filename.isEmpty() || (!file.exists())) {
            qWarning() << "input file is empty/does not exist" << filename << "Current dir is" << QDir::currentPath();
            return false;
        }

        QIODevice::OpenMode mode = QIODevice::ReadOnly;
        QFileInfo info = QFileInfo(filename);
#if defined(DTK_HAVE_ZLIB)
        if (info.suffix() == "gz") {
            in = new dtkIOCompressor(&file);
        } else {
            in = &file;
            mode |= QIODevice::Text;
        }
#else
        in = &file;
        mode |= QIODevice::Text;
#endif
        if (!in->open(mode))
            return false;

        // to avoid troubles with floats separators ('.' and not ',')
        QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
        setlocale(LC_NUMERIC, "C");
#endif
        QStringList header = QString(in->readLine()).split(' ');
        // %%MatrixMarket matrix coordinate real general
        if (header.at(1) != "matrix" && header.at(2) != "array")  {
            in->close();
            qWarning() << "not a matrix array format, can't read" << filename;
            return false;
        }
        QString datatype = header.at(3);
        QString format = header.at(4).trimmed();

        bool is_double = true;
        if (datatype  == "integer") {
            is_double = false;
        } else if ((datatype == "complex") || (datatype  == "pattern")) {
            qWarning() << datatype << " format not supported, can't read matrix" << filename;
            return false;
        }

        if (format != "general") {
            qWarning()<< format << "Only general format supported, can't read matrix from file" << filename;
            return false;
        }

        QString line;
        QStringList data;

        qlonglong nnz = 0;
        qlonglong colCount = 0;
        QRegExp re = QRegExp("\\s+");

        while (linesCount == 0) {
            line = in->readLine().trimmed();

            if (line.startsWith("%"))
                continue;
            data = line.split(re);
            linesCount = data[0].toLongLong();
            colCount = data[1].toLongLong();
        }
    }
    comm->broadcast(&linesCount,1, 0);
    this->resize(linesCount);

    if (comm->wid() == 0) {
        T val;

        qlonglong i = 0;
        std::string linestd;

        while (!in->atEnd()) {
            const QByteArray array = in->readLine();
            linestd = std::string(array.constData(), array.length());
            try {
                val = std::stod (linestd);
                this->setAt(i,val);
                ++i;
            } catch (...) {
                // qDebug()<< "skip line";
            }
        }
        in->close();
    }
    return true;

}

//
// dtkDistributedVectorData.tpp ends here
