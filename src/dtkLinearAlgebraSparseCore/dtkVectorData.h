// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include "dtkVectorAbstractData.h"
#include "dtkVectorElement.h"

// ///////////////////////////////////////////////////////////////////
// dtkVectorDataElement
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkVectorDataElement : public dtkVectorElementBase<T, dtkVectorDataElement<T> >
{
    typename dtkArray<T, 0>::const_iterator m_it;
    qlonglong m_id;

public:
    dtkVectorDataElement(typename dtkArray<T, 0>::const_iterator it, qlonglong id);

public:
    dtkVectorDataElement(const dtkVectorDataElement& o);

public:
    ~dtkVectorDataElement(void);

public:
    dtkVectorDataElement& operator = (const dtkVectorDataElement& o);

public:
    qlonglong id(void) const;

public:
    T operator * (void) const;
    T operator [] (qlonglong j) const;

public:
    bool operator == (const dtkVectorDataElement& o) const;
    bool operator != (const dtkVectorDataElement& o) const;
    bool operator <  (const dtkVectorDataElement& o) const;
    bool operator <= (const dtkVectorDataElement& o) const;
    bool operator >  (const dtkVectorDataElement& o) const;
    bool operator >= (const dtkVectorDataElement& o) const;

public:
    dtkVectorDataElement& operator ++ (void);
    dtkVectorDataElement  operator ++ (int);
    dtkVectorDataElement& operator -- (void);
    dtkVectorDataElement  operator -- (int);
    dtkVectorDataElement& operator += (qlonglong j);
    dtkVectorDataElement& operator -= (qlonglong j);
    dtkVectorDataElement  operator +  (qlonglong j) const;
    dtkVectorDataElement  operator -  (qlonglong j) const;
};

// ///////////////////////////////////////////////////////////////////
// dtkVectorData
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkVectorData : public dtkVectorAbstractData<T>
{
public:
             dtkVectorData(void);
    explicit dtkVectorData(qlonglong size);
             dtkVectorData(qlonglong size, const T& value);
             dtkVectorData(const dtkVectorData& other);

public:
    ~dtkVectorData(void);

public:
    dtkVectorData *clone(void) const;

public:
    void copy(dtkVectorAbstractData<T> *other);

public:
    bool      empty(void) const;
    qlonglong  size(void) const;

public:
    void resize(const qlonglong& size);

public:
    void fill(const T& value);

public:
    void add(dtkVectorAbstractData<T> *other);
    void sub(dtkVectorAbstractData<T> *other);
    T asum(void);

public:
    bool  readMatrixMarket(const QString& filename);

public:
    void clearCache(void) {};

public:
    void setAt(const qlonglong& index, const T& value);

    T at(const qlonglong& index) const;

public:
    void addAssign(const qlonglong& index, const T& value);
    void subAssign(const qlonglong& index, const T& value);
    void mulAssign(const qlonglong& index, const T& value);
    void divAssign(const qlonglong& index, const T& value);

public:
    dtkVectorElement<T> begin(void) const;
    dtkVectorElement<T>   end(void) const;

public:
    const T *data(void) const;
          T *data(void);

    const T *constData(void) const;

protected:
    dtkArray<T, 0> m_array;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > dtkVectorAbstractData<T> *dtkVectorDataCreator(void)
{
    return new dtkVectorData<T>();
}

// ///////////////////////////////////////////////////////////////////

#include "dtkVectorData.tpp"

//
// dtkVectorData.h ends here
