/* dtkDistributedVectorData.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2015 - Nicolas Niclausse, Inria.
 * Created: 2015/03/26 14:37:35
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <dtkCore>

#include <dtkDistributed/dtkDistributedArray.h>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedVectorDataElement
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkDistributedVectorDataElement : public dtkVectorElementBase<T, dtkDistributedVectorDataElement<T> >
{
    typename dtkDistributedArray<T>::const_iterator m_it;
    qlonglong m_id;

public:
dtkDistributedVectorDataElement(typename dtkDistributedArray<T>::const_iterator it, qlonglong id);

public:
    dtkDistributedVectorDataElement(const dtkDistributedVectorDataElement& o);

public:
    ~dtkDistributedVectorDataElement(void);

public:
    dtkDistributedVectorDataElement& operator = (const dtkDistributedVectorDataElement& o);

public:
    qlonglong id(void) const;

public:
    T operator * (void) const;
    T operator [] (qlonglong j) const;

public:
    bool operator == (const dtkDistributedVectorDataElement& o) const;
    bool operator != (const dtkDistributedVectorDataElement& o) const;
    bool operator <  (const dtkDistributedVectorDataElement& o) const;
    bool operator <= (const dtkDistributedVectorDataElement& o) const;
    bool operator >  (const dtkDistributedVectorDataElement& o) const;
    bool operator >= (const dtkDistributedVectorDataElement& o) const;

public:
    dtkDistributedVectorDataElement& operator ++ (void);
    dtkDistributedVectorDataElement  operator ++ (int);
    dtkDistributedVectorDataElement& operator -- (void);
    dtkDistributedVectorDataElement  operator -- (int);
    dtkDistributedVectorDataElement& operator += (qlonglong j);
    dtkDistributedVectorDataElement& operator -= (qlonglong j);
    dtkDistributedVectorDataElement  operator +  (qlonglong j) const;
    dtkDistributedVectorDataElement  operator -  (qlonglong j) const;
};

// ///////////////////////////////////////////////////////////////////
// dtkDistributedVectorData
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkDistributedVectorData : public dtkVectorAbstractData<T>
{
public:
             dtkDistributedVectorData(void);
    explicit dtkDistributedVectorData(qlonglong size);
             dtkDistributedVectorData(qlonglong size, const T& value);
             dtkDistributedVectorData(const dtkDistributedVectorData& other);

public:
    ~dtkDistributedVectorData(void);

public:
    dtkDistributedVectorData *clone(void) const;

public:
    void copy(dtkVectorAbstractData<T> *other);

public:
    bool      empty(void) const;
    qlonglong  size(void) const;

public:
    void clearCache(void);
    void rlock(void);
    void wlock(void);
    void unlock(void);

public:
    void resize(const qlonglong& size);

public:
    void add(dtkVectorAbstractData<T> *other);
    void sub(dtkVectorAbstractData<T> *other);
    T asum(void);

public:
    void setMapper(dtkDistributedMapper *mapper);

public:
    bool readMatrixMarket(const QString& filename);

public:
    void fill(const T& value);

public:
    void setAt(const qlonglong& index, const T& value);

    T at(const qlonglong& index) const;

public:
    void addAssign(const qlonglong& index, const T& value);
    void subAssign(const qlonglong& index, const T& value);
    void mulAssign(const qlonglong& index, const T& value);
    void divAssign(const qlonglong& index, const T& value);

public:
    dtkVectorElement<T> begin(void) const;
    dtkVectorElement<T>   end(void) const;

public:
    const T *data(void) const;
          T *data(void);

    const T *constData(void) const;

protected:
    dtkDistributedArray<T> *m_array;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > dtkVectorAbstractData<T> *dtkDistributedVectorDataCreator(void)
{
    return new dtkDistributedVectorData<T>();
}

// ///////////////////////////////////////////////////////////////////

#include "dtkDistributedVectorData.tpp"

//
// dtkDistributedVectorData.h ends here
