/* dtkVectorAbstractData.tpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2015, Inria.
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#if defined(DTK_HAVE_ZLIB)
#include <dtkCore/dtkIOCompressor>
#endif

// write MatrixMarket Extension format
template < typename T > bool dtkVectorAbstractData<T>::writeMatrixMarket(const QString& filename) const
{
    QFile file(filename);
    QIODevice *in;

    if(filename.isEmpty()) {
        qWarning() << "output filename is empty !";
        return false;
    }

    QIODevice::OpenMode mode = QIODevice::WriteOnly;
    QFileInfo info = QFileInfo(filename);
#if defined(DTK_HAVE_ZLIB)
    if (info.suffix() == "gz") {
        in = new dtkIOCompressor(&file);
    } else {
        in = &file;
        mode |= QIODevice::Text;
    }
#else
    in = &file;
    mode |= QIODevice::Text;
#endif

    if (!in->open(mode))
        return false;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    //FIXME: handle type long/double
    in->write("%%MatrixMarket matrix array real general\n");
    in->write(qPrintable(QString::number(this->size()) + " 1\n"));
    for (qlonglong i=0; i < this->size(); ++i) {
        in->write(qPrintable(QString::number(this->at(i),'g',16) +"\n"));
    }
    in->close();
    return true;
}
