// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkLinearAlgebraSparseCoreExport.h>
#include "dtkVectorElement.h"

class dtkDistributedMapper;

// /////////////////////////////////////////////////////////////////
// dtkVectorAbstractData
// /////////////////////////////////////////////////////////////////

template < typename T > class dtkVectorAbstractData : public QObject
{
public:
             dtkVectorAbstractData(void) {;}
    virtual ~dtkVectorAbstractData(void) {;}

public:
    QString dataType(void) const { return this->objectName(); }

public:
    virtual dtkVectorAbstractData *clone(void) const = 0;

public:
    virtual void copy(dtkVectorAbstractData<T>* other) = 0;

public:
    virtual bool      empty(void) const = 0;
    virtual qlonglong  size(void) const = 0;

public:
    virtual void  add(dtkVectorAbstractData<T>* other) = 0;
    virtual void  sub(dtkVectorAbstractData<T>* other) = 0;
    virtual T  asum(void) = 0;

public:
    virtual void setMapper(dtkDistributedMapper *mapper) {;}

public:
    virtual bool  readMatrixMarket(const QString& filename) = 0;
    virtual bool  writeMatrixMarket(const QString& filename) const;

public:
    virtual void resize(const qlonglong& size) = 0;

public:
    virtual void fill(const T& value) = 0;

public:
    virtual void clearCache(void) = 0;
    virtual void rlock(void)  {};
    virtual void wlock(void)  {};
    virtual void unlock(void) {};

public:
    virtual void setAt(const qlonglong& index, const T& value) = 0;

    virtual T at(const qlonglong& index) const = 0;

public:
    virtual void addAssign(const qlonglong& index, const T& value) = 0;
    virtual void subAssign(const qlonglong& index, const T& value) = 0;
    virtual void mulAssign(const qlonglong& index, const T& value) = 0;
    virtual void divAssign(const qlonglong& index, const T& value) = 0;

public:
    virtual dtkVectorElement<T> begin(void) const = 0;
    virtual dtkVectorElement<T>   end(void) const = 0;

public:
    virtual const T *data(void) const = 0;
    virtual       T *data(void)       = 0;

    virtual const T *constData(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkVectorAbstractDataPluginFactory : public dtkCorePluginFactory<dtkVectorAbstractData<T> >
{
};

typedef dtkCorePluginFactoryTemplate dtkVectorAbstractDataPluginFactoryTemplate;

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkVectorAbstractDataPlugin : public QObject
{
    Q_OBJECT

public:
             dtkVectorAbstractDataPlugin(void) {}
    virtual ~dtkVectorAbstractDataPlugin(void) {}

public:
    virtual void   initialize(void) = 0;
    virtual void uninitialize(void) = 0;
};

Q_DECLARE_INTERFACE(dtkVectorAbstractDataPlugin, DTK_DECLARE_PLUGIN_INTERFACE(dtkVectorAbstractData))

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkVectorAbstractDataPluginManager : public dtkCorePluginManager<dtkVectorAbstractDataPlugin>
{
};

#include "dtkVectorAbstractData.tpp"

//
// dtkVectorAbstractData.h ends here
