// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixProxy implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkSparseMatrixProxy<T>::dtkSparseMatrixProxy(dtkSparseMatrixEngine<T> *d, qlonglong i, qlonglong j) : data(d), m_i(i), m_j(j)
{
}

template < typename T > inline dtkSparseMatrixProxy<T>::dtkSparseMatrixProxy(const dtkSparseMatrixProxy& o) : data(o.data), m_i(o.m_i), m_j(o.m_j)
{
}

template < typename T > inline dtkSparseMatrixProxy<T>::~dtkSparseMatrixProxy(void)
{
}

template < typename T > inline dtkSparseMatrixProxy<T>& dtkSparseMatrixProxy<T>::operator = (const dtkSparseMatrixProxy& o)
{
    data->assign(m_i, m_j, o.value());
    return *this;
}

template < typename T > inline T dtkSparseMatrixProxy<T>::value(void) const
{
    return data->at(m_i, m_j);
}

template < typename T > inline dtkSparseMatrixProxy<T>& dtkSparseMatrixProxy<T>::operator =  (const T& value)
{
    data->assign(m_i, m_j, value);
    return *this;
}

template < typename T > inline dtkSparseMatrixProxy<T>& dtkSparseMatrixProxy<T>::operator += (const T& value)
{
    data->addAssign(m_i, m_j, value);
    return *this;
}

template < typename T > inline dtkSparseMatrixProxy<T>& dtkSparseMatrixProxy<T>::operator -= (const T& value)
{
    data->subAssign(m_i, m_j, value);
    return *this;
}

template < typename T > inline dtkSparseMatrixProxy<T>& dtkSparseMatrixProxy<T>::operator *= (const T& value)
{
    data->mulAssign(m_i, m_j, value);
    return *this;
}

template < typename T > inline dtkSparseMatrixProxy<T>& dtkSparseMatrixProxy<T>::operator /= (const T& value)
{
    data->divAssign(m_i, m_j, value);
    return *this;
}

template < typename T > inline dtkSparseMatrixProxy<T>::operator T() const
{
    return this->value();
}

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixProxy operators
// ///////////////////////////////////////////////////////////////////

template < typename T > inline bool operator == (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs.value() == rhs.value());
}

template < typename T > inline bool operator == (const dtkSparseMatrixProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() == rhs);
}

template < typename T > inline bool operator == (const T& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs == rhs.value());
}

template < typename T > inline bool operator != (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs.value() != rhs.value());
}

template < typename T > inline bool operator != (const dtkSparseMatrixProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() != rhs);
}

template < typename T > inline bool operator != (const T& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs != rhs.value());
}

template < typename T > inline bool operator < (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs.value() < rhs.value());
}

template < typename T > inline bool operator < (const dtkSparseMatrixProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() < rhs);
}

template < typename T > inline bool operator < (const T& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs < rhs.value());
}

template < typename T > inline bool operator > (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs.value() > rhs.value());
}

template < typename T > inline bool operator > (const dtkSparseMatrixProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() > rhs);
}

template < typename T > inline bool operator > (const T& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs > rhs.value());
}

template < typename T > inline bool operator <= (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs.value() <= rhs.value());
}

template < typename T > inline bool operator <= (const dtkSparseMatrixProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() <= rhs);
}

template < typename T > inline bool operator <= (const T& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs <= rhs.value());
}

template < typename T > inline bool operator >= (const dtkSparseMatrixProxy<T>& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs.value() >= rhs.value());
}

template < typename T > inline bool operator >= (const dtkSparseMatrixProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() >= rhs);
}

template < typename T > inline bool operator >= (const T& lhs, const dtkSparseMatrixProxy<T>& rhs)
{
    return (lhs >= rhs.value());
}

template < typename T > inline QDebug& operator << (QDebug dbg, const dtkSparseMatrixProxy<T>& proxy)
{
    dbg.nospace() << proxy.value();

    return dbg.space();
}


//
// dtkSparseMatrixProxy.tpp ends here
