// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include "dtkSparseSolver.h"

class dtkSparseSolverJacobiSORPrivate;

class dtkSparseSolverJacobiSOR : public dtkSparseSolver<double>
{

public:
     dtkSparseSolverJacobiSOR(void);
    ~dtkSparseSolverJacobiSOR(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    dtkSparseSolverJacobiSORPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *dtkSparseSolverJacobiSORGenericCreator(void)
{
    return new dtkSparseSolverJacobiSOR();
}

//
// dtkSparseSolverJacobiSOR.h ends here
