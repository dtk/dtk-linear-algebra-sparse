// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLinearAlgebraSparseCoreExport.h>

#include <QtCore>

#include <dtkCore>

template <typename T> class dtkSparseMatrix;
template <typename T, qlonglong PreallocSize> class dtkArray;

// /////////////////////////////////////////////////////////////////
// dtkSparseMatrixSolver
// /////////////////////////////////////////////////////////////////

template < typename T > class dtkSparsePreconditioner : public QObject
{
public:
             dtkSparsePreconditioner(void) {;}
    virtual ~dtkSparsePreconditioner(void) {;}

public:
    virtual void setMatrix(const dtkSparseMatrix<T>& matrix) = 0;

    virtual void setOptionalParameters(const dtkArray<double>& parameters) = 0;

public:
    virtual int run(void) = 0;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkSparsePreconditionerPluginFactory : public dtkCorePluginFactory<dtkSparsePreconditioner<T> >
{
};

typedef dtkCorePluginFactoryTemplate dtkSparsePreconditionerPluginFactoryTemplate;

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparsePreconditionerPlugin : public QObject
{
    Q_OBJECT

 public:
             dtkSparsePreconditionerPlugin(void) {}
    virtual ~dtkSparsePreconditionerPlugin(void) {}

 public:
    virtual void   initialize(void) = 0;
    virtual void uninitialize(void) = 0;
};

Q_DECLARE_INTERFACE(dtkSparsePreconditionerPlugin, DTK_DECLARE_PLUGIN_INTERFACE(dtkSparsePreconditioner))

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparsePreconditionerPluginManager : public dtkCorePluginManager<dtkSparsePreconditionerPlugin>
{
};

//
// dtkSparsePreconditioner.h ends here


