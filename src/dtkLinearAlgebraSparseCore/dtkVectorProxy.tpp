// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#include "dtkVectorAbstractData.h"

// ///////////////////////////////////////////////////////////////////
// dtkVectorProxy implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkVectorProxy<T>::dtkVectorProxy(dtkVectorAbstractData<T> *d, qlonglong i) : data(d), m_i(i)
{
}

template < typename T > inline dtkVectorProxy<T>::dtkVectorProxy(const dtkVectorProxy& o) : data(o.data), m_i(o.m_i)
{
}

template < typename T > inline dtkVectorProxy<T>::~dtkVectorProxy(void)
{
}

template < typename T > inline dtkVectorProxy<T>& dtkVectorProxy<T>::operator = (const dtkVectorProxy& o)
{
    data->setAt(m_i, o.value());
    return *this;
}

template < typename T > inline T dtkVectorProxy<T>::value(void) const
{
    return data->at(m_i);
}

template < typename T > inline dtkVectorProxy<T>& dtkVectorProxy<T>::operator = (const T& value)
{
    data->setAt(m_i, value);
    return *this;
}

template < typename T > inline dtkVectorProxy<T>& dtkVectorProxy<T>::operator += (const T& value)
{
    data->addAssign(m_i, value);
    return *this;
}

template < typename T > inline dtkVectorProxy<T>& dtkVectorProxy<T>::operator -= (const T& value)
{
    data->subAssign(m_i, value);
    return *this;
}

template < typename T > inline dtkVectorProxy<T>& dtkVectorProxy<T>::operator *= (const T& value)
{
    data->mulAssign(m_i, value);
    return *this;
}

template < typename T > inline dtkVectorProxy<T>& dtkVectorProxy<T>::operator /= (const T& value)
{
    data->divAssign(m_i, value);
    return *this;
}

template < typename T > inline dtkVectorProxy<T>::operator T() const
{
    return this->value();
}

// ///////////////////////////////////////////////////////////////////
// dtkVectorProxy operators
// ///////////////////////////////////////////////////////////////////

template < typename T > inline bool operator == (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs.value() == rhs.value());
}

template < typename T > inline bool operator == (const dtkVectorProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() == rhs);
}

template < typename T > inline bool operator == (const T& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs == rhs.value());
}

template < typename T > inline bool operator != (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs.value() != rhs.value());
}

template < typename T > inline bool operator != (const dtkVectorProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() != rhs);
}

template < typename T > inline bool operator != (const T& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs != rhs.value());
}

template < typename T > inline bool operator < (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs.value() < rhs.value());
}

template < typename T > inline bool operator < (const dtkVectorProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() < rhs);
}

template < typename T > inline bool operator < (const T& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs < rhs.value());
}

template < typename T > inline bool operator > (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs.value() > rhs.value());
}

template < typename T > inline bool operator > (const dtkVectorProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() > rhs);
}

template < typename T > inline bool operator > (const T& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs > rhs.value());
}

template < typename T > inline bool operator <= (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs.value() <= rhs.value());
}

template < typename T > inline bool operator <= (const dtkVectorProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() <= rhs);
}

template < typename T > inline bool operator <= (const T& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs <= rhs.value());
}

template < typename T > inline bool operator >= (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs.value() >= rhs.value());
}

template < typename T > inline bool operator >= (const dtkVectorProxy<T>& lhs, const T& rhs)
{
    return (lhs.value() >= rhs);
}

template < typename T > inline bool operator >= (const T& lhs, const dtkVectorProxy<T>& rhs)
{
    return (lhs >= rhs.value());
}

template < typename T > inline QDebug& operator << (QDebug dbg, const dtkVectorProxy<T>& proxy)
{
    dbg.nospace() << proxy.value();

    return dbg.space();
}

// 
// dtkVectorProxy.tpp ends here
