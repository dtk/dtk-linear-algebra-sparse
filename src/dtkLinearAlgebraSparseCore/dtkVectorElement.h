// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <QtCore>

template <typename T> class dtkVectorAbstractElement;

// ///////////////////////////////////////////////////////////////////
// dtkVectorElement
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkVectorElement
{
    dtkVectorAbstractElement<T> *elt;

public:
    typedef qlonglong difference_type;
    typedef T value_type;

public:
    explicit dtkVectorElement(dtkVectorAbstractElement<T> *elt = 0);

public:
    dtkVectorElement(const dtkVectorElement& o);
    dtkVectorElement(dtkVectorElement&& o);

public:
    ~dtkVectorElement(void);

public:
    dtkVectorElement& operator = (const dtkVectorElement& o);
    dtkVectorElement& operator = (dtkVectorElement&& o);

public:
    qlonglong id(void) const;

public:
    T operator *  (void) const;
    T operator [] (qlonglong j) const;

public:
    bool operator == (const dtkVectorElement& o) const;
    bool operator != (const dtkVectorElement& o) const;
    bool operator <  (const dtkVectorElement& o) const;
    bool operator <= (const dtkVectorElement& o) const;
    bool operator >  (const dtkVectorElement& o) const;
    bool operator >= (const dtkVectorElement& o) const;

public:
    dtkVectorElement& operator ++ (void);
    dtkVectorElement  operator ++ (int);
    dtkVectorElement& operator -- (void);
    dtkVectorElement  operator -- (int);
    dtkVectorElement& operator += (qlonglong j);
    dtkVectorElement& operator -= (qlonglong j);
    dtkVectorElement  operator +  (qlonglong j) const;
    dtkVectorElement  operator -  (qlonglong j) const;
};

// ///////////////////////////////////////////////////////////////////
// dtkVectorAbstractElement
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkVectorAbstractElement
{
public:
    virtual ~dtkVectorAbstractElement(void) {}

public:
    virtual dtkVectorAbstractElement *clone(void) const = 0;

public:
    virtual void copy(const dtkVectorAbstractElement& o) = 0;

public:
    virtual qlonglong id(void) const = 0;

public:
    virtual T value(void) const = 0;
    virtual T value(qlonglong id) const = 0;

public:
    virtual bool       equal(const dtkVectorAbstractElement& o) const = 0;
    virtual bool   lowerThan(const dtkVectorAbstractElement& o) const = 0;
    virtual bool greaterThan(const dtkVectorAbstractElement& o) const = 0;

public:
    virtual void advance(void) = 0;
    virtual void  rewind(void) = 0;
    virtual void advance(qlonglong j) = 0;
    virtual void  rewind(qlonglong j) = 0;
};

// ///////////////////////////////////////////////////////////////////
// dtkVectorElementBase CRTP 
// ///////////////////////////////////////////////////////////////////

template <typename T, typename Line> class dtkVectorElementBase : public dtkVectorAbstractElement<T>
{
public:
    virtual ~dtkVectorElementBase(void) {}

public:
    dtkVectorElementBase *clone(void) const;

public:
    void copy(const dtkVectorAbstractElement<T>& o);

public:
     qlonglong id(void) const;

public:
    T value(void) const;
    T value(qlonglong id) const;

public:
    bool       equal(const dtkVectorAbstractElement<T>& o) const;
    bool   lowerThan(const dtkVectorAbstractElement<T>& o) const;
    bool greaterThan(const dtkVectorAbstractElement<T>& o) const;

public:
     void advance(void);
     void  rewind(void);
     void advance(qlonglong j);
     void  rewind(qlonglong j);
};

// ///////////////////////////////////////////////////////////////////

#include "dtkVectorElement.tpp"

// 
// dtkVectorElement.h ends here
