// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include "dtkSparseMatrixLine.h"

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixLineElement implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkSparseMatrixLineElement<T>::dtkSparseMatrixLineElement(const dtkSparseMatrixAbstractLine<T> *line, qlonglong id) : l(line), m_id(id)
{
}

template < typename T > inline dtkSparseMatrixLineElement<T>::dtkSparseMatrixLineElement(const dtkSparseMatrixLineElement<T>& o) : l(o.l), m_id(o.m_id)
{
}

template < typename T > inline dtkSparseMatrixLineElement<T>::~dtkSparseMatrixLineElement(void)
{
}

template < typename T > inline dtkSparseMatrixLineElement<T>& dtkSparseMatrixLineElement<T>::operator = (const dtkSparseMatrixLineElement<T>& o)
{
    l = o.l;
    m_id = o.m_id;
    return *this;
}

template < typename T > inline qlonglong dtkSparseMatrixLineElement<T>::id(void) const
{
    return l->elementId(m_id);
}

template < typename T > inline qlonglong dtkSparseMatrixLineElement<T>::lineId(void) const
{
    return l->id();
}

template < typename T > inline T dtkSparseMatrixLineElement<T>::operator * (void) const
{
    return l->elementValue(m_id);
}

template < typename T > inline T dtkSparseMatrixLineElement<T>::operator [] (qlonglong j) const
{
    dtkSparseMatrixLineElement<T> o(*this);
    o += j;
    return *o;
}

template < typename T > inline bool dtkSparseMatrixLineElement<T>::operator == (const dtkSparseMatrixLineElement<T>& o) const
{
    return m_id == o.m_id;
}

template < typename T > inline bool dtkSparseMatrixLineElement<T>::operator != (const dtkSparseMatrixLineElement<T>& o) const
{
    return m_id != o.m_id;
}

template < typename T > inline bool dtkSparseMatrixLineElement<T>::operator < (const dtkSparseMatrixLineElement<T>& o) const
{
    return m_id < o.m_id;
}

template < typename T > inline bool dtkSparseMatrixLineElement<T>::operator <= (const dtkSparseMatrixLineElement<T>& o) const
{
    return m_id <= o.m_id;
}

template < typename T > inline bool dtkSparseMatrixLineElement<T>::operator > (const dtkSparseMatrixLineElement<T>& o) const
{
    return m_id > o.m_id;
}

template < typename T > inline bool dtkSparseMatrixLineElement<T>::operator >= (const dtkSparseMatrixLineElement<T>& o) const
{
    return m_id >= o.m_id;
}

template < typename T > inline dtkSparseMatrixLineElement<T>& dtkSparseMatrixLineElement<T>::operator ++ (void)
{
    ++m_id;
    return *this;
}

template < typename T > inline dtkSparseMatrixLineElement<T> dtkSparseMatrixLineElement<T>::operator ++ (int)
{
    dtkSparseMatrixLineElement<T> o(*this); 
    ++m_id;
    return o;
}

template < typename T > inline dtkSparseMatrixLineElement<T>& dtkSparseMatrixLineElement<T>::operator -- (void)
{
    --m_id;
    return *this;
}

template < typename T > inline dtkSparseMatrixLineElement<T> dtkSparseMatrixLineElement<T>::operator -- (int)
{
    dtkSparseMatrixLineElement<T> o(*this); 
    --m_id;
    return o;
}

template < typename T > inline dtkSparseMatrixLineElement<T>& dtkSparseMatrixLineElement<T>::operator += (qlonglong j)
{
    m_id += j;
    return *this;
}

template < typename T > inline dtkSparseMatrixLineElement<T>& dtkSparseMatrixLineElement<T>::operator -= (qlonglong j)
{
    m_id -= j; 
    return *this;
}

template < typename T > inline dtkSparseMatrixLineElement<T> dtkSparseMatrixLineElement<T>::operator + (qlonglong j) const
{
    dtkSparseMatrixLineElement<T> o(*this); 
    o += j;
    return o;
}

template < typename T > inline dtkSparseMatrixLineElement<T> dtkSparseMatrixLineElement<T>::operator - (qlonglong j) const
{
    dtkSparseMatrixLineElement<T> o(*this); 
    o -= j; 
    return o;
}

// 
// dtkSparseMatrixLineElement.tpp ends here
