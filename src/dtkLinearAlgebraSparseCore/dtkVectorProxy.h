// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <QtCore>

#include "dtkVectorAbstractData.h"

// ///////////////////////////////////////////////////////////////////
// dtkVectorProxy
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkVectorProxy
{
    dtkVectorAbstractData<T> *data;
    qlonglong m_i;

public:
    explicit dtkVectorProxy(dtkVectorAbstractData<T> *d, qlonglong i);
             dtkVectorProxy(const dtkVectorProxy& o);

public:
    ~dtkVectorProxy(void);

public:
    dtkVectorProxy& operator = (const dtkVectorProxy& o);

public:
    T value(void) const;

public:
    dtkVectorProxy& operator =  (const T& value);
    dtkVectorProxy& operator += (const T& value);
    dtkVectorProxy& operator -= (const T& value);
    dtkVectorProxy& operator *= (const T& value);
    dtkVectorProxy& operator /= (const T& value);

public:
    operator T() const;
};

// ///////////////////////////////////////////////////////////////////
// dtkVectorProxy operators
// ///////////////////////////////////////////////////////////////////

template < typename T > bool operator == (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator == (const dtkVectorProxy<T>& lhs, const T& rhs);
template < typename T > bool operator == (const T& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator != (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator != (const dtkVectorProxy<T>& lhs, const T& rhs);
template < typename T > bool operator != (const T& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator <  (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator <  (const dtkVectorProxy<T>& lhs, const T& rhs);
template < typename T > bool operator <  (const T& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator >  (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator >  (const dtkVectorProxy<T>& lhs, const T& rhs);
template < typename T > bool operator >  (const T& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator <= (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator <= (const dtkVectorProxy<T>& lhs, const T& rhs);
template < typename T > bool operator <= (const T& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator >= (const dtkVectorProxy<T>& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > bool operator >= (const dtkVectorProxy<T>& lhs, const T& rhs);
template < typename T > bool operator >= (const T& lhs, const dtkVectorProxy<T>& rhs);
template < typename T > QDebug& operator << (QDebug dbg, const dtkVectorProxy<T>& proxy);

// ///////////////////////////////////////////////////////////////////

#include "dtkVectorProxy.tpp"

// 
// dtkVectorProxy.h ends here
