// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkCore>

#include <QtCore>

#include <dtkLinearAlgebraSparseCoreExport.h>
#include "dtkSparseMatrixLine.h"

class dtkDistributedGraphTopology;

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixEngine interface
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkSparseMatrixEngine : public QObject
{
public:
             dtkSparseMatrixEngine(void) {;}
    virtual ~dtkSparseMatrixEngine(void) {;}

public:
    virtual dtkSparseMatrixEngine *clone(void) const = 0;

public:
    virtual bool copy(const dtkSparseMatrixEngine *other) = 0;

public:
    virtual dtkDistributedGraphTopology *graph(void) const { return NULL; }
    virtual void setGraph(dtkDistributedGraphTopology *) {;}

public:
    virtual void rlock(void)  {};
    virtual void wlock(void)  {};
    virtual void unlock(void) {};

public:
    virtual bool readMatrixMarket(const QString& filename) = 0;

public:
    virtual qlonglong     rowCount(void) const = 0;
    virtual qlonglong     colCount(void) const = 0;
    virtual qlonglong nonZeroCount(void) const = 0;

public:
    virtual void clear(void) = 0;

public:
    virtual void resize(qlonglong line_count) = 0;

public:
    virtual void insert(qlonglong i, qlonglong j, const T& value) = 0;
    virtual void remove(qlonglong i, qlonglong j) = 0;

public:
    virtual bool assemble(void) = 0;

public:
    virtual void fill(const T& value) = 0;
    virtual void mult(const T& value) = 0;
    virtual void add(dtkSparseMatrixEngine<T> *other) = 0;
    virtual void sub(dtkSparseMatrixEngine<T> *other) = 0;

public:
    virtual T at(qlonglong i, qlonglong j) const = 0;

public:
    virtual void    assign(qlonglong i, qlonglong j, const T& value) = 0;
    virtual void addAssign(qlonglong i, qlonglong j, const T& value) = 0;
    virtual void subAssign(qlonglong i, qlonglong j, const T& value) = 0;
    virtual void mulAssign(qlonglong i, qlonglong j, const T& value) = 0;
    virtual void divAssign(qlonglong i, qlonglong j, const T& value) = 0;

public:
    typedef dtkSparseMatrixLine<T> line_iterator;

    virtual line_iterator  begin(void) const = 0;
    virtual line_iterator    end(void) const = 0;
    virtual line_iterator cbegin(void) const { return this->begin(); }
    virtual line_iterator   cend(void) const { return   this->end(); }

public:
    virtual line_iterator lineAt(qlonglong line_id) const = 0;

public:
    virtual const T *data(void) const { return NULL; }
    virtual       T *data(void)       { return NULL; }
};

// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkSparseMatrixEnginePluginFactory : public dtkCorePluginFactory<dtkSparseMatrixEngine<T> > 
{
};

typedef dtkCorePluginFactoryTemplate dtkSparseMatrixEnginePluginFactoryTemplate;

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseMatrixEnginePlugin : public QObject
{
    Q_OBJECT

public:
             dtkSparseMatrixEnginePlugin(void) {}
    virtual ~dtkSparseMatrixEnginePlugin(void) {}
    
public:
    virtual void   initialize(void) = 0;
    virtual void uninitialize(void) = 0;
};

Q_DECLARE_INTERFACE(dtkSparseMatrixEnginePlugin, DTK_DECLARE_PLUGIN_INTERFACE(dtkSparseMatrixEngine))

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseMatrixEnginePluginManager : public dtkCorePluginManager<dtkSparseMatrixEnginePlugin> 
{
};

// 
// dtkSparseMatrixEngine.h ends here
