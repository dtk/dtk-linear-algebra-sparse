// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLinearAlgebraSparseCoreExport.h>

#include <QtCore>
#include <QRunnable>

#include <dtkCore>

template <typename T> class dtkSparseMatrix;
template <typename T> class dtkVector;
template <typename T> class dtkSparsePreconditioner;

typedef QHash<QString,int> qhash_string_int;
typedef QHash<QString,double> qhash_string_double;
Q_DECLARE_METATYPE(qhash_string_int)
Q_DECLARE_METATYPE(qhash_string_int*)
Q_DECLARE_METATYPE(qhash_string_double)
Q_DECLARE_METATYPE(qhash_string_double*)

// /////////////////////////////////////////////////////////////////
// dtkSparseMatrixSolver
// /////////////////////////////////////////////////////////////////

template < typename T > class dtkSparseSolver : public QRunnable
{
public:
             dtkSparseSolver(void) {;}
    virtual ~dtkSparseSolver(void) {;}

public:
    virtual void setMatrix(dtkSparseMatrix<T> *matrix) = 0;
    virtual void setRHSVector(dtkVector<T> *rhs_vector) = 0;
    virtual void setSolutionVector(dtkVector<T> *sol_vector) = 0;

    virtual void setOptionalParameters(const QHash<QString, int>& parameters) = 0;
    virtual void setOptionalParameters(const QHash<QString, double>& parameters) = 0;

public:
    virtual void run(void) = 0;

public:
    virtual dtkVector<T> *solutionVector(void) const = 0;
    virtual const dtkArray<T>& stats(void) const = 0;
};

Q_DECLARE_METATYPE(dtkSparseSolver<double> *)

// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkSparseSolverPluginFactory : public dtkCorePluginFactory<dtkSparseSolver<T> >
{
};

typedef dtkCorePluginFactoryTemplate dtkSparseSolverPluginFactoryTemplate;

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseSolverPlugin : public QObject
{
    Q_OBJECT

 public:
             dtkSparseSolverPlugin(void) {}
    virtual ~dtkSparseSolverPlugin(void) {}

 public:
    virtual void   initialize(void) = 0;
    virtual void uninitialize(void) = 0;
};

Q_DECLARE_INTERFACE(dtkSparseSolverPlugin, DTK_DECLARE_PLUGIN_INTERFACE(dtkSparseSolver))

// ///////////////////////////////////////////////////////////////////

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseSolverPluginManager : public dtkCorePluginManager<dtkSparseSolverPlugin>
{
};

//
// dtkSparseSolver.h ends here
