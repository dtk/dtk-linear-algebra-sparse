// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include "dtkSparseMatrixEngine.h"

#include <dtkCore>
#include <dtkDistributed>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineCSR interface
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkDistributedSparseMatrixEngineCSR : public dtkSparseMatrixEngine<T>
{
public:
     dtkDistributedSparseMatrixEngineCSR(void);
     dtkDistributedSparseMatrixEngineCSR(const dtkDistributedSparseMatrixEngineCSR& o);
    ~dtkDistributedSparseMatrixEngineCSR(void);

public:
    dtkDistributedSparseMatrixEngineCSR *clone(void) const { return new dtkDistributedSparseMatrixEngineCSR(*this); }

public:
    bool copy(const dtkSparseMatrixEngine<T> *other);

public:
    dtkDistributedGraphTopology *graph(void) const;
    void setGraph(dtkDistributedGraphTopology *graph);

public:
    bool readMatrixMarket(const QString& filename);

public:
    qlonglong     rowCount(void) const { return _N; }
    qlonglong     colCount(void) const { return _N; }
    qlonglong nonZeroCount(void) const { return m_graph ? m_graph->edgeCount() : 0LL; }

public:
    void clear(void);

public:
    void resize(qlonglong line_count);

public:
    void insert(qlonglong i, qlonglong j, const T& value);
    void remove(qlonglong i, qlonglong j); 

public:
    bool assemble(void);

public:
    void fill(const T& value);
    void mult(const T& value) ;
    void add(dtkSparseMatrixEngine<T> *other);
    void sub(dtkSparseMatrixEngine<T> *other);

public:
    T at(qlonglong i, qlonglong j) const;

public:
    void    assign(qlonglong i, qlonglong j, const T& value);
    void addAssign(qlonglong i, qlonglong j, const T& value);
    void subAssign(qlonglong i, qlonglong j, const T& value);
    void mulAssign(qlonglong i, qlonglong j, const T& value);
    void divAssign(qlonglong i, qlonglong j, const T& value);

public:
    typedef dtkSparseMatrixLine<T> line_iterator;

    line_iterator  begin(void) const;
    line_iterator    end(void) const;

public:
    line_iterator lineAt(qlonglong line_id) const;
    
public:
    const T *data(void) const;
          T *data(void);

private:
    qlonglong pos(qlonglong i, qlonglong j) const;

private:
    mutable bool is_assembled;
    qlonglong _N;
    QHash<qlonglong, T> m_val_hash;

private:
    dtkDistributedGraphTopology *m_graph;

private:
    dtkDistributedArray<T> *m_values;

private:
    mutable qlonglong m_current_id;
    mutable dtkDistributedGraphTopology::Neighbours m_current_n;
    mutable dtkDistributedGraphTopology::Neighbours::iterator m_current_it;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > dtkSparseMatrixEngine<T> *dtkDistributedSparseMatrixEngineCSRCreator(void)
{
    return new dtkDistributedSparseMatrixEngineCSR<T>();
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineLine
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkDistributedSparseMatrixEngineCSRLine : public dtkSparseMatrixLineBase < T, dtkDistributedSparseMatrixEngineCSRLine<T> >
{
    qlonglong m_id;
    dtkDistributedGraphTopology::vertex v_it;
    typename dtkDistributedArray<T>::const_iterator val_it;
    qlonglong m_line_size;

public:
    dtkDistributedSparseMatrixEngineCSRLine(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph, qlonglong id);
    dtkDistributedSparseMatrixEngineCSRLine(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph);

public:
    dtkDistributedSparseMatrixEngineCSRLine(const dtkDistributedSparseMatrixEngineCSRLine& o);

public:
    ~dtkDistributedSparseMatrixEngineCSRLine(void);

public:
    dtkDistributedSparseMatrixEngineCSRLine& operator = (const dtkDistributedSparseMatrixEngineCSRLine& o);

public:
    qlonglong id(void) const;

public:
    qlonglong  size(void) const;
    qlonglong width(void) const;

public:
    T elementValue(qlonglong id) const;
    qlonglong elementId(qlonglong id) const;

public:
    bool operator == (const dtkDistributedSparseMatrixEngineCSRLine& o) const;
    bool operator != (const dtkDistributedSparseMatrixEngineCSRLine& o) const;
    bool operator <  (const dtkDistributedSparseMatrixEngineCSRLine& o) const;
    bool operator <= (const dtkDistributedSparseMatrixEngineCSRLine& o) const;
    bool operator >  (const dtkDistributedSparseMatrixEngineCSRLine& o) const;
    bool operator >= (const dtkDistributedSparseMatrixEngineCSRLine& o) const;

public:
    dtkDistributedSparseMatrixEngineCSRLine& operator ++ (void);
    dtkDistributedSparseMatrixEngineCSRLine  operator ++ (int);
    dtkDistributedSparseMatrixEngineCSRLine& operator -- (void);
    dtkDistributedSparseMatrixEngineCSRLine  operator -- (int);
    dtkDistributedSparseMatrixEngineCSRLine& operator += (qlonglong j);
    dtkDistributedSparseMatrixEngineCSRLine& operator -= (qlonglong j);
    dtkDistributedSparseMatrixEngineCSRLine  operator +  (qlonglong j) const;
    dtkDistributedSparseMatrixEngineCSRLine  operator -  (qlonglong j) const;

protected:
    void advance(void);
    void advance(qlonglong j);
    void rewind(void);
    void rewind(qlonglong j);
};

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineCSRLineGlobal
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkDistributedSparseMatrixEngineCSRLineGlobal : public dtkSparseMatrixLineBase < T, dtkDistributedSparseMatrixEngineCSRLineGlobal<T> >
{
    dtkDistributedGraphTopology *m_graph;
    dtkDistributedArray<T>      *m_array;
    qlonglong m_id;
    qlonglong m_line_start_pos;
    qlonglong m_line_size;

public:
    dtkDistributedSparseMatrixEngineCSRLineGlobal(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph, qlonglong id);
    dtkDistributedSparseMatrixEngineCSRLineGlobal(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph);

public:
    dtkDistributedSparseMatrixEngineCSRLineGlobal(const dtkDistributedSparseMatrixEngineCSRLineGlobal& o);

public:
    ~dtkDistributedSparseMatrixEngineCSRLineGlobal(void);

public:
    dtkDistributedSparseMatrixEngineCSRLineGlobal& operator = (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o);

public:
    qlonglong id(void) const;

public:
    qlonglong  size(void) const;
    qlonglong width(void) const;

public:
    T elementValue(qlonglong id) const;
    qlonglong elementId(qlonglong id) const;

public:
    bool operator == (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const;
    bool operator != (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const;
    bool operator <  (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const;
    bool operator <= (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const;
    bool operator >  (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const;
    bool operator >= (const dtkDistributedSparseMatrixEngineCSRLineGlobal& o) const;

public:
    dtkDistributedSparseMatrixEngineCSRLineGlobal& operator ++ (void);
    dtkDistributedSparseMatrixEngineCSRLineGlobal  operator ++ (int);
    dtkDistributedSparseMatrixEngineCSRLineGlobal& operator -- (void);
    dtkDistributedSparseMatrixEngineCSRLineGlobal  operator -- (int);
    dtkDistributedSparseMatrixEngineCSRLineGlobal& operator += (qlonglong j);
    dtkDistributedSparseMatrixEngineCSRLineGlobal& operator -= (qlonglong j);
    dtkDistributedSparseMatrixEngineCSRLineGlobal  operator +  (qlonglong j) const;
    dtkDistributedSparseMatrixEngineCSRLineGlobal  operator -  (qlonglong j) const;

protected:
    void advance(void);
    void advance(qlonglong j);
    void rewind(void);
    void rewind(qlonglong j);
};

// ///////////////////////////////////////////////////////////////////

#include "dtkDistributedSparseMatrixEngineCSR.tpp"

// 
// dtkDistributedSparseMatrixEngineCSR.h ends here
