// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLinearAlgebraSparseSettings.h"

dtkLinearAlgebraSparseSettings::dtkLinearAlgebraSparseSettings(void) : QSettings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-linear-algebra-sparse")
{
    this->beginGroup("linear-algebra-sparse");
    if(!this->allKeys().contains("plugins"))
        this->setValue("plugins", QString());
    this->sync();
    this->endGroup();
}

dtkLinearAlgebraSparseSettings::~dtkLinearAlgebraSparseSettings(void)
{

}

//
// dtkLinearAlgebraSparseSettings.cpp ends here
