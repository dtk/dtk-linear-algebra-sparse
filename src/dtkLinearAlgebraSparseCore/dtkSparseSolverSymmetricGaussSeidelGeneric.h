// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include "dtkVector.h"
#include "dtkVectorAbstractData.h"
#include "dtkSparseMatrix.h"
#include "dtkSparseMatrixLine.h"

class dtkSparseSolverSymmetricGaussSeidelGenericPrivate;

class dtkSparseSolverSymmetricGaussSeidelGeneric : public dtkSparseSolver<double>
{

public:
     dtkSparseSolverSymmetricGaussSeidelGeneric(void);
    ~dtkSparseSolverSymmetricGaussSeidelGeneric(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    dtkSparseSolverSymmetricGaussSeidelGenericPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *dtkSparseSolverSymmetricGaussSeidelGenericGenericCreator(void)
{
    return new dtkSparseSolverSymmetricGaussSeidelGeneric();
}

//
// dtkSparseSolverSymmetricGaussSeidelGeneric.h ends here
