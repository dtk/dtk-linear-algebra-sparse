// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineDD implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkDistributedSparseMatrixEngineDD<T>::dtkDistributedSparseMatrixEngineDD(void) : dtkSparseMatrixEngine<T>(), is_assembled(false), _N(0), m_graph(NULL), m_values(NULL)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineDD<T>::dtkDistributedSparseMatrixEngineDD(const dtkDistributedSparseMatrixEngineDD<T>& o) : dtkSparseMatrixEngine<T>(), is_assembled(o.is_assembled), _N(o._N), m_graph(o.m_graph), m_values(new dtkDistributedArray<T>(*(o.m_values)))
{
    m_graph->ref();
}

template < typename T > inline dtkDistributedSparseMatrixEngineDD<T>::~dtkDistributedSparseMatrixEngineDD(void)
{
    this->clear();
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDD<T>::copy(const dtkSparseMatrixEngine<T> *other)
{
    if (other->objectName() == this->objectName()) {
        const dtkDistributedSparseMatrixEngineDD<T>& o = *static_cast<const dtkDistributedSparseMatrixEngineDD<T> *>(other);
        m_val_hash = o.m_val_hash;
        _N = o._N;
        is_assembled = o.is_assembled;

        if (o.m_graph) {
            m_graph = o.m_graph;
            m_graph->ref();
        } else {
            m_graph = NULL;
        }
        if (o.m_values) {
            if (m_values) {
                *m_values = *(o.m_values);
            } else {
                m_values = new dtkDistributedArray<T>(*(o.m_values));
            }
        } else {
            m_values = NULL;
        }
        return true;
    }

    return false;
}

template < typename T > inline dtkDistributedGraphTopology *dtkDistributedSparseMatrixEngineDD<T>::graph(void) const
{
    return m_graph;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::setGraph(dtkDistributedGraphTopology *graph)
{
    if (!graph)
        return;

    this->clear();

    m_graph = graph;
    m_graph->ref();

    _N = m_graph->size();

    if (m_graph->isAssembled()) {
        m_values = new dtkDistributedArray<T>(m_graph->edgeCount(), m_graph->edgeMapper());
        is_assembled = true;
    }
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::rlock(void)
{
    m_graph->rlock();
    m_values->rlock();
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::wlock(void)
{
    m_graph->wlock();
    m_values->wlock();
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::unlock(void)
{
    m_graph->unlock();
    m_values->unlock();
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDD<T>::readMatrixMarket(const QString& filename)
{
    this->clear();
 
    m_graph = new dtkDistributedGraphTopology;
    
    // dummy array, to be deleted and recreated with the good size in read
    dtkDistributedArray<T> *values = new dtkDistributedArray<T>(m_graph->communicator()->size());

    is_assembled = m_graph->readWithValues(filename, dtkDistributedGraphTopology::MatrixMarketFormat, values);

    if (is_assembled) {
        _N = m_graph->size();

        dtkDistributedMapper *dd_mapper = m_graph->m_dd.mapper;

        m_values = new dtkDistributedArray<T>(dd_mapper->count(), dd_mapper);
        auto v_it = values->begin();
        auto v_end = values->end();
        
        auto p_it = m_graph->m_dd.positions->begin();
        
        for(; v_it != v_end; ++v_it, ++p_it) {
            m_values->setAt(*p_it, *v_it);
        }

        delete values;

    } else {
        this->clear();
    }

    return is_assembled;
} 

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::clear(void)
{
    if(is_assembled) {
        m_graph->unlock();
        is_assembled = false;
    }

    _N = 0LL;
    m_val_hash.clear();

    if (m_graph && !m_graph->deref())
        delete m_graph;
    m_graph = NULL;

    if (m_values) {
        delete m_values;
    }
    m_values = NULL;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::resize(qlonglong line_count)
{ 
    if (_N == line_count)
        return;

    this->clear();
    
    _N = line_count;
    m_graph = new dtkDistributedGraphTopology(_N);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::insert(qlonglong i, qlonglong j, const T& value)
{
    if (is_assembled) {
        qDebug() << Q_FUNC_INFO << "Try to insert into an already assembled matrix. Nothing is done.";
        return;
    }

    m_val_hash.insert(i*_N+j , value);
    // m_val_map[i * _N + j] = value;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::remove(qlonglong i, qlonglong j)
{
    if (is_assembled) {
        qDebug() << Q_FUNC_INFO << "Try to remove from an already assembled matrix. Nothing is done.";
        return;
    }

    m_val_hash.remove(i * _N + j);
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDD<T>::assemble(void)
{
    if (is_assembled) {
        return false;
    }

    if (!m_graph) {
        m_graph = new dtkDistributedGraphTopology(_N);
    } else {
        if (m_graph->size() != _N)
            return false;
    }

    auto it  = m_val_hash.cbegin();
    auto end = m_val_hash.cend();
    for(; it != end; ++it) {
        qlonglong i = it.key() / _N;
        qlonglong j = it.key() % _N;
        m_graph->addEdge(i, j);
    }
    m_graph->assemble();

    if (m_values)
        delete m_values;

    m_values = new dtkDistributedArray<T>(m_graph->m_dd.mapper->count(), m_graph->m_dd.mapper);
    m_values->fill(T(0));

    const dtkArray<qlonglong, 0>& local_v_to_e = m_graph->m_dd.local_vertex_to_edge;
    const dtkArray<qlonglong, 0>& local_e_to_v = m_graph->m_dd.local_edge_to_vertex;
    const dtkArray<qlonglong, 0>& loc_to_glob  = m_graph->m_dd.loc_to_glob;

    auto vit = m_values->begin();

    qlonglong size_loc = local_v_to_e.size() - 1;

    for (qlonglong i_loc = 0; i_loc < size_loc; ++i_loc) {
        qlonglong i = loc_to_glob.at(i_loc);
        qlonglong start = local_v_to_e.at(i_loc);
        qlonglong end = local_v_to_e.at(i_loc + 1);
        for (qlonglong k = start; k < end; ++k) {
            qlonglong j = local_e_to_v.at(k);
            *vit = m_val_hash.take(i * _N + j);
            ++vit;
        }
    }

    dtkDistributedArray<qlonglong>& m_positions = *(m_graph->m_dd.positions);
    m_graph->communicator()->barrier();

    it = m_val_hash.cbegin();
    m_values->wlock(m_graph->wid());
    m_graph->rlock();
    m_positions.rlock();
    for (; it != end; ++it) {
        qlonglong i = it.key() / _N;
        qlonglong j = it.key() % _N;
        qlonglong pos;

        dtkDistributedGraphTopology::Neighbours n = (*m_graph)[i];
        dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
        dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();

        for(;nit != nend; ++nit) {
            if (*nit == j) {
                pos = m_positions.at(nit.id());
                break;
            }
        }
        if (pos < 0) {
            qDebug() << Q_FUNC_INFO << "Oups ! Negative position unexpected for (i,j)=" << i << j;
            continue;
        }
        if (i == j) {
            m_values->addAssign(pos, *it);
        } else {
            m_values->setAt(pos, *it);
        }
    }
    m_positions.unlock();
    m_graph->unlock();
    m_values->unlock(m_graph->wid());
    
    m_val_hash.clear();

    m_graph->communicator()->barrier();

    m_graph->rlock();
    is_assembled = true;
    return true;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::fill(const T& value)
{
    if (!is_assembled) {
        qDebug() << Q_FUNC_INFO << "Try to fill a not already assembled matrix. Nothing is done.";
        return;
    }
    m_values->fill(value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::mult(const T& value)
{
    if (!is_assembled) {
        dtkWarn()  << "Try to fill a not already assembled matrix. Nothing is done.";
        return;
    }
    *m_values *= value;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::add(dtkSparseMatrixEngine<T>* other)
{
    if (!is_assembled) {
        dtkWarn() << "Try to add a not already assembled matrix. Nothing is done.";
        return;
    }
    // current implementation only work if we have the same graph
    if ( (other->objectName() == this->objectName()) && (m_graph == other->graph())) {
        const dtkDistributedSparseMatrixEngineDD<T>& o = *static_cast<const dtkDistributedSparseMatrixEngineDD<T> *>(other);

        *m_values += *(o.m_values);
    } else {
        qCritical() << "Not the same graph, can't add sparse matrixes";
    }
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::sub(dtkSparseMatrixEngine<T>* other)
{
    if (!is_assembled) {
        dtkWarn()  << "Try to sub a not already assembled matrix. Nothing is done.";
        return;
    }
    if ( (other->objectName() == this->objectName()) && (m_graph == other->graph())) {
        const dtkDistributedSparseMatrixEngineDD<T>& o = *static_cast<const dtkDistributedSparseMatrixEngineDD<T> *>(other);

        *m_values -= *(o.m_values);
    } else {
        qCritical() << "Not the same graph, can't add sparse matrixes";
    }
}

template < typename T > inline T dtkDistributedSparseMatrixEngineDD<T>::at(qlonglong i, qlonglong j) const
{
    qlonglong position = this->pos(i, j);
    if (position < 0) {
        return T(0);
    }
    return m_values->at(position);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::assign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->setAt(position, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::addAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->addAssign(position, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::subAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->addAssign(position, -value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::mulAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->mulAssign(position, value);
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDD<T>::divAssign(qlonglong i, qlonglong j, const T& value)
{
    if (!is_assembled) {
        dtkWarn() << Q_FUNC_INFO << "Matrix has not been assembled. Nothing is done.";
        return;
    }

    qlonglong position = this->pos(i, j);
    if (position < 0) {
        dtkWarn() << Q_FUNC_INFO << "Element (i,j) does not exists in the matrix. Nothing is done.";
        return;
    }
    m_values->divAssign(position, value);
}

template < typename T > inline typename dtkDistributedSparseMatrixEngineDD<T>::line_iterator dtkDistributedSparseMatrixEngineDD<T>::begin(void) const
{
    return line_iterator( new dtkDistributedSparseMatrixEngineDDLineGlobal<T>(m_values, m_graph, m_graph->mapper()->firstIndex(m_graph->wid())) );
}

template < typename T > inline typename dtkDistributedSparseMatrixEngineDD<T>::line_iterator dtkDistributedSparseMatrixEngineDD<T>::end(void) const
{
    return line_iterator( new dtkDistributedSparseMatrixEngineDDLineGlobal<T>(m_values, m_graph) );
}

template < typename T > inline typename dtkDistributedSparseMatrixEngineDD<T>::line_iterator dtkDistributedSparseMatrixEngineDD<T>::lineAt(qlonglong line_id) const
{
    return line_iterator( new dtkDistributedSparseMatrixEngineDDLineGlobal<T>(m_values, m_graph, line_id) );
}

template < typename T > inline const T *dtkDistributedSparseMatrixEngineDD<T>::data(void) const
{
    return m_values->data();
}

template < typename T > inline T *dtkDistributedSparseMatrixEngineDD<T>::data(void)
{
    return m_values->data();
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineDD<T>::pos(qlonglong i, qlonglong j) const
{
    qlonglong i_loc = m_graph->m_dd.glob_to_loc.value(i, -1LL);
    
    if (i_loc > -1) {
        
        qlonglong offset = m_graph->m_dd.mapper->firstIndex(m_graph->wid());
        const dtkArray<qlonglong, 0>& local_v_to_e = m_graph->m_dd.local_vertex_to_edge;
        const dtkArray<qlonglong, 0>& local_e_to_v = m_graph->m_dd.local_edge_to_vertex;
        
        qlonglong start = local_v_to_e.at(i_loc);
        qlonglong end = local_v_to_e.at(i_loc + 1);
        for (qlonglong k = start; k < end; ++k) {
            if (j == local_e_to_v.at(k)) {
                return k + offset;
            }
        }
    }
    
    dtkDistributedGraphTopology::Neighbours n = (*m_graph)[i];
    dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
    dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();
    
    for(;nit != nend; ++nit) {
        if (*nit == j) {
            return m_graph->m_dd.positions->at(nit.id());
        }
    }
    
    return -1LL;
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineDDLineGlobal implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>::dtkDistributedSparseMatrixEngineDDLineGlobal(dtkDistributedArray<T> *values, dtkDistributedGraphTopology *graph, qlonglong id) : m_graph(graph), m_array(values), m_id(id)
{
    m_line_size = m_graph->neighbourCount(m_id);
    m_line_start_pos = m_graph->firstNeighbourPos(m_id);
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>::dtkDistributedSparseMatrixEngineDDLineGlobal(dtkDistributedArray<T> *values, dtkDistributedGraphTopology *graph) : m_graph(graph), m_array(values)
{
    m_id = m_graph->mapper()->lastIndex(m_graph->wid());
    m_line_size = m_graph->neighbourCount(m_id);
    m_line_start_pos = m_graph->firstNeighbourPos(m_id) + m_line_size;
    ++m_id;
    m_line_size = 0;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>::dtkDistributedSparseMatrixEngineDDLineGlobal(const dtkDistributedSparseMatrixEngineDDLineGlobal& o) : m_graph(o.m_graph), m_array(o.m_array), m_id(o.m_id), m_line_start_pos(o.m_line_start_pos), m_line_size(o.m_line_size)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>::~dtkDistributedSparseMatrixEngineDDLineGlobal(void)
{
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>& dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator = (const dtkDistributedSparseMatrixEngineDDLineGlobal& o)
{
    m_graph = o.m_graph;
    m_array = o.m_array;
    m_id = o.m_id;
    m_line_start_pos = o.m_line_start_pos;
    m_line_size = o.m_line_size;
    return *this;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineDDLineGlobal<T>::id(void) const
{
    return m_id;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineDDLineGlobal<T>::size(void) const
{
    return m_line_size;
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineDDLineGlobal<T>::width(void) const
{
    if (m_line_size > 1) {
        dtkDistributedGraphTopology::Neighbours n = (*m_graph)[m_id];
        return (*(--(n.end())) - *(n.begin()));
    }

    return 0LL;
}

template < typename T > inline T dtkDistributedSparseMatrixEngineDDLineGlobal<T>::elementValue(qlonglong id) const
{
    return m_array->at(m_graph->m_dd.positions->at(m_line_start_pos + id));
}

template < typename T > inline qlonglong dtkDistributedSparseMatrixEngineDDLineGlobal<T>::elementId(qlonglong id) const
{
    qlonglong pos_J = id % m_line_size;
    dtkDistributedGraphTopology::Neighbours n = (*m_graph)[m_id];
    qlonglong J = *(n.begin() + pos_J);
    return J;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator == (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const
{
    return m_id == o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator != (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const
{
    return m_id != o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator < (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const
{
    return m_id < o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator <= (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const
{
    return m_id <= o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator > (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const
{
    return m_id > o.m_id;
}

template < typename T > inline bool dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator >= (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const
{
    return m_id >= o.m_id;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>& dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator ++ (void)
{
    this->advance();
    return *this;
}

 template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T> dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator ++ (int)
{
    dtkDistributedSparseMatrixEngineDDLineGlobal o(*this);
    this->advance();
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>& dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator -- (void)
{
    this->rewind();
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T> dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator -- (int)
{
    dtkDistributedSparseMatrixEngineDDLineGlobal o(*this);
    this->rewind();
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>& dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator += (qlonglong j)
{
    this->advance(j);
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T>& dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator -= (qlonglong j)
{
    this->rewind(j);
    return *this;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T> dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator + (qlonglong j) const
{
    dtkDistributedSparseMatrixEngineDDLineGlobal o(*this);
    o += j;
    return o;
}

template < typename T > inline dtkDistributedSparseMatrixEngineDDLineGlobal<T> dtkDistributedSparseMatrixEngineDDLineGlobal<T>::operator - (qlonglong j) const
{
    dtkDistributedSparseMatrixEngineDDLineGlobal o(*this);
    o -= j;
    return o;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDDLineGlobal<T>::advance(void)
{
    m_line_start_pos += m_line_size;
    ++m_id;
    m_line_size = m_id != m_graph->size() ? m_graph->neighbourCount(m_id) : 0LL;
    
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDDLineGlobal<T>::rewind(void)
{
    --m_id;
    m_line_size = m_id >= 0  ? m_graph->neighbourCount(m_id) : 0LL;
    m_line_start_pos -= m_line_size;
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDDLineGlobal<T>::advance(qlonglong j)
{
    for (qlonglong i = 0; i < j; ++i) {
        this->advance();
    }
}

template < typename T > inline void dtkDistributedSparseMatrixEngineDDLineGlobal<T>::rewind(qlonglong j)
{
    for (qlonglong i = 0; i < j; ++i) {
        this->rewind();
    }
}


// 
// dtkDistributedSparseMatrixEngineDD.tpp ends here
