// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLinearAlgebraSparseCoreExport.h>

#include <QtCore>

class DTKLINEARALGEBRASPARSECORE_EXPORT dtkLinearAlgebraSparseSettings : public QSettings
{
public:
     dtkLinearAlgebraSparseSettings(void);
    ~dtkLinearAlgebraSparseSettings(void);
};

//
// dtkLinearAlgebraSparseSettings.h ends here
