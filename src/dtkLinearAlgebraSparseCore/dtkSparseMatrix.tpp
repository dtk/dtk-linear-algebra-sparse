// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#if defined(DTK_HAVE_ZLIB)
#include <dtkCore/dtkIOCompressor>
#endif

#include "dtkLinearAlgebraSparse.h"

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrix implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkSparseMatrix<T>::dtkSparseMatrix(void) : d(0)
{
}

template < typename T > inline dtkSparseMatrix<T>::dtkSparseMatrix(dtkSparseMatrixEngine<T> *engine) : d(engine)
{
}

template < typename T > inline dtkSparseMatrix<T>::dtkSparseMatrix(const dtkSparseMatrix<T>& other) : d(other.d->clone())
{
}

template < typename T > inline dtkSparseMatrix<T>::~dtkSparseMatrix(void)
{
    if (d)
        delete d;
    d = 0;
}

template < typename T > inline dtkSparseMatrix<T>& dtkSparseMatrix<T>::operator = (const dtkSparseMatrix<T>& other)
{
    if (!this->convert(other.dataType())) {
        d->copy(other.d);
    }

    return *this;
}

template < typename T > inline dtkSparseMatrix<T>& dtkSparseMatrix<T>::operator *= (const T& coef)
{
    d->mult(coef);
    return *this;
}

template < typename T > inline dtkSparseMatrix<T>& dtkSparseMatrix<T>::operator += (const dtkSparseMatrix<T>& other)
{
    d->add(other.d);
    return *this;
}

template < typename T > inline dtkSparseMatrix<T>& dtkSparseMatrix<T>::operator -= (const dtkSparseMatrix<T>& other)
{
    d->sub(other.d);
    return *this;
}

template < typename T > inline QString dtkSparseMatrix<T>::dataType(void) const
{
    return d->objectName();
}

template < typename T > inline bool dtkSparseMatrix<T>::convert(const QString& data_type)
{
    if (dataType() != data_type) {
        dtkSparseMatrixEngine<T> *od = d;
        dtkSparseMatrixEngine<T> *nd = dtkLinearAlgebraSparse::matrixEngine::pluginFactory<double>().create(data_type);
        nd->setObjectName(data_type);

        nd->copy(od);

        // nd->resize(od->rowCount(), od->colCount());
        // nd->reserve(od->nonZeroCount());

        // line_iterator lit  = begin();
        // line_iterator lend = end();

        // for(; lit != lend; ++lit) {
        //     dtkSparseMatrixLineElement<T> eit  = lit.begin();
        //     dtkSparseMatrixLineElement<T> eend = lit.end();

        //     for(; eit != eend; ++eit) {
        //         nd->append(lit.id(), eit.id(), eit.value());
        //     }
        // }
        // nd->build();

        d = nd;
        delete od;

        if(d->graph()->isAssembled())
            d->graph()->rlock();

        return true;
    }

    return false;
}

template < typename T > dtkDistributedGraphTopology *dtkSparseMatrix<T>::graph(void)
{
    return d->graph();
}

template < typename T > void dtkSparseMatrix<T>::setGraph(dtkDistributedGraphTopology *graph)
{
    d->setGraph(graph);
}

template < typename T > bool dtkSparseMatrix<T>::readMatrixMarket(const QString& filename)
{
    return d->readMatrixMarket(filename);
}

template < typename T > bool dtkSparseMatrix<T>::writeMatrixMarket(const QString& filename)
{
    QFile file(filename);
    QIODevice *in;

    if(filename.isEmpty()) {
        qWarning() << "output filename is empty !";
        return false;
    }

    QIODevice::OpenMode mode = QIODevice::WriteOnly;
    QFileInfo info = QFileInfo(filename);
#if defined(DTK_HAVE_ZLIB)
    if (info.suffix() == "gz") {
        in = new dtkIOCompressor(&file);
    } else {
        in = &file;
        mode |= QIODevice::Text;
    }
#else
    in = &file;
    mode |= QIODevice::Text;
#endif

    if (!in->open(mode))
        return false;

    // to avoid troubles with floats separators ('.' and not ',')
    QLocale::setDefault(QLocale::c());
#if defined (Q_OS_UNIX) && !defined(Q_OS_MAC)
    setlocale(LC_NUMERIC, "C");
#endif

    //FIXME: handle type long/double
    in->write("%%MatrixMarket matrix coordinate real general\n");
    in->write(qPrintable(QString::number(this->rowCount()) + " "+QString::number(this->colCount())+" "+QString::number(this->nonZeroCount())+"\n"));
    //FIXME: use iterators on non empty values instead of this inefficient implemetation
    for (qlonglong i = 0; i < this->rowCount(); ++i) {
        dtkSparseMatrix<double>::line_iterator lit = this->lineAt(i);
        dtkSparseMatrixLineElement<T>  it = lit.begin();
        dtkSparseMatrixLineElement<T>  end = lit.end();

            for (; it != end; it++) {
            qlonglong j = it.id();
            in->write(qPrintable(QString::number(i+1)+ " "+ QString::number(j+1)+ " " + QString::number(*it,'g',16) +"\n"));
        }
    }
    in->close();
    return true;
}

template < typename T > inline void dtkSparseMatrix<T>::assemble(void)
{
    d->assemble();
}

template < typename T > inline void dtkSparseMatrix<T>::rlock(void)
{
    d->rlock();
}

template < typename T > inline void dtkSparseMatrix<T>::wlock(void)
{
    d->wlock();
}

template < typename T > inline void dtkSparseMatrix<T>::unlock(void)
{
    d->unlock();
}

template < typename T > inline qlonglong dtkSparseMatrix<T>::rowCount(void) const
{
    return d->rowCount();
}

template < typename T > inline qlonglong dtkSparseMatrix<T>::colCount(void) const
{
    return d->colCount();
}

template < typename T > inline qlonglong dtkSparseMatrix<T>::nonZeroCount(void) const
{
    return d->nonZeroCount();
}

template < typename T > inline void dtkSparseMatrix<T>::clear(void)
{
    d->clear();
}

template < typename T > inline void dtkSparseMatrix<T>::resize(qlonglong line_count)
{
    d->resize(line_count);
}

template < typename T > inline void dtkSparseMatrix<T>::insert(qlonglong i, qlonglong j, const T& value)
{
    d->insert(i, j, value);
}

template < typename T > inline void dtkSparseMatrix<T>::remove(qlonglong i, qlonglong j)
{
    d->remove(i, j);
}

template < typename T > inline void dtkSparseMatrix<T>::setAt(qlonglong i, qlonglong j, const T& value)
{
    d->assign(i, j, value);
}

template < typename T > inline void dtkSparseMatrix<T>::addAssign(qlonglong i, qlonglong j, const T& value)
{
    d->addAssign(i, j, value);
}

template < typename T > inline void dtkSparseMatrix<T>::fill(const T& value)
{
    d->fill(value);
}

template < typename T > inline T dtkSparseMatrix<T>::at(qlonglong i, qlonglong j) const
{
    return d->at(i, j);
}

template < typename T > inline const typename dtkSparseMatrix<T>::reference dtkSparseMatrix<T>::operator () (qlonglong i, qlonglong j) const
{
    return reference(d, i, j);
}

template < typename T > inline typename dtkSparseMatrix<T>::reference dtkSparseMatrix<T>::operator () (qlonglong i, qlonglong j)
{
    return reference(d, i, j);
}

template < typename T > inline typename dtkSparseMatrix<T>::line_iterator dtkSparseMatrix<T>::begin(void) const
{
    return d->begin();
}

template < typename T > inline typename dtkSparseMatrix<T>::line_iterator dtkSparseMatrix<T>::end(void) const
{
    return d->end();
}

template < typename T > inline typename dtkSparseMatrix<T>::line_iterator dtkSparseMatrix<T>::lineAt(qlonglong line_id) const
{
    return d->lineAt(line_id);
}

template < typename T > inline const T *dtkSparseMatrix<T>::data(void) const
{
    return d->data();
}

template < typename T > inline T *dtkSparseMatrix<T>::data(void)
{
    return d->data();
}

template < typename T > inline const T *dtkSparseMatrix<T>::constData(void) const
{
    return d->data();
}

//
// dtkSparseMatrix.tpp ends here




