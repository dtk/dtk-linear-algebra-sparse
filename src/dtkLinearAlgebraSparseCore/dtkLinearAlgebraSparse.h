// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLinearAlgebraSparseCoreExport.h>

#include <dtkCore>

#include "dtkSparseMatrixEngine.h"
#include "dtkSparseSolver.h"
#include "dtkSparsePreconditioner.h"
#include "dtkVectorAbstractData.h"

template <typename T> class dtkSparseMatrix;
template <typename T> class dtkVector;
template <typename T> class dtkSparseSystemBuilder;

// ///////////////////////////////////////////////////////////////////
// dtkLinearAlgebraSparse factories and managers
// ///////////////////////////////////////////////////////////////////

namespace dtkLinearAlgebraSparse
{
    namespace pluginManager {
        DTKLINEARALGEBRASPARSECORE_EXPORT void initialize(QString path = QString());
        DTKLINEARALGEBRASPARSECORE_EXPORT void setVerboseLoading(bool verbose = true);
        DTKLINEARALGEBRASPARSECORE_EXPORT void setAutoLoading(bool auto_load);
    }

    template <typename T> dtkSparseSystemBuilder<T> *builder();

    namespace matrixFactory {
        template <typename T> dtkSparseMatrix<T> *create(const QString& data_type);
    }

    namespace matrixEngine {
        DTKLINEARALGEBRASPARSECORE_EXPORT void                                        initialize(const QString& path);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseMatrixEnginePluginManager&         pluginManager(void);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseMatrixEnginePluginFactoryTemplate& pluginFactoryTemplate(void);
        template < typename T >       dtkSparseMatrixEnginePluginFactory<T>&      pluginFactory(void);
    }

    namespace vectorFactory {
        template <typename T> dtkVector<T> *create(const QString& data_type);
    }

    namespace vectorData {
        DTKLINEARALGEBRASPARSECORE_EXPORT void                                        initialize(const QString& path);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkVectorAbstractDataPluginManager&         pluginManager(void);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkVectorAbstractDataPluginFactoryTemplate& pluginFactoryTemplate(void);
        template <typename T>       dtkVectorAbstractDataPluginFactory<T>&        pluginFactory(void);
    }

    namespace solver {
        DTKLINEARALGEBRASPARSECORE_EXPORT void                                  initialize(const QString& path);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseSolverPluginManager&         pluginManager(void);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseSolverPluginFactoryTemplate& pluginFactoryTemplate(void);
        template <typename T>       dtkSparseSolverPluginFactory<T>&        pluginFactory(void);
    }

    namespace solverFactory {
        template <typename T> dtkSparseSolver<T> *create(const QString& solver_type);
    }

    namespace preconditioner {
        DTKLINEARALGEBRASPARSECORE_EXPORT void                                          initialize(const QString& path);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparsePreconditionerPluginManager&         pluginManager(void);
        DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparsePreconditionerPluginFactoryTemplate& pluginFactoryTemplate(void);
        template < typename T >       dtkSparsePreconditionerPluginFactory<T>&      pluginFactory(void);
    }
}

// ///////////////////////////////////////////////////////////////////
// dtkLinearAlgebraSparse template implementations
// ///////////////////////////////////////////////////////////////////

template <typename T> inline dtkSparseSystemBuilder<T> *builder(void)
{
    return dtkSparseSystemBuilder<T>::instance();
}

template <typename T> inline dtkSparseMatrix<T> *dtkLinearAlgebraSparse::matrixFactory::create(const QString& data_type)
{
    dtkSparseMatrixEngine<T> *engine = dtkLinearAlgebraSparse::matrixEngine::pluginFactory<T>().create(data_type);
    if (!engine) {
        qDebug() << Q_FUNC_INFO << "Error, matrix data " << data_type << " does not exist or is not registered.";
        return NULL;
    }
    engine->setObjectName(data_type);

    return new dtkSparseMatrix<T>(engine);
}

template <typename T> inline dtkSparseMatrixEnginePluginFactory<T>& dtkLinearAlgebraSparse::matrixEngine::pluginFactory(void)
{
    return *dtkLinearAlgebraSparse::matrixEngine::pluginFactoryTemplate().pluginFactory<dtkSparseMatrixEnginePluginFactory<T> >();
}

template <typename T> inline dtkVector<T> *dtkLinearAlgebraSparse::vectorFactory::create(const QString& data_type)
{
    dtkVectorAbstractData<T> *data = dtkLinearAlgebraSparse::vectorData::pluginFactory<T>().create(data_type);

    if (!data) {
        qDebug() << Q_FUNC_INFO << "Error, vector data " << data_type << " does not exist or is not registered.";
        return NULL;
    }

    data->setObjectName(data_type);

    return new dtkVector<T>(data);
}

template <typename T> inline dtkVectorAbstractDataPluginFactory<T>& dtkLinearAlgebraSparse::vectorData::pluginFactory(void)
{
    return *dtkLinearAlgebraSparse::vectorData::pluginFactoryTemplate().pluginFactory<dtkVectorAbstractDataPluginFactory<T> >();
}

template <typename T> inline dtkSparseSolverPluginFactory<T>& dtkLinearAlgebraSparse::solver::pluginFactory(void)
{
    return *dtkLinearAlgebraSparse::solver::pluginFactoryTemplate().pluginFactory<dtkSparseSolverPluginFactory<T> >();
}

template <typename T> inline dtkSparseSolver<T> *dtkLinearAlgebraSparse::solverFactory::create(const QString& solver_type)
{
    dtkSparseSolver<T> *solver = dtkLinearAlgebraSparse::solver::pluginFactory<T>().create(solver_type);

    if (!solver) {
        qDebug() << Q_FUNC_INFO << "Error, solver " << solver_type << " does not exist or is not registered.";
    }

    return solver;
}

template <typename T> inline dtkSparsePreconditionerPluginFactory<T>& dtkLinearAlgebraSparse::preconditioner::pluginFactory(void)
{
    return *dtkLinearAlgebraSparse::preconditioner::pluginFactoryTemplate().pluginFactory<dtkSparsePreconditionerPluginFactory<T> >();
}

//
// dtkLinearAlgebraSparse.h ends here
