/* dtkSparseSystemBuilder.h --- 
 * 
 * Version: 
 * 
 */

/* Commentary: 
 * 
 */

/* Change log:
 * 
 */

#pragma once

#include "dtkLinearAlgebraSparse"

#include <dtkCore>
#if defined(DTK_BUILD_DISTRIBUTED)
#include <dtkDistributed>
#include <QJsonDocument>
#endif

template <typename T> class dtkSparseSolver;
template <typename T> class dtkSparseMatrix;
template <typename T> class dtkVector;

template < typename T >
class DTKLINEARALGEBRASPARSECORE_EXPORT dtkSparseSystemBuilder
{
public :
    static dtkSparseSystemBuilder<T> *instance(); //return smart pointer
    void init(const QString& solver_type);
    bool autoLoading(void);
    dtkSparseSolver<T> *createSolver(); 
    dtkSparseMatrix<T> *createMatrix(); 
    dtkVector<T> *createVector(); 

public:
    void setAutoLoading(bool auto_load);
    
private:
    dtkSparseSystemBuilder<T> (void);
    static dtkSparseSystemBuilder<T> * _instance;
    QString solver_impl;
    QString matrix_impl;
    QString vector_impl;
    bool auto_load;
};

template < typename T >
dtkSparseSystemBuilder<T> *dtkSparseSystemBuilder<T>::_instance = 0;
 
///
#include "dtkSparseSystemBuilder.tpp"
