// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include "dtkSparseSolver.h"

class dtkSparseSolverJacobiPrivate;

class dtkSparseSolverJacobi : public dtkSparseSolver<double>
{

public:
     dtkSparseSolverJacobi(void);
    ~dtkSparseSolverJacobi(void);

public:
    void setMatrix(dtkSparseMatrix<double> *matrix);
    void setRHSVector(dtkVector<double> *rhs_vector);
    void setSolutionVector(dtkVector<double> *sol_vector);

    void setOptionalParameters(const QHash<QString, int>& parameters);
    void setOptionalParameters(const QHash<QString, double>& parameters);

public:
    void run(void);

public:
    dtkVector<double> *solutionVector(void) const;
    const dtkArray<double>& stats(void) const;

protected:
    dtkSparseSolverJacobiPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

inline dtkSparseSolver<double> *dtkSparseSolverJacobiGenericCreator(void)
{
    return new dtkSparseSolverJacobi();
}

//
// dtkSparseSolverJacobi.h ends here
