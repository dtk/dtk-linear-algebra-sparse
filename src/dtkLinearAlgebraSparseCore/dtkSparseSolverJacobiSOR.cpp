// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseSolverJacobiSOR.h"
#include "dtkVector.h"
#include "dtkVectorAbstractData.h"
#include "dtkSparseMatrix.h"
#include "dtkSparseMatrixLine.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkSparseSolverJacobiSORPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkVector<double> *rhs_vector;
    dtkVector<double> *sol_vector;

public:
    QHash<QString, int> params_int;
    QHash<QString, double> params_real;

    int number_of_iterations;
    double residual_reduction_order;

public:
    bool initialized;
    double variation;
    dtkVector<double> *tmp_vector;
    dtkVector<double> *old_vector;

public:
    dtkArray<double> relaxators;

public:
    dtkArray<double> stats;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkSparseSolverJacobiSOR::dtkSparseSolverJacobiSOR(void) : dtkSparseSolver<double>(), d(new dtkSparseSolverJacobiSORPrivate)
{
    d->matrix = NULL;
    d->rhs_vector = NULL;
    d->sol_vector = NULL;

    d->initialized = false;
    d->tmp_vector = NULL;
    d->old_vector = NULL;

    d->residual_reduction_order = 1.e-6;
    d->number_of_iterations = 50;
}

dtkSparseSolverJacobiSOR::~dtkSparseSolverJacobiSOR(void)
{
    // if (d->tmp_vector)
    //     delete d->tmp_vector;
}

void dtkSparseSolverJacobiSOR::setMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;
}

void dtkSparseSolverJacobiSOR::setRHSVector(dtkVector<double> *rhs_vector)
{
    d->rhs_vector = rhs_vector;
}

void dtkSparseSolverJacobiSOR::setSolutionVector(dtkVector<double> *sol_vector)
{
    d->sol_vector = sol_vector;
}

void dtkSparseSolverJacobiSOR::setOptionalParameters(const QHash<QString, int>& parameters)
{
     QHash<QString, int>::const_iterator it = parameters.find("number_of_iterations");

     if(it != parameters.end())
         d->params_int.insert("number_of_iterations", it.value());

     d->number_of_iterations = d->params_int.value("number_of_iterations");
}

void dtkSparseSolverJacobiSOR::setOptionalParameters(const QHash<QString, double>& parameters)
{
     QHash<QString, double>::const_iterator it = parameters.find("residual_reduction_order");

     if(it != parameters.end())
         d->params_real.insert("residual_reduction_order", it.value());

     d->residual_reduction_order = d->params_real.value("residual_reduction_order");
}

void dtkSparseSolverJacobiSOR::run(void)
{
    bool stop_criterion = false;
    qlonglong iteration_count = 0;
    double variation_ini = 1;

    double neighbour;
    double diag;

    const dtkSparseMatrix<double>& mat = *(d->matrix);
    const dtkVector<double>& rhs = *(d->rhs_vector);

    if (!d->initialized) {
        if (!d->sol_vector) {
            d->sol_vector = new dtkVector<double>(rhs);
        }
        d->tmp_vector = new dtkVector<double>(rhs);
        d->old_vector = new dtkVector<double>(rhs);
        d->initialized = true;
    }

    dtkVector<double>& sol = *(d->sol_vector);
    dtkVector<double>& tmp = *(d->tmp_vector);
    dtkVector<double>& old = *(d->old_vector);

    sol.fill(0.0);

    dtkSparseMatrixLine<double> line_it;
    dtkSparseMatrixLine<double> line_end = mat.end();

    d->rhs_vector->rlock();

    bool relaxators_computed = false;

    double max_r = -1;
    double min_r = 1.e15;
    double avg_r = 0;

    dtkArray<double> relax_avg;

    d->variation = 1;

    while(!stop_criterion) {
        line_it = mat.begin();

        if (!relaxators_computed) {

            auto lit  = mat.begin();
            auto lend = mat.end();
            qlonglong size = 0;
            for(; lit != lend; ++lit, ++size) {}

            d->relaxators.reserve(size);

            double extra_sum;
            double diag;

            lit  = mat.begin();
            for (qlonglong i = 0; lit != lend; ++lit, ++i) {

                dtkSparseMatrixLineElement<double> elt_it  = lit.begin();
                dtkSparseMatrixLineElement<double> elt_end = lit.end();

                extra_sum = 0;

                for(;elt_it != elt_end; ++elt_it) {
                    if (elt_it.id() != lit.id()) {
                        extra_sum += std::abs(*elt_it);
                    } else {
                        diag = std::abs(*elt_it);
                    }
                }

                d->relaxators << diag / extra_sum;

                max_r = std::max(max_r, d->relaxators.last());
                min_r = std::min(min_r, d->relaxators.last());
                avg_r += d->relaxators.last();

            }
            avg_r /= d->relaxators.size();
            //avg_r = 0.5 * (min_r + max_r);

            // lit  = mat.begin();

            // relax_avg.resize(d->relaxators.size());
            // relax_avg.fill(0.);

            // for (qlonglong i = 0; lit != lend; ++lit, ++i) {
            //     relax_avg[lit.id()] = lit.size();
            // }

            relaxators_computed = true;
        }

        //if (iteration_count == 0) qDebug() << "OOOOOOOOO" << relax_avg;
         //if (iteration_count == 0) qDebug() << "OOOOOOOOO" << d->relaxators;

        // need the previous solution to update the new one
        tmp = sol;

        sol.wlock();
        old.wlock();
        tmp.rlock();

        double spin = 0.5 * avg_r * tanh((variation_ini - d->variation) / variation_ini);
        double relax;
        if (iteration_count%2 == 1)
            relax = max_r * (1. + spin);
        else
            relax = 1. - spin;

        if(iteration_count < 1)
            qDebug () << min_r << avg_r << max_r;

        for (; line_it != line_end; ++line_it) {

            qlonglong i = line_it.id();

            dtkSparseMatrixLineElement<double> elt_it  = line_it.begin();
            dtkSparseMatrixLineElement<double> elt_end = line_it.end();

            //if(relax_cond == 0) {

                

                neighbour = 0;
                diag = 0;

                for(;elt_it != elt_end; ++elt_it) {

                    qlonglong j = elt_it.id();

                    if (j != i) {
                        neighbour += (*elt_it) * tmp[j];
                    } else {
                        diag = (*elt_it);
                    }
                }
                sol[i] = (1. - relax) * tmp[i] + relax * (rhs.at(i) - neighbour) / diag ;

            // } else {

            //     neighbour = 0;
            //     diag = 0;

            //     for(;elt_it != elt_end; ++elt_it) {

            //         qlonglong j = elt_it.id();

            //         if (j != i) {
            //             neighbour += (*elt_it) * tmp[j];
            //         } else {
            //             diag = (*elt_it);
            //         }
            //     }
            //     old[i] = (1. - min_relax) * tmp[i] + min_relax * (rhs.at(i) - neighbour) / diag ;
            // }
        }

        // line_it = mat.begin();
        // for (; line_it != line_end; ++line_it) {

        //     qlonglong i = line_it.id();

        //     if (iteration_count == 0 || d->variation < 0.1)
        //         //relax_cond = iteration_count % (qlonglong(relax_avg[i] / 8));
        //         relax_cond = iteration_count % 15;

        //     if(relax_cond != 0) {
        //         dtkSparseMatrixLineElement<double> elt_it  = line_it.begin();
        //         dtkSparseMatrixLineElement<double> elt_end = line_it.end();

        //         neighbour = 0;
        //         diag = 0;

        //         for(;elt_it != elt_end; ++elt_it) {

        //             qlonglong j = elt_it.id();

        //             if (j != i) {
        //                 neighbour += (*elt_it) * old[j];
        //             } else {
        //                 diag = (*elt_it);
        //             }
        //         }
        //         sol[i] = tmp[i] - max_r * old[i] + max_r * (rhs.at(i) - neighbour) / diag ;
        //     }
        // }
        //
        tmp.unlock();
        old.unlock();
        sol.unlock();

        tmp -= sol;
        d->variation = tmp.asum();

        if(iteration_count == 0)
            variation_ini = d->variation;

        // if (iteration_count%5 == 0)
        //     qDebug() << iteration_count << d->variation << variation_ini << relax << spin;

        if(iteration_count == d->number_of_iterations || d->variation/variation_ini < d->residual_reduction_order) {
            //if(iteration_count == d->number_of_iterations || d->variation < d->residual_reduction_order) {
            qDebug() << "stop criterion reached, iteration:" <<  iteration_count;
            stop_criterion = true;
        }

        ++iteration_count;

        sol.clearCache();
        tmp.clearCache();
    }
    d->rhs_vector->unlock();

}

dtkVector<double> *dtkSparseSolverJacobiSOR::solutionVector(void) const
{
    return d->sol_vector;
}

const dtkArray<double>& dtkSparseSolverJacobiSOR::stats(void) const
{
    return d->stats;
}

//
// dtkSparseSolverJacobiSOR.cpp ends here
