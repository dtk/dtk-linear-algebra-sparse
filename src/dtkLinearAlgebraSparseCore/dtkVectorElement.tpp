// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

// ///////////////////////////////////////////////////////////////////
// dtkVectorElementBase CRTP implementation
// ///////////////////////////////////////////////////////////////////

template <typename T, typename Element> inline dtkVectorElementBase<T, Element> *dtkVectorElementBase<T, Element>::clone(void) const
{
    return new Element(static_cast<const Element&>(*this));
}

template <typename T, typename Element> inline void dtkVectorElementBase<T, Element>::copy(const dtkVectorAbstractElement<T>& o)
{
    static_cast<Element&>(*this) = static_cast<const Element&>(o);
}

template <typename T, typename Element> inline qlonglong dtkVectorElementBase<T, Element>::id(void) const
{
    return (static_cast<const Element&>(*this)).id();
}

template <typename T, typename Element> inline T dtkVectorElementBase<T, Element>::value(void) const
{
    return *(static_cast<const Element&>(*this));
}

template <typename T, typename Element> inline T dtkVectorElementBase<T, Element>::value(qlonglong j) const
{
    return (static_cast<const Element&>(*this))[j];
}

template <typename T, typename Element> inline bool dtkVectorElementBase<T, Element>::equal(const dtkVectorAbstractElement<T>& o) const
{
    return (static_cast<const Element&>(*this) == static_cast<const Element&>(o));
}

template <typename T, typename Element> inline bool dtkVectorElementBase<T, Element>::lowerThan(const dtkVectorAbstractElement<T>& o) const
{
    return (static_cast<const Element&>(*this) < static_cast<const Element&>(o));
}

template <typename T, typename Element> inline bool dtkVectorElementBase<T, Element>::greaterThan(const dtkVectorAbstractElement<T>& o) const
{
    return (static_cast<const Element&>(*this) > static_cast<const Element&>(o));
}

template <typename T, typename Element> inline void dtkVectorElementBase<T, Element>::advance(void)
{
    ++(static_cast<Element&>(*this));
}

template <typename T, typename Element> inline void dtkVectorElementBase<T, Element>::advance(qlonglong j)
{
    static_cast<Element&>(*this) += j;
}

template <typename T, typename Element> inline void dtkVectorElementBase<T, Element>::rewind(void)
{
    --(static_cast<Element&>(*this));
}

template <typename T, typename Element> inline void dtkVectorElementBase<T, Element>::rewind(qlonglong j)
{
    static_cast<Element&>(*this) -= j;
}

// ///////////////////////////////////////////////////////////////////
// dtkVectorElement implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkVectorElement<T>::dtkVectorElement(dtkVectorAbstractElement<T> *e) : elt(e)
{
}

template < typename T > inline dtkVectorElement<T>::dtkVectorElement(const dtkVectorElement& o) : elt(o.elt->clone()) 
{
}

template < typename T > inline dtkVectorElement<T>::dtkVectorElement(dtkVectorElement&& o) : elt(0)
{
    std::swap(elt, o.elt);
}

template < typename T > inline dtkVectorElement<T>::~dtkVectorElement(void)
{
}

template < typename T > inline dtkVectorElement<T>& dtkVectorElement<T>::operator = (const dtkVectorElement& o)
{
    elt->copy(*(o.elt)); 
    return *this;
}

template < typename T > inline dtkVectorElement<T>& dtkVectorElement<T>::operator = (dtkVectorElement&& o)
{
    std::swap(elt, o.elt); 
    return *this;
}

template < typename T > inline qlonglong dtkVectorElement<T>::id(void) const
{
    return elt->id();
}

template < typename T > inline T dtkVectorElement<T>::operator * (void) const
{
    return elt->value();
}

template < typename T > inline T dtkVectorElement<T>::operator [] (qlonglong j) const
{
    return elt->value(j);
}

template < typename T > inline bool dtkVectorElement<T>::operator == (const dtkVectorElement& o) const
{
    return elt->equal(*(o.elt));
}

template < typename T > inline bool dtkVectorElement<T>::operator != (const dtkVectorElement& o) const
{
    return !elt->equal(*(o.elt));
}

template < typename T > inline bool dtkVectorElement<T>::operator < (const dtkVectorElement& o) const
{
    return elt->lowerThan(*(o.elt));
}

template < typename T > inline bool dtkVectorElement<T>::operator <= (const dtkVectorElement& o) const
{
    return (elt->lowerThan(*(o.elt)) || elt->equal(*(o.elt)));
}

template < typename T > inline bool dtkVectorElement<T>::operator > (const dtkVectorElement& o) const
{
    return elt->greaterThan(*(o.elt));
}

template < typename T > inline bool dtkVectorElement<T>::operator >= (const dtkVectorElement& o) const
{
    return (elt->greaterThan(*(o.elt)) || elt->equal(*(o.elt)));
}

template < typename T > inline dtkVectorElement<T>& dtkVectorElement<T>::operator ++ (void)
{
    elt->advance(); 
    return *this;
}

template < typename T > inline dtkVectorElement<T> dtkVectorElement<T>::operator ++ (int)
{
    dtkVectorElement o(*this); 
    elt->advance();
    return o;
}

template < typename T > inline dtkVectorElement<T>& dtkVectorElement<T>::operator -- (void)
{
    elt->rewind(); 
    return *this;
}

template < typename T > inline dtkVectorElement<T> dtkVectorElement<T>::operator -- (int)
{
    dtkVectorElement o(*this); 
    elt->rewind(); 
    return o;
}

template < typename T > inline dtkVectorElement<T>& dtkVectorElement<T>::operator += (qlonglong j)
{
    elt->advance(j); 
    return *this;
}

template < typename T > inline dtkVectorElement<T>& dtkVectorElement<T>::operator -= (qlonglong j)
{
    elt->rewind(j); 
    return *this;
}

template < typename T > inline dtkVectorElement<T> dtkVectorElement<T>::operator + (qlonglong j) const
{
    dtkVectorElement o(*this); 
    o += j; 
    return o;
}

template < typename T > inline dtkVectorElement<T> dtkVectorElement<T>::operator - (qlonglong j) const
{
    dtkVectorElement o(*this); 
    o -= j; 
    return o;
}

// 
// dtkVectorElement.tpp ends here
