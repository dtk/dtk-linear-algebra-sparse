// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkSparseMatrixEngine.h"

#include <dtkCore>
#include <dtkDistributed>

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineDD interface
// ///////////////////////////////////////////////////////////////////

template < typename T > class dtkDistributedSparseMatrixEngineDD : public dtkSparseMatrixEngine<T>
{
public:
     dtkDistributedSparseMatrixEngineDD(void);
     dtkDistributedSparseMatrixEngineDD(const dtkDistributedSparseMatrixEngineDD& o);
    ~dtkDistributedSparseMatrixEngineDD(void);

public:
    dtkDistributedSparseMatrixEngineDD *clone(void) const { return new dtkDistributedSparseMatrixEngineDD(*this); }

public:
    bool copy(const dtkSparseMatrixEngine<T> *other);

public:
    dtkDistributedGraphTopology *graph(void) const;
    void setGraph(dtkDistributedGraphTopology *graph);

public:
    void rlock(void);
    void wlock(void);
    void unlock(void);

public:
    bool readMatrixMarket(const QString& filename);

public:
    qlonglong     rowCount(void) const { return _N; }
    qlonglong     colCount(void) const { return _N; }
    qlonglong nonZeroCount(void) const { return m_graph ? m_graph->edgeCount() : 0LL; }

public:
    void clear(void);

public:
    void resize(qlonglong line_count);

public:
    void insert(qlonglong i, qlonglong j, const T& value);
    void remove(qlonglong i, qlonglong j);

public:
    bool assemble(void);

public:
    void fill(const T& value);
    void mult(const T& value) ;
    void add(dtkSparseMatrixEngine<T> *other);
    void sub(dtkSparseMatrixEngine<T> *other);

public:
    T at(qlonglong i, qlonglong j) const;

public:
    void    assign(qlonglong i, qlonglong j, const T& value);
    void addAssign(qlonglong i, qlonglong j, const T& value);
    void subAssign(qlonglong i, qlonglong j, const T& value);
    void mulAssign(qlonglong i, qlonglong j, const T& value);
    void divAssign(qlonglong i, qlonglong j, const T& value);

public:
    typedef dtkSparseMatrixLine<T> line_iterator;

    line_iterator  begin(void) const;
    line_iterator    end(void) const;

public:
    line_iterator lineAt(qlonglong line_id) const;

public:
    const T *data(void) const;
          T *data(void);

private:
    qlonglong pos(qlonglong i, qlonglong j) const;

private:
    mutable bool is_assembled;
    qlonglong _N;
    QHash<qlonglong, T> m_val_hash;

private:
    dtkDistributedGraphTopology *m_graph;

private:
    dtkDistributedArray<T> *m_values;
};

// ///////////////////////////////////////////////////////////////////

template < typename T > dtkSparseMatrixEngine<T> *dtkDistributedSparseMatrixEngineDDCreator(void)
{
    return new dtkDistributedSparseMatrixEngineDD<T>();
}

// ///////////////////////////////////////////////////////////////////
// dtkDistributedSparseMatrixEngineDDLineGlobal
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkDistributedSparseMatrixEngineDDLineGlobal : public dtkSparseMatrixLineBase < T, dtkDistributedSparseMatrixEngineDDLineGlobal<T> >
{
    dtkDistributedGraphTopology *m_graph;
    dtkDistributedArray<T>      *m_array;
    qlonglong m_id;
    qlonglong m_line_start_pos;
    qlonglong m_line_size;

public:
    dtkDistributedSparseMatrixEngineDDLineGlobal(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph, qlonglong id);
    dtkDistributedSparseMatrixEngineDDLineGlobal(dtkDistributedArray <T> *values, dtkDistributedGraphTopology *graph);

public:
    dtkDistributedSparseMatrixEngineDDLineGlobal(const dtkDistributedSparseMatrixEngineDDLineGlobal& o);

public:
    ~dtkDistributedSparseMatrixEngineDDLineGlobal(void);

public:
    dtkDistributedSparseMatrixEngineDDLineGlobal& operator = (const dtkDistributedSparseMatrixEngineDDLineGlobal& o);

public:
    qlonglong id(void) const;

public:
    qlonglong  size(void) const;
    qlonglong width(void) const;

public:
    T elementValue(qlonglong id) const;
    qlonglong elementId(qlonglong id) const;

public:
    bool operator == (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const;
    bool operator != (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const;
    bool operator <  (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const;
    bool operator <= (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const;
    bool operator >  (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const;
    bool operator >= (const dtkDistributedSparseMatrixEngineDDLineGlobal& o) const;

public:
    dtkDistributedSparseMatrixEngineDDLineGlobal& operator ++ (void);
    dtkDistributedSparseMatrixEngineDDLineGlobal  operator ++ (int);
    dtkDistributedSparseMatrixEngineDDLineGlobal& operator -- (void);
    dtkDistributedSparseMatrixEngineDDLineGlobal  operator -- (int);
    dtkDistributedSparseMatrixEngineDDLineGlobal& operator += (qlonglong j);
    dtkDistributedSparseMatrixEngineDDLineGlobal& operator -= (qlonglong j);
    dtkDistributedSparseMatrixEngineDDLineGlobal  operator +  (qlonglong j) const;
    dtkDistributedSparseMatrixEngineDDLineGlobal  operator -  (qlonglong j) const;

protected:
    void advance(void);
    void advance(qlonglong j);
    void rewind(void);
    void rewind(qlonglong j);
};

// ///////////////////////////////////////////////////////////////////

#include "dtkDistributedSparseMatrixEngineDD.tpp"

//
// dtkDistributedSparseMatrixEngineDD.h ends here
