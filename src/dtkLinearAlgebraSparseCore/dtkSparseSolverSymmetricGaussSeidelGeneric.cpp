// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseSolverSymmetricGaussSeidelGeneric.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkSparseSolverSymmetricGaussSeidelGenericPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkVector<double> *rhs_vector;
    dtkVector<double> *sol_vector;

public:
    QHash<QString, int> params_int;
    QHash<QString, double> params_real;

    int number_of_iterations;
    double residual_reduction_order;

public:
    bool initialized;
    qreal variation;
    dtkVector<double> *tmp_vector;

public:
    dtkArray<double> stats;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkSparseSolverSymmetricGaussSeidelGeneric::dtkSparseSolverSymmetricGaussSeidelGeneric(void) : dtkSparseSolver<double>(), d(new dtkSparseSolverSymmetricGaussSeidelGenericPrivate)
{
    d->matrix = NULL;
    d->rhs_vector = NULL;
    d->sol_vector = NULL;

    d->initialized = false;
    d->tmp_vector = NULL;

    d->params_int.insert("number_of_iterations", 50);
    d->params_real.insert("residual_reduction_order", 1.e-6);

    d->residual_reduction_order = 1.e-6;
    d->number_of_iterations = 50;
}

dtkSparseSolverSymmetricGaussSeidelGeneric::~dtkSparseSolverSymmetricGaussSeidelGeneric(void)
{

}

void dtkSparseSolverSymmetricGaussSeidelGeneric::setMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;
}

void dtkSparseSolverSymmetricGaussSeidelGeneric::setRHSVector(dtkVector<double> *rhs_vector)
{
    d->rhs_vector = rhs_vector;
}

void dtkSparseSolverSymmetricGaussSeidelGeneric::setSolutionVector(dtkVector<double> *sol_vector)
{
    d->sol_vector = sol_vector;
}

void dtkSparseSolverSymmetricGaussSeidelGeneric::setOptionalParameters(const QHash<QString, int>& parameters)
{
     QHash<QString, int>::const_iterator it = parameters.find("number_of_iterations");

     if(it != parameters.end())
         d->params_int.insert("number_of_iterations", it.value());

     d->number_of_iterations = d->params_int.value("number_of_iterations");
}

void dtkSparseSolverSymmetricGaussSeidelGeneric::setOptionalParameters(const QHash<QString, double>& parameters)
{
     QHash<QString, double>::const_iterator it = parameters.find("residual_reduction_order");

     if(it != parameters.end())
         d->params_real.insert("residual_reduction_order", it.value());

     d->residual_reduction_order = d->params_real.value("residual_reduction_order");
}

void dtkSparseSolverSymmetricGaussSeidelGeneric::run(void)
{
    // initialization of the parameters stop_criterion = false;
    bool stop_criterion = false;
    qlonglong iteration_count = 0;
    double variation_ini;

    qlonglong row_count = d->rhs_vector->size();
    double neighbour;
    double diag;

    const dtkSparseMatrix<double>& mat = *(d->matrix);
    const dtkVector<double>& rhs = *(d->rhs_vector);

    if (!d->initialized) {
        if (!d->sol_vector) {
            d->sol_vector = new dtkVector<double>(rhs);
        }
        d->tmp_vector = new dtkVector<double>(rhs);
        d->initialized = true;
    }

    dtkVector<double>& sol = *(d->sol_vector);
    dtkVector<double>& previous_sol = *(d->tmp_vector);

    sol.fill(0.0);

    d->rhs_vector->rlock();
    d->matrix->rlock();

    while(!stop_criterion) {

        dtkSparseMatrixLine<double> line_it = mat.begin();
        dtkSparseMatrixLine<double> line_end = mat.end();

        sol.rlock();
        d->variation = 0;

        // first phase, increasing order
        for (; line_it != line_end; ++line_it) {

            dtkSparseMatrixLineElement<double> elt_it  = line_it.begin();
            dtkSparseMatrixLineElement<double> elt_end = line_it.end();

            neighbour = 0;
            diag = 0;

            qlonglong i = line_it.id();

            for(;elt_it != elt_end; ++elt_it) {

                qlonglong j = elt_it.id();

                if ( j != i ) {
                    neighbour += (*elt_it) * sol[j];
                } else {
                    diag = (*elt_it);
                }
            }

            sol[i] = (rhs.at(i) - neighbour) / diag ;
        }

        // second phase, decreasing order
        line_it = mat.end() - 1;
        line_end = mat.begin();
        sol.clearCache();

        
        for (; line_it >= line_end; --line_it) {

            dtkSparseMatrixLineElement<double> elt_it  = line_it.begin();
            dtkSparseMatrixLineElement<double> elt_end = line_it.end();

            neighbour = 0;
            diag = 0;

            qlonglong i = line_it.id();

            for(;elt_it != elt_end; ++elt_it) {

                qlonglong j = elt_it.id();
                if ( j != i ) {
                    neighbour += (*elt_it) * sol[j];
                } else {
                    diag = (*elt_it);
                }
            }

            sol[i] = (rhs.at(i) - neighbour) / diag ;

        }

        sol.unlock();

        // stopping criterion

        previous_sol -= sol;
        d->variation = previous_sol.asum();

        if (iteration_count == 0)
            variation_ini = d->variation;

        if (iteration_count == d->number_of_iterations || d->variation/variation_ini < d->residual_reduction_order) {
            qDebug() << "stop criterion reached, iteration:" <<  iteration_count;
            stop_criterion = true;
        }

        ++iteration_count;

        sol.clearCache();

    }
    d->matrix->unlock();
    d->rhs_vector->unlock();

}

dtkVector<double> *dtkSparseSolverSymmetricGaussSeidelGeneric::solutionVector(void) const
{
    return d->sol_vector;
}

const dtkArray<double>& dtkSparseSolverSymmetricGaussSeidelGeneric::stats(void) const
{
    return d->stats;
}

//
// dtkSparseSolverSymmetricGaussSeidelGeneric.cpp ends here
