// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include "dtkSparseMatrixLineElement.h"

#include <QtCore>

template <typename T> class dtkSparseMatrixAbstractLine;

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixLine
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkSparseMatrixLine
{
    dtkSparseMatrixAbstractLine<T> *l;

public:
    typedef dtkSparseMatrixLineElement<T> element_iterator;
    typedef qlonglong difference_type;
    typedef T value_type;

public:
    explicit dtkSparseMatrixLine(dtkSparseMatrixAbstractLine<T> *line = 0);

public:
    dtkSparseMatrixLine(const dtkSparseMatrixLine& o);
    dtkSparseMatrixLine(dtkSparseMatrixLine&& o);

public:
    ~dtkSparseMatrixLine(void);

public:
    dtkSparseMatrixLine& operator = (const dtkSparseMatrixLine& o);
    dtkSparseMatrixLine& operator = (dtkSparseMatrixLine&& o);

public:
    qlonglong id(void) const;

public:
    qlonglong  size(void) const;
    qlonglong width(void) const;

public:
    element_iterator begin(void) const;
    element_iterator   end(void) const;

public:
    bool operator == (const dtkSparseMatrixLine& o) const;
    bool operator != (const dtkSparseMatrixLine& o) const;
    bool operator <  (const dtkSparseMatrixLine& o) const;
    bool operator <= (const dtkSparseMatrixLine& o) const;
    bool operator >  (const dtkSparseMatrixLine& o) const;
    bool operator >= (const dtkSparseMatrixLine& o) const;

public:
    dtkSparseMatrixLine& operator ++ (void);
    dtkSparseMatrixLine  operator ++ (int);
    dtkSparseMatrixLine& operator -- (void);
    dtkSparseMatrixLine  operator -- (int);
    dtkSparseMatrixLine& operator += (qlonglong j);
    dtkSparseMatrixLine& operator -= (qlonglong j);
    dtkSparseMatrixLine  operator +  (qlonglong j) const;
    dtkSparseMatrixLine  operator -  (qlonglong j) const;
};

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixAbstractLine
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkSparseMatrixAbstractLine
{
public:
    virtual ~dtkSparseMatrixAbstractLine(void) {}

public:
    virtual dtkSparseMatrixAbstractLine *clone(void) const = 0;

public:
    virtual void copy(const dtkSparseMatrixAbstractLine& o) = 0;

public:
    virtual qlonglong id(void) const = 0;

public:
    virtual qlonglong  size(void) const = 0;
    virtual qlonglong width(void) const = 0;

public:
    virtual T elementValue(qlonglong id) const = 0;
    virtual qlonglong elementId(qlonglong id) const = 0;

public:
    virtual bool       equal(const dtkSparseMatrixAbstractLine& o) const = 0;
    virtual bool   lowerThan(const dtkSparseMatrixAbstractLine& o) const = 0;
    virtual bool greaterThan(const dtkSparseMatrixAbstractLine& o) const = 0;

public:
    virtual void advance(void) = 0;
    virtual void  rewind(void) = 0;
    virtual void advance(qlonglong j) = 0;
    virtual void  rewind(qlonglong j) = 0;
};

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixLineBase CRTP 
// ///////////////////////////////////////////////////////////////////

template <typename T, typename Line> class dtkSparseMatrixLineBase : public dtkSparseMatrixAbstractLine<T>
{
public:
    virtual ~dtkSparseMatrixLineBase(void) {}

public:
    dtkSparseMatrixLineBase *clone(void) const;

public:
    void copy(const dtkSparseMatrixAbstractLine<T>& o);

public:
     qlonglong id(void) const;

public:
    qlonglong width(void) const;

public:
    bool       equal(const dtkSparseMatrixAbstractLine<T>& o) const;
    bool   lowerThan(const dtkSparseMatrixAbstractLine<T>& o) const;
    bool greaterThan(const dtkSparseMatrixAbstractLine<T>& o) const;

public:
     void advance(void);
     void  rewind(void);
     void advance(qlonglong j);
     void  rewind(qlonglong j);
};

// ///////////////////////////////////////////////////////////////////

#include "dtkSparseMatrixLine.tpp"

// 
// dtkSparseMatrixLine.h ends here
