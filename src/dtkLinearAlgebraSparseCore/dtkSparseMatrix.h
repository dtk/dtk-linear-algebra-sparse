// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <dtkConfig.h>

#include <dtkCore/dtkCorePlugin>

#include "dtkSparseMatrixLine.h"
#include "dtkSparseMatrixProxy.h"

// template <typename T> class dtkSparseMatrixAbstractData;

template <typename T> class dtkSparseMatrixEngine;

// forward declaration
class dtkDistributedGraphTopology;

// /////////////////////////////////////////////////////////////////
// dtkSparseMatrix
// /////////////////////////////////////////////////////////////////

template < typename T > class dtkSparseMatrix
{
public:
    typedef qlonglong               difference_type;
    typedef qlonglong               size_type;
    typedef T                       value_type;
    typedef dtkSparseMatrixProxy<T> reference;
    typedef dtkSparseMatrixLine<T>  line_iterator;

public:
    explicit dtkSparseMatrix(void);
    explicit dtkSparseMatrix(dtkSparseMatrixEngine<T> *engine);
             dtkSparseMatrix(const dtkSparseMatrix& other);

public:
    virtual ~dtkSparseMatrix(void);

public:
    dtkSparseMatrix& operator = (const dtkSparseMatrix<T>& other);
    dtkSparseMatrix& operator += (const dtkSparseMatrix<T>& other);
    dtkSparseMatrix& operator -= (const dtkSparseMatrix<T>& other);
    dtkSparseMatrix& operator *= (const T& value);

public:
    QString dataType(void) const;

public:
    bool convert(const QString& data_type);

public:
    dtkDistributedGraphTopology *graph(void);
    void setGraph(dtkDistributedGraphTopology *graph);

public:
    bool readMatrixMarket(const QString& filename);
    bool writeMatrixMarket(const QString& filename);

public:
    void assemble(void);

public:
    void rlock(void);
    void wlock(void);
    void unlock(void);

public:
    qlonglong     rowCount(void) const;
    qlonglong     colCount(void) const;
    qlonglong nonZeroCount(void) const;

public:
    void clear(void);
    void reset(void);

    void resize(qlonglong line_count);

    void insert(qlonglong i, qlonglong j, const T& value);
    void remove(qlonglong i, qlonglong j);

    void setAt(qlonglong i, qlonglong j, const T& value);
    void addAssign(qlonglong i, qlonglong j, const T& value);

public:
    void fill(const T& value);

public:
    T at(qlonglong i, qlonglong j) const;

public:
    const reference operator () (qlonglong i, qlonglong j) const;
          reference operator () (qlonglong i, qlonglong j);

public:
    line_iterator begin(void) const;
    line_iterator   end(void) const;

public:
    line_iterator lineAt(qlonglong line_id) const;

public:
    const T *data(void) const;
          T *data(void);

    const T *constData(void) const;

protected:
    dtkSparseMatrixEngine<T> *d;
};

// for numComposer
Q_DECLARE_METATYPE(dtkSparseMatrix<double>)
Q_DECLARE_METATYPE(dtkSparseMatrix<double>*)
Q_DECLARE_METATYPE(dtkSparseMatrix<double>**)

// /////////////////////////////////////////////////////////////////

#include "dtkSparseMatrix.tpp"

//
// dtkSparseMatrix.h ends here
