// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVectorAbstractData.h"

// /////////////////////////////////////////////////////////////////
// dtkVector implementation
// /////////////////////////////////////////////////////////////////

template < typename T > inline dtkVector<T>::dtkVector(void) : d(0)
{

}

template < typename T > inline dtkVector<T>::dtkVector(dtkVectorAbstractData<T> *data) : d(data)
{

}

template < typename T > inline dtkVector<T>::dtkVector(const dtkVector<T>& other) : d(other.d->clone())
{

}

template < typename T > inline dtkVector<T>::~dtkVector(void)
{
    if (d)
        delete d;
    d = 0;
}

template < typename T > inline QString dtkVector<T>::dataType(void) const
{
    return d->dataType();
}

template < typename T > inline dtkVector<T>& dtkVector<T>::operator = (const dtkVector<T>& other)
{
    d->copy(other.d);
    return *this;
}

template < typename T > inline dtkVector<T>& dtkVector<T>::operator += (const dtkVector<T>& other)
{
    d->add(other.d);
    return *this;
}

template < typename T > inline dtkVector<T>& dtkVector<T>::operator -= (const dtkVector<T>& other)
{
    d->sub(other.d);
    return *this;
}

template < typename T > inline T dtkVector<T>::asum(void)
{
    return d->asum();
}

template < typename T > inline bool dtkVector<T>::empty(void) const
{
    return d->empty();
}

template < typename T > inline qlonglong dtkVector<T>::size(void) const
{
    return d->size();
}

template < typename T > inline void dtkVector<T>::resize(const qlonglong& size)
{
    d->resize(size);
}

template < typename T > inline void dtkVector<T>::fill(const T& value)
{
    d->fill(value);
}

template < typename T > inline void dtkVector<T>::setAt(const qlonglong& index, const T& value)
{
    d->setAt(index, value);
}

template < typename T > inline T dtkVector<T>::at(const qlonglong& index) const
{
    return d->at(index);
}

template < typename T > inline T dtkVector<T>::first(void) const
{
    return d->at(0);
}

template < typename T > inline T dtkVector<T>::last(void) const
{
    return d->at(d->size() - 1);
}

template < typename T > inline const typename dtkVector<T>::reference dtkVector<T>::operator [] (qlonglong i) const
{
    return reference(d, i);
}

template < typename T > inline typename dtkVector<T>::reference dtkVector<T>::operator [] (qlonglong i)
{
    return reference(d, i);
}

template < typename T > inline void dtkVector<T>::clearCache(void)
{
    d->clearCache();
}

template < typename T > inline void dtkVector<T>::rlock(void)
{
    d->rlock();
}

template < typename T > inline void dtkVector<T>::wlock(void)
{
    d->wlock();
}

template < typename T > inline void dtkVector<T>::unlock(void)
{
    d->unlock();
}

template < typename T > inline typename dtkVector<T>::iterator dtkVector<T>::begin(void) const
{
    return d->begin();
}

template < typename T > inline typename dtkVector<T>::iterator dtkVector<T>::end(void) const
{
    return d->end();
}

template < typename T > inline const T *dtkVector<T>::data(void) const
{
    return d->constData();
}

template < typename T > inline T *dtkVector<T>::data(void)
{
    return d->data();
}

template < typename T > inline const T *dtkVector<T>::constData(void) const
{
    return d->constData();
}

template < typename T > inline void dtkVector<T>::setMapper(dtkDistributedMapper *mapper)
{
    return d->setMapper(mapper);
}

template < typename T > inline bool dtkVector<T>::readMatrixMarket(const QString &filename)
{
    return d->readMatrixMarket(filename);
}

template < typename T > inline bool dtkVector<T>::writeMatrixMarket(const QString &filename) const
{
    return d->writeMatrixMarket(filename);
}

//
// dtkVector.tpp ends here
