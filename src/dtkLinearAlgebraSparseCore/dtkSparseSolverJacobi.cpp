// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseSolverJacobi.h"
#include "dtkVector.h"
#include "dtkVectorAbstractData.h"
#include "dtkSparseMatrix.h"
#include "dtkSparseMatrixLine.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkSparseSolverJacobiPrivate
{
public:
    dtkSparseMatrix<double> *matrix;
    dtkVector<double> *rhs_vector;
    dtkVector<double> *sol_vector;

public:
    QHash<QString, int> params_int;
    QHash<QString, double> params_real;

    int number_of_iterations;
    double residual_reduction_order;

public:
    bool initialized;
    double variation;
    dtkVector<double> *tmp_vector;

public:
    dtkArray<double> stats;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkSparseSolverJacobi::dtkSparseSolverJacobi(void) : dtkSparseSolver<double>(), d(new dtkSparseSolverJacobiPrivate)
{
    d->matrix = NULL;
    d->rhs_vector = NULL;
    d->sol_vector = NULL;

    d->initialized = false;
    d->tmp_vector = NULL;

    d->residual_reduction_order = 1.e-6;
    d->number_of_iterations = 50;
}

dtkSparseSolverJacobi::~dtkSparseSolverJacobi(void)
{
    // if (d->tmp_vector)
    //     delete d->tmp_vector;
}

void dtkSparseSolverJacobi::setMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;
}

void dtkSparseSolverJacobi::setRHSVector(dtkVector<double> *rhs_vector)
{
    d->rhs_vector = rhs_vector;
}

void dtkSparseSolverJacobi::setSolutionVector(dtkVector<double> *sol_vector)
{
    d->sol_vector = sol_vector;
}

void dtkSparseSolverJacobi::setOptionalParameters(const QHash<QString, int>& parameters)
{
     QHash<QString, int>::const_iterator it = parameters.find("number_of_iterations");

     if(it != parameters.end())
         d->params_int.insert("number_of_iterations", it.value());

     d->number_of_iterations = d->params_int.value("number_of_iterations");
}

void dtkSparseSolverJacobi::setOptionalParameters(const QHash<QString, double>& parameters)
{
     QHash<QString, double>::const_iterator it = parameters.find("residual_reduction_order");

     if(it != parameters.end())
         d->params_real.insert("residual_reduction_order", it.value());

     d->residual_reduction_order = d->params_real.value("residual_reduction_order");
}

void dtkSparseSolverJacobi::run(void)
{
    bool stop_criterion = false;
    qlonglong iteration_count = 0;
    double variation_ini;

    qlonglong row_count = d->rhs_vector->size();
    double neighbour;
    double diag;

    const dtkSparseMatrix<double>& mat = *(d->matrix);
    const dtkVector<double>& rhs = *(d->rhs_vector);

    if (!d->initialized) {
        if (!d->sol_vector) {
            d->sol_vector = new dtkVector<double>(rhs);
        }
        d->tmp_vector = new dtkVector<double>(rhs);
        d->initialized = true;
    }

    dtkVector<double>& sol = *(d->sol_vector);
    dtkVector<double>& tmp = *(d->tmp_vector);

    sol.fill(0.0);

    dtkSparseMatrixLine<double> line_it;
    dtkSparseMatrixLine<double> line_end = mat.end();

    d->rhs_vector->rlock();

    while(!stop_criterion) {
        line_it = mat.begin();

        // need the previous solution to update the new one
        tmp = sol;

        sol.wlock();
        tmp.rlock();
        d->variation  = 0;

        for (; line_it != line_end; ++line_it) {

            dtkSparseMatrixLineElement<double> elt_it  = line_it.begin();
            dtkSparseMatrixLineElement<double> elt_end = line_it.end();

            neighbour = 0;
            diag = 0;

            qlonglong i = line_it.id();

            for(;elt_it != elt_end; ++elt_it) {

                qlonglong j = elt_it.id();

                if (j != i) {
                    neighbour += (*elt_it) * tmp[j];
                } else {
                    diag = (*elt_it);
                }
            }

            sol[i] = (rhs.at(i) - neighbour) / diag ;

        }
        tmp.unlock();
        sol.unlock();

        tmp -= sol;
        d->variation = tmp.asum();

        if(iteration_count == 0)
            variation_ini = d->variation;

        if(iteration_count == d->number_of_iterations || d->variation/variation_ini < d->residual_reduction_order) {
            qDebug() << "stop criterion reached, iteration:" <<  iteration_count;
            stop_criterion = true;
        }

        ++iteration_count;

        sol.clearCache();
        tmp.clearCache();
    }
    d->rhs_vector->unlock();

}

dtkVector<double> *dtkSparseSolverJacobi::solutionVector(void) const
{
    return d->sol_vector;
}

const dtkArray<double>& dtkSparseSolverJacobi::stats(void) const
{
    return d->stats;
}

//
// dtkSparseSolverJacobi.cpp ends here
