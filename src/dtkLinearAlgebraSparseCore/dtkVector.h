// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore/dtkCorePlugin>

#include "dtkVectorProxy.h"
#include "dtkVectorElement.h"

template <typename T> class dtkVectorAbstractData;
class dtkDistributedMapper;

// /////////////////////////////////////////////////////////////////
// dtkVector
// /////////////////////////////////////////////////////////////////

template < typename T > class dtkVector
{
public:
    typedef qlonglong           difference_type;
    typedef qlonglong           size_type;
    typedef T                   value_type;
    typedef dtkVectorProxy<T>   reference;
    typedef dtkVectorElement<T> iterator;

public:
    explicit dtkVector(void);
    explicit dtkVector(dtkVectorAbstractData<T> *data);
             dtkVector(const dtkVector<T>& other);

public:
    virtual ~dtkVector(void);

public:
    QString dataType(void) const;

public:
    dtkVector& operator = (const dtkVector<T>& other);
    dtkVector& operator += (const dtkVector<T>& other);
    dtkVector& operator -= (const dtkVector<T>& other);

public:
    T asum(void);

public:
    bool empty(void) const;
    qlonglong size(void) const;

public:
    void resize(const qlonglong& size);

public:
    void fill(const T& value);

public:
    void setMapper(dtkDistributedMapper *mapper);

public:
    bool  readMatrixMarket(const QString& filename);
    bool writeMatrixMarket(const QString& filename) const;

public:
    void clearCache(void);
    void rlock();
    void wlock();
    void unlock();

public:
    void setAt(const qlonglong& index, const T& value);

    T at(const qlonglong& index) const;

    T first(void) const;
    T  last(void) const;

public:
    const reference operator [] (qlonglong i) const;
          reference operator [] (qlonglong i);

public:
    iterator begin(void) const;
    iterator   end(void) const;

public:
    const T *data(void) const;
          T *data(void);

    const T *constData(void) const;

protected:
    dtkVectorAbstractData<T> *d;
};

// for numComposer
Q_DECLARE_METATYPE(dtkVector<double>)
Q_DECLARE_METATYPE(dtkVector<double>*)
Q_DECLARE_METATYPE(dtkVector<double>**)
// /////////////////////////////////////////////////////////////////

#include "dtkVector.tpp"

//
// dtkVector.h ends here
