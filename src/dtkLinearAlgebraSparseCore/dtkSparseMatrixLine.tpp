// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <algorithm>

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixLineBase CRTP implementation
// ///////////////////////////////////////////////////////////////////

template <typename T, typename Line> inline dtkSparseMatrixLineBase<T, Line> *dtkSparseMatrixLineBase<T, Line>::clone(void) const
{
    return new Line(static_cast<const Line&>(*this));
}

template <typename T, typename Line> inline void dtkSparseMatrixLineBase<T, Line>::copy(const dtkSparseMatrixAbstractLine<T>& o)
{
    static_cast<Line&>(*this) = static_cast<const Line&>(o);
}

template <typename T, typename Line> inline qlonglong dtkSparseMatrixLineBase<T, Line>::id(void) const
{
    return (static_cast<const Line&>(*this)).id();
}

template < typename T, typename Line> inline qlonglong dtkSparseMatrixLineBase<T, Line>::width(void) const
{
    qlonglong size = this->size();

    if (size > 1) {
        qlonglong id = 1;
        qlonglong max = this->elementId(0);
        qlonglong min = max;
        for (; id <  size; ++id)  {
            max = std::max(max, this->elementId(id));
            min = std::min(min, this->elementId(id));
        }
        return max - min;
    }

    return 0LL;
}

template <typename T, typename Line> inline bool dtkSparseMatrixLineBase<T, Line>::equal(const dtkSparseMatrixAbstractLine<T>& o) const
{
    return (static_cast<const Line&>(*this) == static_cast<const Line&>(o));
}

template <typename T, typename Line> inline bool dtkSparseMatrixLineBase<T, Line>::lowerThan(const dtkSparseMatrixAbstractLine<T>& o) const
{
    return (static_cast<const Line&>(*this) < static_cast<const Line&>(o));
}

template <typename T, typename Line> inline bool dtkSparseMatrixLineBase<T, Line>::greaterThan(const dtkSparseMatrixAbstractLine<T>& o) const
{
    return (static_cast<const Line&>(*this) > static_cast<const Line&>(o));
}

template <typename T, typename Line> inline void dtkSparseMatrixLineBase<T, Line>::advance(void)
{
    ++(static_cast<Line&>(*this));
}

template <typename T, typename Line> inline void dtkSparseMatrixLineBase<T, Line>::advance(qlonglong j)
{
    static_cast<Line&>(*this) += j;
}

template <typename T, typename Line> inline void dtkSparseMatrixLineBase<T, Line>::rewind(void)
{
    --(static_cast<Line&>(*this));
}

template <typename T, typename Line> inline void dtkSparseMatrixLineBase<T, Line>::rewind(qlonglong j)
{
    static_cast<Line&>(*this) -= j;
}

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixLine implementation
// ///////////////////////////////////////////////////////////////////

template < typename T > inline dtkSparseMatrixLine<T>::dtkSparseMatrixLine(dtkSparseMatrixAbstractLine<T> *line) : l(line)
{
}

template < typename T > inline dtkSparseMatrixLine<T>::dtkSparseMatrixLine(const dtkSparseMatrixLine& o) : l(o.l->clone()) 
{
}

template < typename T > inline dtkSparseMatrixLine<T>::dtkSparseMatrixLine(dtkSparseMatrixLine&& o) : l(0)
{
    std::swap(l, o.l);
}

template < typename T > inline dtkSparseMatrixLine<T>::~dtkSparseMatrixLine(void)
{
}

template < typename T > inline dtkSparseMatrixLine<T>& dtkSparseMatrixLine<T>::operator = (const dtkSparseMatrixLine& o)
{
    l->copy(*(o.l)); 
    return *this;
}

template < typename T > inline dtkSparseMatrixLine<T>& dtkSparseMatrixLine<T>::operator = (dtkSparseMatrixLine&& o)
{
    std::swap(l, o.l); 
    return *this;
}

template < typename T > inline qlonglong dtkSparseMatrixLine<T>::id(void) const
{
    return l->id();
}

template < typename T > inline qlonglong dtkSparseMatrixLine<T>::size(void) const
{
    return l->size();
}

template < typename T > inline qlonglong dtkSparseMatrixLine<T>::width(void) const
{
    return l->width();
}

template < typename T > inline typename dtkSparseMatrixLine<T>::element_iterator dtkSparseMatrixLine<T>::begin(void) const
{
    return element_iterator(l, 0);
}

template < typename T > inline typename dtkSparseMatrixLine<T>::element_iterator dtkSparseMatrixLine<T>::end(void) const
{
    return element_iterator(l, l->size());
}

template < typename T > inline bool dtkSparseMatrixLine<T>::operator == (const dtkSparseMatrixLine& o) const
{
    return l->equal(*(o.l));
}

template < typename T > inline bool dtkSparseMatrixLine<T>::operator != (const dtkSparseMatrixLine& o) const
{
    return !l->equal(*(o.l));
}

template < typename T > inline bool dtkSparseMatrixLine<T>::operator < (const dtkSparseMatrixLine& o) const
{
    return l->lowerThan(*(o.l));
}

template < typename T > inline bool dtkSparseMatrixLine<T>::operator <= (const dtkSparseMatrixLine& o) const
{
    return (l->lowerThan(*(o.l)) || l->equal(*(o.l)));
}

template < typename T > inline bool dtkSparseMatrixLine<T>::operator > (const dtkSparseMatrixLine& o) const
{
    return l->greaterThan(*(o.l));
}

template < typename T > inline bool dtkSparseMatrixLine<T>::operator >= (const dtkSparseMatrixLine& o) const
{
    return (l->greaterThan(*(o.l)) || l->equal(*(o.l)));
}

template < typename T > inline dtkSparseMatrixLine<T>& dtkSparseMatrixLine<T>::operator ++ (void)
{
    l->advance(); 
    return *this;
}

template < typename T > inline dtkSparseMatrixLine<T> dtkSparseMatrixLine<T>::operator ++ (int)
{
    dtkSparseMatrixLine o(*this); 
    l->advance();
    return o;
}

template < typename T > inline dtkSparseMatrixLine<T>& dtkSparseMatrixLine<T>::operator -- (void)
{
    l->rewind(); 
    return *this;
}

template < typename T > inline dtkSparseMatrixLine<T> dtkSparseMatrixLine<T>::operator -- (int)
{
    dtkSparseMatrixLine o(*this); 
    l->rewind(); 
    return o;
}

template < typename T > inline dtkSparseMatrixLine<T>& dtkSparseMatrixLine<T>::operator += (qlonglong j)
{
    l->advance(j); 
    return *this;
}

template < typename T > inline dtkSparseMatrixLine<T>& dtkSparseMatrixLine<T>::operator -= (qlonglong j)
{
    l->rewind(j); 
    return *this;
}

template < typename T > inline dtkSparseMatrixLine<T> dtkSparseMatrixLine<T>::operator + (qlonglong j) const
{
    dtkSparseMatrixLine o(*this); 
    o += j; 
    return o;
}

template < typename T > inline dtkSparseMatrixLine<T> dtkSparseMatrixLine<T>::operator - (qlonglong j) const
{
    dtkSparseMatrixLine o(*this); 
    o -= j; 
    return o;
}

// 
// dtkSparseMatrixLine.tpp ends here
