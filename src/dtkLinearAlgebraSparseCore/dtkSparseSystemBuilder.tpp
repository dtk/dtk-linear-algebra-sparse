// dtkSparseSystemBuilder.tpp ---
//
// Version:
//
//

// Commentary:
//
//

// Change log:
//
//

#include <dtkConfig.h>


template<typename T>  dtkSparseSystemBuilder<T>::dtkSparseSystemBuilder()
{
    solver_impl ="";
    matrix_impl="";
    vector_impl="";
    auto_load = false;
}


template<typename T>
dtkSparseSystemBuilder<T> * dtkSparseSystemBuilder<T>::instance()
{
	if(!_instance)
	{
		dtkSparseSystemBuilder<T>::_instance = new dtkSparseSystemBuilder<T>();
	}

    return _instance;
}

template<typename T>
void dtkSparseSystemBuilder<T>::init(const QString& solver_type)
{
    solver_impl = solver_type;

#if defined(DTK_BUILD_DISTRIBUTED)
    dtkDistributed::communicator::initialize();
#endif

    dtkLinearAlgebraSparse::pluginManager::setAutoLoading(auto_load);
    dtkLinearAlgebraSparse::pluginManager::initialize();

    //find solver name absolute path
    dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
    linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");

    QString realpath = linear_algebra_sparse_settings.value("plugins").toString();
    if (realpath.isEmpty()) {
        realpath = QDir(DTK_INSTALL_PREFIX).filePath("plugins/dtkLinearAlgebraSparse");
        dtkDebug() << "no plugin path configured, use default:" << realpath ;
    }

    QDir dir(realpath);
    QStringList filter("*" % solver_type % "*");
    QFileInfoList solver_name = dir.entryInfoList(filter, QDir::Files);
    if(solver_name.size() != 1) {
        dtkFatal() << "ERROR can't find solver " << solver_type;
        return;
    }

    //open a plugin loader to read metadata
    QPluginLoader *loader = new QPluginLoader(solver_name.first().absoluteFilePath());
    linear_algebra_sparse_settings.endGroup();

    auto matrix_array = loader->metaData().value("MetaData").toObject().value("matrix").toArray();
    auto vector_array = loader->metaData().value("MetaData").toObject().value("vector").toArray();
    delete loader;

    //by default, take the first implementation
    matrix_impl = matrix_array.first().toString();
    vector_impl = vector_array.first().toString();

    //if the application is distributed, look for distributed matrix and vector implementations
#if defined(DTK_BUILD_DISTRIBUTED)
    if(dtkDistributed::app() && dtkDistributed::app()->communicator()->size()>1) {
        //first set matrix
        bool mat_found=false;
        for(auto mat_impl : matrix_array)
        {
            if(mat_impl.toString().contains("Distributed")) {
                matrix_impl= mat_impl.toString();
                mat_found=true;
                break;
            }
        }

        if(!mat_found) {
            QJsonDocument doc;
            doc.setArray(matrix_array);
            dtkFatal() << "ERROR distributed matrix not found for solver :" << solver_impl << "matrix :" << QString(doc.toJson());
        }

        //then do the same thing for vector
        bool vec_found=false;
        for(auto vec_impl : vector_array)
        {
            if(vec_impl.toString().contains("Distributed")) {
                vector_impl= vec_impl.toString();
                vec_found=true;
                break;
            }
        }

        if(!vec_found)  {
            QJsonDocument doc;
            doc.setArray(vector_array);
            dtkFatal() << "ERROR distributed vector not found for solver :" << solver_impl << "vector :" << QString(doc.toJson());
        }
    }
#endif

    //if there is no auto_loading, then load the required solver
    if(!auto_load)
        dtkLinearAlgebraSparse::solver::pluginManager().loadFromName(solver_impl);
}

template<typename T> bool dtkSparseSystemBuilder<T>::autoLoading(void)
{
    return auto_load;
}

template < typename T > dtkSparseSolver<T> *dtkSparseSystemBuilder<T>::createSolver()
{
    dtkSparseSolver<T> *solver = dtkLinearAlgebraSparse::solver::pluginFactory<T>().create(solver_impl);
    if (!solver) {
        dtkFatal() << Q_FUNC_INFO << "Error, solver " << solver_impl << " does not exist or is not registered.";
    }

    return solver;
}

template < typename T > dtkSparseMatrix<T> *dtkSparseSystemBuilder<T>::createMatrix()
{
    dtkSparseMatrixEngine<T> *engine = dtkLinearAlgebraSparse::matrixEngine::pluginFactory<T>().create(matrix_impl);
    if (!engine) {
        dtkFatal() << Q_FUNC_INFO << "Error, matrix data " << matrix_impl << " does not exist or is not registered.";
        return NULL;
    }
    engine->setObjectName(matrix_impl);

    return new dtkSparseMatrix<T>(engine);  ;

}


template < typename T >
dtkVector<T> *dtkSparseSystemBuilder<T>::createVector()
{
    dtkVectorAbstractData<T> *data = dtkLinearAlgebraSparse::vectorData::pluginFactory<T>().create(vector_impl);
    if (!data) {
        qDebug() << Q_FUNC_INFO << "Error, vector data " << vector_impl << " does not exist or is not registered.";
        return NULL;
    }
    data->setObjectName(vector_impl);

    return new dtkVector<T>(data);
}


template<typename T>
void dtkSparseSystemBuilder<T>::setAutoLoading(bool auto_load)
{
    this->auto_load = auto_load;
}
