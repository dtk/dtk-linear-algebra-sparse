#pragma once

#include <dtkComposer>
#include <dtkSparseSolver>
#include <dtkSparseMatrix>
#include <dtkVector>

template <typename T> class dtkComposerNodeSparseSolverPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class  dtkComposerNodeSparseSolver : public dtkComposerNodeObject< dtkSparseSolver<T> >
{
public:
     dtkComposerNodeSparseSolver(void);
    ~dtkComposerNodeSparseSolver(void);

public:
    void run(void);

private:
    dtkComposerNodeSparseSolverPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeSparseSolverPrivate
{
public:
    dtkComposerTransmitterReceiver<dtkSparseMatrix<T> *> rcv_matrix;
    dtkComposerTransmitterReceiver<      dtkVector<T> *> rcv_rhs;
    dtkComposerTransmitterReceiver<      dtkVector<T> *> rcv_sol;
    dtkComposerTransmitterReceiver<           qlonglong> rcv_iterations;

public:
    dtkComposerTransmitterEmitter<      dtkVector<T> *> emt_sol;
};

template <typename T> inline dtkComposerNodeSparseSolver<T>::dtkComposerNodeSparseSolver(void) : dtkComposerNodeObject< dtkSparseSolver<T> >(), d(new dtkComposerNodeSparseSolverPrivate<T>())
{
    this->setFactory(dtkLinearAlgebraSparse::solver::pluginFactory<T>());

    this->appendReceiver(&d->rcv_matrix);
    this->appendReceiver(&d->rcv_rhs);
    this->appendReceiver(&d->rcv_sol);
    this->appendReceiver(&d->rcv_iterations);

    this->appendEmitter(&d->emt_sol);
}

template <typename T> inline dtkComposerNodeSparseSolver<T>::~dtkComposerNodeSparseSolver(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeSparseSolver<T>::run(void)
{
    if (!d->rcv_matrix.isEmpty() && !d->rcv_rhs.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No solver instantiated, abort:" << this->currentImplementation();
            d->emt_sol.clearData();
            return;
        }

        dtkSparseMatrix<T> *mat = d->rcv_matrix.constData();
        dtkVector<T> *rhs = d->rcv_rhs.constData();

        if (!mat) {
            dtkWarn() << "null matrix, skip solver";
            d->emt_sol.clearData();
            return;
        }
        if (!rhs) {
            dtkWarn() << "null vector, skip solver";
            d->emt_sol.clearData();
            return;
        }

        dtkVector<T> *sol = 0;
        if (!d->rcv_sol.isEmpty())
            sol = d->rcv_sol.data();

        dtkSparseSolver<T> *solver = this->object();

        if (!d->rcv_iterations.isEmpty()) {
            qlonglong iterations = d->rcv_iterations.data();
            QHash<QString, int> parameters_int;
            parameters_int.insert("number_of_iterations", iterations);
            solver->setOptionalParameters(parameters_int);
        }

        solver->setMatrix(mat);
        solver->setRHSVector(rhs);
        if (sol)
            solver->setSolutionVector(sol);

        solver->run();

        d->emt_sol.setData(solver->solutionVector());

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_sol.clearData();
        return;

    }
}

//
// dtkComposerNodeSparseSolver.h ends here


//
// dtkComposerNodeSparseSolver.h ends here
