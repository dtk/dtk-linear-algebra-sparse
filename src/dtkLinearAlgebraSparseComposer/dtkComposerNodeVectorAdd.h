// Copyright: INRIA

#pragma once

#include <dtkComposer>
#include <dtkVector>

template <typename T> class dtkComposerNodeVectorAddPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorAdd : public dtkComposerNodeLeaf
{
public:
     dtkComposerNodeVectorAdd(void);
    ~dtkComposerNodeVectorAdd(void);

public:
    void run(void);

private:
    dtkComposerNodeVectorAddPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorAddPrivate
{
public:
    dtkComposerTransmitterReceiver< dtkVector<T> *> rcv_vector;
    dtkComposerTransmitterReceiver< dtkVector<T> *> rcv_b;

public:
    dtkComposerTransmitterEmitter< dtkVector<T> * > emt_vector;

public:
    dtkVector<T> *vector;
};

template <typename T> inline dtkComposerNodeVectorAdd<T>::dtkComposerNodeVectorAdd(void) : d(new dtkComposerNodeVectorAddPrivate<T>())
{
    this->appendReceiver(&d->rcv_vector);
    this->appendReceiver(&d->rcv_b);

    this->appendEmitter(&d->emt_vector);

    d->vector = 0;
}

template <typename T> inline dtkComposerNodeVectorAdd<T>::~dtkComposerNodeVectorAdd(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeVectorAdd<T>::run(void)
{
    if (!d->rcv_vector.isEmpty() && !d->rcv_b.isEmpty()) {

        d->vector = d->rcv_vector.data();
        dtkVector<T> *b = d->rcv_b.constData();

        *(d->vector) += *b;
        d->emt_vector.setData(d->vector);

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_vector.clearData();
        return;

    }
}

//
// dtkComposerNodeVectorAdd.h ends here
