// Copyright: INRIA

#pragma once

#include <dtkComposer>
#include <dtkVector>

template <typename T> class dtkComposerNodeVectorASumPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorASum : public dtkComposerNodeLeaf
{
public:
     dtkComposerNodeVectorASum(void);
    ~dtkComposerNodeVectorASum(void);

public:
    void run(void);

private:
    dtkComposerNodeVectorASumPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorASumPrivate
{
public:
    dtkComposerTransmitterReceiver< dtkVector<T> *> rcv_vector;

public:
    dtkComposerTransmitterEmitter< T > emt_asum;

/* public: */
/*     dtkVector<T> *vector; */
};

template <typename T> inline dtkComposerNodeVectorASum<T>::dtkComposerNodeVectorASum(void) : d(new dtkComposerNodeVectorASumPrivate<T>())
{
    /* this->setFactory(dtkLinearAlgebraSparse::vectorData::pluginFactory<T>()); */

    this->appendReceiver(&d->rcv_vector);

    this->appendEmitter(&d->emt_asum);

    /* d->vector = 0; */
}

template <typename T> inline dtkComposerNodeVectorASum<T>::~dtkComposerNodeVectorASum(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeVectorASum<T>::run(void)
{
    if (!d->rcv_vector.isEmpty()) {

        dtkVector<T> *vector = d->rcv_vector.constData();

        T asum = vector->asum();
        d->emt_asum.setData(asum);

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_asum.clearData();
        return;

    }
}

//
// dtkComposerNodeVectorASum.h ends here
