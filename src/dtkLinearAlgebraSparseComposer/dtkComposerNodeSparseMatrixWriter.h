#pragma once

#include <dtkComposer>
#include <dtkSparseMatrix>

template <typename T> class dtkComposerNodeSparseMatrixWriterPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class  dtkComposerNodeSparseMatrixWriter : public dtkComposerNodeLeaf
{
public:
     dtkComposerNodeSparseMatrixWriter(void);
    ~dtkComposerNodeSparseMatrixWriter(void);

public:
    void run(void);

private:
    dtkComposerNodeSparseMatrixWriterPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeSparseMatrixWriterPrivate
{
public:
    dtkComposerTransmitterReceiver< dtkSparseMatrix<T> *> rcv_matrix;
    dtkComposerTransmitterReceiver<QString>               rcv_filename;

public:
};

template <typename T> inline dtkComposerNodeSparseMatrixWriter<T>::dtkComposerNodeSparseMatrixWriter(void) : dtkComposerNodeLeaf(), d(new dtkComposerNodeSparseMatrixWriterPrivate<T>())
{
    this->appendReceiver(&d->rcv_matrix);
    this->appendReceiver(&d->rcv_filename);
}

template <typename T> inline dtkComposerNodeSparseMatrixWriter<T>::~dtkComposerNodeSparseMatrixWriter(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeSparseMatrixWriter<T>::run(void)
{
    if (!d->rcv_filename.isEmpty() && !d->rcv_matrix.isEmpty()) {

        dtkSparseMatrix<T> *mat = d->rcv_matrix.constData();
        QString filename = d->rcv_filename.constData();

        if (mat) {
            mat->writeMatrixMarket(filename);

        } else {
            dtkError() << Q_FUNC_INFO << "Input sparse matrix is null. Nothing is done";
            return;
        }

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        return;
    }
}

//
// dtkComposerNodeSparseMatrixWriter.h ends here
