#pragma once

#include <dtkLinearAlgebraSparseComposerExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

class dtkComposerNodeFactory;

class DTKLINEARALGEBRASPARSECOMPOSEREXTENSION_EXPORT dtkLinearAlgebraSparseComposerExtension : public dtkComposerExtension
{
public:
     dtkLinearAlgebraSparseComposerExtension(void);
    ~dtkLinearAlgebraSparseComposerExtension(void);

public:
    void extend(dtkComposerNodeFactory *factory);
};

//
// dtkLinearAlgebraSparseComposerFactoryExtension.h ends here
