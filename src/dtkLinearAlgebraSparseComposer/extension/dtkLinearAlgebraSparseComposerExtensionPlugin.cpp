/* dtkLinearAlgebraSparseComposerExtension.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */
//
//


// Code:

#include "dtkLinearAlgebraSparseComposerExtensionPlugin.h"
#include "dtkLinearAlgebraSparseComposerExtension.h"

#include <dtkLinearAlgebraSparse>

#include <dtkComposer>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


void dtkLinearAlgebraSparseComposerExtensionPlugin::initialize(void)
{
    dtkComposer::extension::pluginFactory().record("dtkLinearAlgebraSparse", dtkLinearAlgebraSparseComposerCreator);
    dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkLinearAlgebraSparse");
    bool verbose = dtkComposer::extension::pluginManager().verboseLoading();
    dtkLinearAlgebraSparse::pluginManager::setVerboseLoading(verbose);
    extension->extend(&(dtkComposer::node::factory()));
    dtkLinearAlgebraSparse::pluginManager::initialize();
}

void dtkLinearAlgebraSparseComposerExtensionPlugin::uninitialize(void)
{

}


// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkComposerExtension)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension *dtkLinearAlgebraSparseComposerCreator(void)
{
    return new dtkLinearAlgebraSparseComposerExtension();
}
