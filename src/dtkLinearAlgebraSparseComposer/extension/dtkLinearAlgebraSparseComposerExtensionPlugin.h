/* dtkLinearAlgebraSparseComposerExtensionPlugin.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */

#pragma once

#include <dtkComposer/dtkComposerExtension.h>
#include <dtkLinearAlgebraSparseComposerExtensionExport.h>

#include <QtCore>
#include <QtDebug>

class DTKLINEARALGEBRASPARSECOMPOSEREXTENSION_EXPORT dtkLinearAlgebraSparseComposerExtensionPlugin : public dtkComposerExtensionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkComposerExtensionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkLinearAlgebraSparseComposerExtensionPlugin" FILE "dtkLinearAlgebraSparseComposerExtensionPlugin.json")

public:
     dtkLinearAlgebraSparseComposerExtensionPlugin(void) {}
    ~dtkLinearAlgebraSparseComposerExtensionPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

DTKLINEARALGEBRASPARSECOMPOSEREXTENSION_EXPORT dtkComposerExtension* dtkLinearAlgebraSparseComposerCreator(void);
