// Version: $Id$
//
//


// Code:

#include "dtkLinearAlgebraSparseComposerExtension.h"

#include "dtkComposerNodeSparseMatrix.h"
#include "dtkComposerNodeSparseMatrixWriter.h"
#include "dtkComposerNodeSparseSolver.h"
#include "dtkComposerNodeVector.h"
#include "dtkComposerNodeVectorASum.h"
#include "dtkComposerNodeVectorAdd.h"
#include "dtkComposerNodeVectorSub.h"
#include "dtkComposerNodeVectorWriter.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkLinearAlgebraSparseComposerExtension::dtkLinearAlgebraSparseComposerExtension(void) : dtkComposerExtension()
{

}

dtkLinearAlgebraSparseComposerExtension::~dtkLinearAlgebraSparseComposerExtension(void)
{

}

void dtkLinearAlgebraSparseComposerExtension::extend(dtkComposerNodeFactory *factory)
{
    if (!factory) {
        dtkError() << "No composer factory, can't extend it with dtkLinearAlgebraSparse nodes ";
        return;
    }
    factory->record(":dtkComposer/dtkComposerNodeSparseMatrix.json",       dtkComposerNodeCreator<       dtkComposerNodeSparseMatrix<double> >);
    factory->record(":dtkComposer/dtkComposerNodeSparseMatrixWriter.json", dtkComposerNodeCreator< dtkComposerNodeSparseMatrixWriter<double> >);
    factory->record(":dtkComposer/dtkComposerNodeSparseSolver.json",       dtkComposerNodeCreator<       dtkComposerNodeSparseSolver<double> >);
    factory->record(":dtkComposer/dtkComposerNodeVector.json",             dtkComposerNodeCreator<             dtkComposerNodeVector<double> >);
    factory->record(":dtkComposer/dtkComposerNodeVectorASum.json",         dtkComposerNodeCreator<         dtkComposerNodeVectorASum<double> >);
    factory->record(":dtkComposer/dtkComposerNodeVectorAdd.json",          dtkComposerNodeCreator<          dtkComposerNodeVectorAdd<double> >);
    factory->record(":dtkComposer/dtkComposerNodeVectorSub.json",          dtkComposerNodeCreator<          dtkComposerNodeVectorSub<double> >);
    factory->record(":dtkComposer/dtkComposerNodeVectorWriter.json",       dtkComposerNodeCreator<       dtkComposerNodeVectorWriter<double> >);
}

// 
// dtkLinearAlgebraSparseComposerExtension.cpp ends here
