#pragma once

#include <dtkComposer>

#include <dtkVector>

template <typename T> class dtkComposerNodeVectorWriterPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorWriter : public dtkComposerNodeLeaf
{
public:
     dtkComposerNodeVectorWriter(void);
    ~dtkComposerNodeVectorWriter(void);

public:
    void run(void);

private:
    dtkComposerNodeVectorWriterPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorWriterPrivate
{
public:
    dtkComposerTransmitterReceiver< dtkVector<T> *> rcv_vector;
    dtkComposerTransmitterReceiver<QString>         rcv_filename;

public:
    dtkComposerTransmitterEmitter<bool>             emt_status;
};

template <typename T> inline dtkComposerNodeVectorWriter<T>::dtkComposerNodeVectorWriter(void) : dtkComposerNodeLeaf(), d(new dtkComposerNodeVectorWriterPrivate<T>())
{
    this->appendReceiver(&d->rcv_vector);
    this->appendReceiver(&d->rcv_filename);

    this->appendEmitter(&d->emt_status);
}

template <typename T> inline dtkComposerNodeVectorWriter<T>::~dtkComposerNodeVectorWriter(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeVectorWriter<T>::run(void)
{
    if (!d->rcv_filename.isEmpty() && !d->rcv_vector.isEmpty()) {

        dtkVector<T> *vec = d->rcv_vector.constData();
        QString filename = d->rcv_filename.constData();

        if (vec) {
            bool status = vec->writeMatrixMarket(filename);
            d->emt_status.setData(status);

        } else {
            dtkError() << Q_FUNC_INFO << "Input vector is null. Nothing is done";
            return;
        }

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        return;
    }
}

//
// dtkComposerNodeVectorWriter.h ends here
