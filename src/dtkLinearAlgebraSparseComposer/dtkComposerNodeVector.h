#pragma once

#include <dtkComposer>

#include <dtkVectorAbstractData>
#include <dtkVector>

template <typename T> class dtkComposerNodeVectorPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVector : public dtkComposerNodeObject< dtkVectorAbstractData<T> >
{
public:
     dtkComposerNodeVector(void);
    ~dtkComposerNodeVector(void);

public:
    void run(void);

private:
    dtkComposerNodeVectorPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorPrivate
{
public:
    dtkComposerTransmitterReceiver<QString> rcv_filename;

public:
    dtkComposerTransmitterEmitter< dtkVector<T> *> emt_vector;

public:
    dtkVector<T> *vector;
};

template <typename T> inline dtkComposerNodeVector<T>::dtkComposerNodeVector(void) : dtkComposerNodeObject< dtkVectorAbstractData<T> >(), d(new dtkComposerNodeVectorPrivate<T>())
{
    this->setFactory(dtkLinearAlgebraSparse::vectorData::pluginFactory<T>());

    this->appendReceiver(&d->rcv_filename);

    this->appendEmitter(&d->emt_vector);

    d->vector = 0;
}

template <typename T> inline dtkComposerNodeVector<T>::~dtkComposerNodeVector(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeVector<T>::run(void)
{
    if (!d->rcv_filename.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No vector instantiated, abort:" << this->currentImplementation();
            d->emt_vector.clearData();
            return;
        }

        QString filename = d->rcv_filename.constData();

        dtkVectorAbstractData<T> *data = this->object();
        data->setObjectName(this->currentImplementation());

        if (!d->vector)
            d->vector = new dtkVector<T>(this->object());

        if (d->vector->readMatrixMarket(filename)) {
            d->emt_vector.setData(d->vector);
        } else {
            dtkWarn() << Q_FUNC_INFO << "Error while reading filename"<< filename << ". Nothing is done.";
            d->emt_vector.clearData();
        }

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_vector.clearData();
        return;

    }
}

//
// dtkComposerNodeVector.h ends here
