// Copyright: INRIA

#pragma once

#include <dtkComposer>

#include <dtkVector>

template <typename T> class dtkComposerNodeVectorSubPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorSub : public dtkComposerNodeLeaf
{
public:
     dtkComposerNodeVectorSub(void);
    ~dtkComposerNodeVectorSub(void);

public:
    void run(void);

private:
    dtkComposerNodeVectorSubPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeVectorSubPrivate
{
public:
    dtkComposerTransmitterReceiver< dtkVector<T> *> rcv_vector;
    dtkComposerTransmitterReceiver< dtkVector<T> *> rcv_b;

public:
    dtkComposerTransmitterEmitter< dtkVector<T> * > emt_vector;

public:
    dtkVector<T> *vector;
};

template <typename T> inline dtkComposerNodeVectorSub<T>::dtkComposerNodeVectorSub(void) : d(new dtkComposerNodeVectorSubPrivate<T>())
{
    this->appendReceiver(&d->rcv_vector);
    this->appendReceiver(&d->rcv_b);

    this->appendEmitter(&d->emt_vector);

    d->vector = 0;
}

template <typename T> inline dtkComposerNodeVectorSub<T>::~dtkComposerNodeVectorSub(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeVectorSub<T>::run(void)
{
    if (!d->rcv_vector.isEmpty() && !d->rcv_b.isEmpty()) {

        d->vector = d->rcv_vector.data();
        dtkVector<T> *b = d->rcv_b.constData();

        *(d->vector) -= *b;
        d->emt_vector.setData(d->vector);

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_vector.clearData();
        return;

    }
}

//
// dtkComposerNodeVectorSub.h ends here
