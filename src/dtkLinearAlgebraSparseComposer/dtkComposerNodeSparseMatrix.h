#pragma once

#include <dtkComposer>
#include <dtkSparseMatrix>

template <typename T> class dtkComposerNodeSparseMatrixPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeSparseMatrix : public dtkComposerNodeObject< dtkSparseMatrixEngine<T> >
{
public:
     dtkComposerNodeSparseMatrix(void);
    ~dtkComposerNodeSparseMatrix(void);

public:
    void run(void);

private:
    dtkComposerNodeSparseMatrixPrivate<T> *d;
};

// ///////////////////////////////////////////////////////////////////

template <typename T> class dtkComposerNodeSparseMatrixPrivate
{
public:
    dtkComposerTransmitterReceiver<QString> rcv_filename;

public:
    dtkComposerTransmitterEmitter< dtkSparseMatrix<T> *> emt_matrix;

public:
    dtkSparseMatrix<T> *matrix;
};

template <typename T> inline dtkComposerNodeSparseMatrix<T>::dtkComposerNodeSparseMatrix(void) : dtkComposerNodeObject< dtkSparseMatrixEngine<T> >(), d(new dtkComposerNodeSparseMatrixPrivate<T>())
{
    this->setFactory(dtkLinearAlgebraSparse::matrixEngine::pluginFactory<T>());

    this->appendReceiver(&d->rcv_filename);

    this->appendEmitter(&d->emt_matrix);

    d->matrix = 0;
}

template <typename T> inline dtkComposerNodeSparseMatrix<T>::~dtkComposerNodeSparseMatrix(void)
{
    delete d;

    d = 0;
}

template <typename T> inline void dtkComposerNodeSparseMatrix<T>::run(void)
{
    if (!d->rcv_filename.isEmpty()) {

        if (!this->object()) {
            dtkWarn() << Q_FUNC_INFO << "No matrix instantiated, abort:" << this->currentImplementation();
            d->emt_matrix.clearData();
            return;
        }

        QString filename = d->rcv_filename.constData();

        dtkSparseMatrixEngine<T> *data = this->object();
        data->setObjectName(this->currentImplementation());

        if (d->matrix)
            d->matrix->clear();
        else
            d->matrix = new dtkSparseMatrix<T>(this->object());

        if (d->matrix->readMatrixMarket(filename)) {
            d->emt_matrix.setData(d->matrix);
        } else {
            dtkWarn() << Q_FUNC_INFO << "Error while loading file" << filename << ". Nothing is done.";
            d->emt_matrix.clearData();
        }

    } else {

        dtkWarn() << Q_FUNC_INFO << "The input are not all set. Nothing is done.";
        d->emt_matrix.clearData();
        return;

    }
}

//
// dtkComposerNodeSparseMatrix.h ends here
