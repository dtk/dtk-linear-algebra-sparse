# ChangLog
## 1.3.3 - 2018-03-14
 * fix renaming with SKIP_AUTOMOC done in previous release
## 1.3.2 - 2018-03-14
 * really fix INSTALL_RPATH for composer extension
## 1.3.1 - 2018-03-09
- fix target properties for composer extension plugin
## 1.3.0 - 2018-03-08
 * layer rearchitecturing: split into Core and Composer components
 * fix windows build

## 1.2.2
 * remove obsolete files
 * build fix for windows
