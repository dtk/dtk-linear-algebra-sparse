/* main.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2015 - Nicolas Niclausse, Inria.
 * Created: 2015/03/26 16:28:38
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <QtCore>

#include <dtkDistributed>

#include <dtkLinearAlgebraSparse>
#include <iostream>

class Solver : public QRunnable
{
public:
    QString matrix_file;
    QString sol_ref_file;
    QString sol_vector_file;
    QString rhs_vector_file;
    QString implementation;
    QString solver_implementation;
    int iterations =50;
    double rro= 1.e-6;

public:
    void run(void) {
        QTime timer; timer.restart();

        dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
        QString vectorImpl = "dtkDistributedVectorData";
        //QString matrixImpl = "dtkDistributedSparseMatrixData";
        QString matrixImpl = "dtkDistributedSparseMatrixEngineCSR";
        // if (implementation == "sequential") {
        //     vectorImpl = "dtkVectorData";
        //     matrixImpl = "dtkSparseMatrixData";
        // }

        dtkDebug() << "Implementations:" << vectorImpl << matrixImpl << solver_implementation << "iterations/rro:" << iterations << "/" << rro;
        dtkVector<double> *rhs = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
        dtkVector<double> *sol = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
        if (!rhs->readMatrixMarket(rhs_vector_file)) {
            return;
        }
        sol->resize(rhs->size());
        sol->fill(0.0);

        dtkSparseMatrix<double> *mat = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);
        if (!mat->readMatrixMarket(matrix_file))
            return;

        dtkSparseSolver<double> *solver = dtkLinearAlgebraSparse::solverFactory::create<double>(solver_implementation);
        solver->setMatrix(mat);
        solver->setRHSVector(rhs);
        solver->setSolutionVector(sol);

        QHash<QString, int> parameters_int;
        parameters_int.insert("number_of_iterations", iterations);
        solver->setOptionalParameters(parameters_int);

        QHash<QString, double> parameters_real;
        parameters_real.insert("residual_reduction_order", rro);
        solver->setOptionalParameters(parameters_real);

        if (comm->rank() == 0) {
            qDebug() << "solve system of size" << rhs->size();
            qDebug() << "read duration:" << timer.elapsed() << "ms";
            timer.restart();
        }

        solver->run();
        sol = solver->solutionVector();

        if (comm->rank() == 0) {
            qDebug() << "solver duration:" << timer.elapsed() << "ms";
            qDebug() << "solver done, solution: " ;
            if (!sol_vector_file.isEmpty()) {
                sol->writeMatrixMarket(sol_vector_file);
            }
            qDebug()<<  sol->at(0) << sol->at(1) << sol->at(2)<< sol->at(3) ;
        }
        if (!sol_ref_file.isEmpty()) {
            double residual;
            dtkVector<double> *refsol = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
            if (!refsol->readMatrixMarket(sol_ref_file)) {
                return;
            } else {
                *refsol -= *sol;
                residual = refsol->asum();
                if (comm->rank() == 0) {
                    std::cout << residual << std::endl;
                }
            }
        }

        delete rhs;
        delete mat;
        delete solver;
    }
};

int main(int argc, char **argv)
{
    dtkDistributedApplication *app = dtkDistributed::create(argc, argv);
    app->setApplicationName("dtkSparseMatrixSolver");
    app->setApplicationVersion("1.3.3");


    QCommandLineParser *parser = app->parser();
    parser->setApplicationDescription("DTK sparse matrix solver.");

    // ------------ set options

    QCommandLineOption matrixOption("matrix", "matrix (matrixmarket format)", "file");
    parser->addOption(matrixOption);
    QCommandLineOption rhsOption("rhs", "rhs vector (matrixmarket format)", "file");

    parser->addOption(rhsOption);

    QCommandLineOption solOption("sol", "solution vector to be written", "file");
    parser->addOption(solOption);

    QCommandLineOption refSolOption("refsol", "reference solution", "file");
    parser->addOption(refSolOption);

    QCommandLineOption implOption("impl", "implementation", "sequential|distributed");
    parser->addOption(implOption);

    QCommandLineOption solverImplOption("solver", "solver implementation. ", "jacobi|SGS|...", "jacobi");
    parser->addOption(solverImplOption);

    QCommandLineOption showSolversOption("show-solvers", "show solver implementations available.");
    parser->addOption(showSolversOption);

    QCommandLineOption iterationOption("iterations", "number of iterations", "integer", "50");
    parser->addOption(iterationOption);

    QCommandLineOption rroOption("rro", "residual reduction order", "double", "1.e-6");
    parser->addOption(rroOption);

    app->initialize();

    // ------------ initialize layers

    dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
    linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");
    QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));

    if (parser->isSet(verboseOption)) {
        dtkLinearAlgebraSparse::solver::pluginManager().setVerboseLoading(true);
    }
    dtkLinearAlgebraSparse::pluginManager::initialize(linear_algebra_sparse_settings.value("plugins").toString());
    linear_algebra_sparse_settings.endGroup();

    QStringList solvers = dtkLinearAlgebraSparse::solver::pluginFactory<double>().keys();
    QString solversStr = solvers.filter(QRegExp("^(?!.*Generic).*$")).join(" ");

    if (parser->isSet(showSolversOption)) {
        qDebug() << " default solvers:" << "jacobi" << "SGS";
        qDebug() << " dynamically loaded solvers:" << solversStr;
        exit(0);
    }
    dtkInfo() <<"Dynamicaly loaded solver plugins:" << solversStr;

    // ------------ check parameters

    if (!parser->isSet(matrixOption) || !parser->isSet(rhsOption)) {
        qCritical() << "Error: no matrix or vector set ! Use --matrix <filename> --rhs <filename>" ;
        return 1;
    }

    Solver solver;

    if (parser->isSet(solverImplOption)) {
        if (parser->value(solverImplOption) == "SGS") {
            solver.solver_implementation = "dtkSparseSolverSymmetricGaussSeidelGeneric";
        } else if (parser->value(solverImplOption) == "jacobi") {
            solver.solver_implementation = "dtkSparseSolverJacobiGeneric";
        } else if (parser->value(solverImplOption) == "jor") {
            solver.solver_implementation = "dtkSparseSolverJacobiSORGeneric";
        } else if (solvers.contains(parser->value(solverImplOption))){
            solver.solver_implementation = parser->value(solverImplOption);
        } else {
            qWarning() << "Unknown solver " << parser->value(solverImplOption) << "abort";
            return 1;
        }
    } else {
        solver.solver_implementation = "dtkSparseSolverJacobiGeneric";
    }

    // ------------ set parameters

    solver.matrix_file     = parser->value(matrixOption);
    solver.rhs_vector_file = parser->value(rhsOption);
    solver.sol_vector_file = parser->value(solOption);
    solver.sol_ref_file    = parser->value(refSolOption);
    solver.implementation  = parser->value(implOption);

    if (parser->isSet(iterationOption)) {
        solver.iterations  = parser->value(iterationOption).toInt();
    }

    if (parser->isSet(rroOption)) {
        solver.rro  = parser->value(rroOption).toDouble();
    }

    app->spawn();
    app->exec(&solver);
    app->unspawn();
    return 0;
}
