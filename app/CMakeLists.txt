## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

add_subdirectory(dtkSparseMatrixEditor)

if(DTK_BUILD_DISTRIBUTED)
  add_subdirectory(dtkSparseMatrixSolver)
endif(DTK_BUILD_DISTRIBUTED)

######################################################################
### CMakeLists.txt ends here
