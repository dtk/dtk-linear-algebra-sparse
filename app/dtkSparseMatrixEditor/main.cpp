// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtWidgets>

#include <dtkSparseMatrixEditorMainWindow.h>

int main(int argc, char **argv)
{
    QApplication application(argc, argv);
    application.setApplicationName("dtkSparseMatrixEditor");
    application.setApplicationVersion("1.3.3");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

    dtkSparseMatrixEditorMainWindow main_window;
    main_window.show();
    return application.exec();
}
