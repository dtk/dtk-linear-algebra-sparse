// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtWidgets>

#include <dtkLinearAlgebraSparse>

#include "dtkSparseMatrixEditorMainWindow.h"
#include "dtkSparseMatrixEditorWidget.h"

dtkSparseMatrixEditorMainWindow::dtkSparseMatrixEditorMainWindow()
{
    m_sparse_matrix_editor = new dtkSparseMatrixEditor;
    setCentralWidget(m_sparse_matrix_editor);

    dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
    linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");
    dtkLinearAlgebraSparse::pluginManager::initialize(linear_algebra_sparse_settings.value("plugins").toString());
    linear_algebra_sparse_settings.endGroup();

    m_mat = dtkLinearAlgebraSparse::matrixFactory::create<double>("dtkSparseMatrixData");

    this->setGeometry(0, 0, 800, 800);

    this->createActions();
    this->createMenus();
}

void dtkSparseMatrixEditorMainWindow::createActions()
{
    m_open_action = new QAction(tr("&Open..."), this);
    m_open_action->setShortcuts(QKeySequence::Open);
    m_open_action->setStatusTip(tr("Open an existing file"));
    connect(m_open_action, SIGNAL(triggered()), this, SLOT(open()));
}

void dtkSparseMatrixEditorMainWindow::createMenus()
{
    m_file_menu = this->menuBar()->addMenu(tr("&File"));
    m_file_menu->addAction(m_open_action);
}

void dtkSparseMatrixEditorMainWindow::loadFile(const QString &file_name)
{
    m_mat->clear();
    m_mat->readMatrixMarket(file_name);
    m_mat->assemble();

    m_sparse_matrix_editor->setSparseMatrix(m_mat);
}

void dtkSparseMatrixEditorMainWindow::open()
{
    QString file_name = QFileDialog::getOpenFileName(this);
    if (!file_name.isEmpty())
        this->loadFile(file_name);
}
