// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSparseMatrixEditorWidget.h"

#include <dtkLinearAlgebraSparse>

#include <QtGlobal>

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixEditorCanvas
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixEditorCanvasPrivate
{
public:
    dtkSparseMatrix<double> *matrix;

public:
    int x;
    int y;
};

dtkSparseMatrixEditorCanvas::dtkSparseMatrixEditorCanvas(QWidget *parent) : QWidget(parent), d(new dtkSparseMatrixEditorCanvasPrivate)
{
    d->matrix = NULL;

    d->x = -1;
    d->y = -1;

    this->setMouseTracking(true);
}

dtkSparseMatrixEditorCanvas::~dtkSparseMatrixEditorCanvas(void)
{
    delete d;
}

void dtkSparseMatrixEditorCanvas::setSparseMatrix(dtkSparseMatrix<double> *matrix)
{
    d->matrix = matrix;
}

QSize dtkSparseMatrixEditorCanvas::sizeHint(void)
{
    return QSize(300, 300);
}

void dtkSparseMatrixEditorCanvas::mouseMoveEvent(QMouseEvent *event)
{
    if(!d->matrix)
        return;

    int x = event->pos().x();
    int y = event->pos().y();
    int sampling = d->matrix->rowCount();
    int padding = 10;

    unsigned int frame_width = this->width();
    unsigned int frame_height = this->height();
    unsigned int matrix_width = frame_width - 4 * padding;
    unsigned int matrix_height = frame_height - 4 * padding;

    if (x < 2*padding || x > frame_width-2*padding) {
        d->x = -1;
        d->y = -1;
        this->update();
        return;
    }

    if (y < 2*padding || y > frame_height-2*padding) {
        d->x = -1;
        d->y = -1;
        this->update();
        return;
    }

    int i = (sampling * (y - 2*padding)) / matrix_height;
    int j = (sampling * (x - 2*padding)) / matrix_width;

    // set the lines position
    // d->x = 2*padding + (x - 2 * padding)* j + qreal(frame_width  - 4*padding) / qreal(2*sampling);
    // d->y = 2*padding + (y - 2 * padding)* i + qreal(frame_height - 4*padding) / qreal(2*sampling);
    d->x = 2*padding + (qreal(matrix_width) / qreal(sampling)) * j ;
    d->y = 2*padding + (qreal(matrix_height) / qreal(sampling)) * i;
    // d->x = 2*padding + (qreal(matrix_width) / qreal(sampling)) * j + qreal(frame_width  - 4*padding)/qreal(2*sampling);
    // d->y = 2*padding + (qreal(matrix_height) / qreal(sampling)) * i + qreal(frame_height - 4*padding)/qreal(2*sampling);

    qreal v = d->matrix->at(i, j);

    emit valueChanged(i, j, v);

    this->update();
}

void dtkSparseMatrixEditorCanvas::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.fillRect(event->rect(), Qt::black);
    painter.setRenderHint(QPainter::Antialiasing, false);

    if(!d->matrix)
        return;

    unsigned int frame_width = this->rect().width();
    unsigned int frame_height = this->rect().height();

    unsigned int sampling = d->matrix->rowCount();
    unsigned int padding = 10;

    unsigned int matrix_width = frame_width - 4 * padding;
    unsigned int matrix_height = frame_height - 4 * padding;

    // compute the size in px of one element
    double element_width =  (double)matrix_width / (double)sampling;
    double element_height = (double)matrix_height / (double)sampling;

    // double number_elements_in_pixel_x = 1. / element_width;
    // double number_elements_in_pixel_y = 1. / element_height;

    // number_elements_in_pixel_x = qMax(1., number_elements_in_pixel_x);
    // number_elements_in_pixel_y = qMax(1., number_elements_in_pixel_y);

    // create a dense vizualization matrix and initialize it
    unsigned int **visualization_matrix = new unsigned int*[matrix_height];

    for(size_t i = 0; i < matrix_height; ++i)
        visualization_matrix[i] = new unsigned int[matrix_width];

    for (int i = 0; i < matrix_height; ++i) {
        for (int j = 0; j < matrix_width; ++j) {
            visualization_matrix[i][j] = 0;
        }
    }

    dtkSparseMatrix<double>& mat = *(d->matrix);

    dtkSparseMatrixLine<double> line_it = mat.begin();
    dtkSparseMatrixLine<double> line_end = mat.end();

    for (; line_it != line_end; ++line_it) {
        qlonglong i = line_it.id();

        dtkSparseMatrixLineElement<double> elt_it  = line_it.begin();
        dtkSparseMatrixLineElement<double> elt_end = line_it.end();

        for (;elt_it != elt_end; ++elt_it) {

            qlonglong j = elt_it.id();

            // determine in which pixel we are
            unsigned int id_pixel_x = j * matrix_width / sampling;
            unsigned int id_pixel_y = i * matrix_height / sampling;

            // add the element to the visualization matrix
            visualization_matrix[id_pixel_y][id_pixel_x] += 1;
        }
    }

    // find the highest number of elements in the visualization matrix
    unsigned int max_elements_in_matrix = 0;
    for (int i = 0; i < matrix_height; ++i) {
        for (int j = 0; j < matrix_width; ++j) {
            max_elements_in_matrix = qMax(max_elements_in_matrix, visualization_matrix[i][j]);
        }
    }

    // visualization related
    double rectangle_width = qMax(1., element_width);
    double rectangle_height = qMax(1., element_height);

    // paint pixel by pixel
    for (int i = 0; i < matrix_height; ++i) {
        for (int j = 0; j < matrix_width; ++j) {

            // set the color brush depending on the density
            double rgb = qMin(visualization_matrix[i][j] * 1.4, (double)max_elements_in_matrix) / max_elements_in_matrix * 255;

            painter.setPen(QPen(QColor(rgb, rgb, rgb), 1));
            painter.setBrush(QColor(rgb, rgb, rgb));

            // add padding
            // int x = 2 * padding + rectangle_width  * j;
            // int y = 2 * padding + rectangle_height * i;
            int x = 2 * padding + j;
            int y = 2 * padding + i;

            painter.drawRect(x, y, rectangle_width, rectangle_height);
        }
    }

    // draw left and right lines
    painter.setPen(QPen(QColor("#d0d0d0"), 2));
    painter.drawLine(padding, padding, padding, frame_height-padding);
    painter.drawLine(frame_width-padding, padding, frame_width-padding, frame_height-padding);

    for(size_t i = 0; i < matrix_height; ++i)
        delete visualization_matrix[i];
    delete[] visualization_matrix;

    // paint the cross
    if(d->x < 0 || d->y < 0)
        return;

    painter.setPen(QPen(QColor("#1aeafb"), 1));
    painter.drawLine(0, d->y, frame_width, d->y);
    painter.drawLine(d->x, 0, d->x, frame_height);
}

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixEditor
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixEditorPrivate
{
public:
    QLineEdit *m;
    QLineEdit *n;
    QLineEdit *i;
    QLineEdit *j;
    QLineEdit *v;

public:
    QLabel *t;

public:
    dtkSparseMatrixEditorCanvas *canvas;
};

dtkSparseMatrixEditor::dtkSparseMatrixEditor(QWidget *parent) : QFrame(parent), d(new dtkSparseMatrixEditorPrivate)
{
    d->canvas = new dtkSparseMatrixEditorCanvas(this);

    d->m = new QLineEdit(this);
    d->m->setReadOnly(true);
    d->m->setText(QString::number(0));

    d->n = new QLineEdit(this);
    d->n->setReadOnly(true);
    d->n->setText(QString::number(0));

    d->i = new QLineEdit(this);
    d->i->setReadOnly(true);

    d->j = new QLineEdit(this);
    d->j->setReadOnly(true);

    d->v = new QLineEdit(this);
    d->v->setReadOnly(true);

    d->t = new QLabel("Sparse matrix", this);

    QHBoxLayout *t_layout = new QHBoxLayout;
    t_layout->addWidget(d->t);
    t_layout->addWidget(d->m);
    t_layout->addWidget(d->n);

    QHBoxLayout *b_layout = new QHBoxLayout;
    b_layout->addWidget(d->i);
    b_layout->addWidget(d->j);
    b_layout->addWidget(d->v);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addLayout(t_layout);
    layout->addWidget(d->canvas);
    layout->addLayout(b_layout);

    connect(d->canvas, SIGNAL(valueChanged(qlonglong, qlonglong, qreal)), this, SLOT(onValueChanged(qlonglong, qlonglong, qreal)));
}

dtkSparseMatrixEditor::~dtkSparseMatrixEditor(void)
{
    delete d;
}

void dtkSparseMatrixEditor::setSparseMatrix(dtkSparseMatrix<double> *matrix)
{
    if (!matrix)
        return;

    d->t->setText(matrix->dataType());
    d->m->setText(QString::number(matrix->rowCount()));
    d->n->setText(QString::number(matrix->rowCount()));
    d->canvas->setSparseMatrix(matrix);
    d->i->clear();
    d->j->clear();
    d->v->clear();
}

void dtkSparseMatrixEditor::onValueChanged(qlonglong i, qlonglong j, qreal value)
{
    d->i->setText(QString::number(i));
    d->j->setText(QString::number(j));
    d->v->setText(QString::number(value));
}
