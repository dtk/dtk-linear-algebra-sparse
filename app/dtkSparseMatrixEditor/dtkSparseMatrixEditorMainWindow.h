// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QMainWindow>
#include <QPlainTextEdit>

class dtkSparseMatrixEditor;
template <typename T> class dtkSparseMatrix;

// /////////////////////////////////////////////////////////////////
// dtkSparseMatrixEditorMainWindow
// /////////////////////////////////////////////////////////////////

class dtkSparseMatrixEditorMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    dtkSparseMatrixEditorMainWindow();

private slots:
    void open();

private:
    void createActions();
    void createMenus();
    void loadFile(const QString &fileName);

private:
    QAction *m_open_action;
    QMenu *m_file_menu;

private:
    dtkSparseMatrixEditor *m_sparse_matrix_editor;
    dtkSparseMatrix<double> *m_mat;
};
