// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtWidgets>

template <typename T> class dtkSparseMatrix;

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixEditor
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixEditorPrivate;

class dtkSparseMatrixEditor : public QFrame
{
    Q_OBJECT

public:
     dtkSparseMatrixEditor(QWidget *parent = 0);
    ~dtkSparseMatrixEditor(void);

public:
    void setSparseMatrix(dtkSparseMatrix<double> *matrix);

public slots:
    void onValueChanged(qlonglong, qlonglong, qreal);

private:
    dtkSparseMatrixEditorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// dtkSparseMatrixEditorCanvas
// ///////////////////////////////////////////////////////////////////

class dtkSparseMatrixEditorCanvasPrivate;

class dtkSparseMatrixEditorCanvas : public QWidget
{
    Q_OBJECT

public:
     dtkSparseMatrixEditorCanvas(QWidget *parent = 0);
    ~dtkSparseMatrixEditorCanvas(void);

public:
    void setSparseMatrix(dtkSparseMatrix<double> *matrix);

public:
    QSize sizeHint(void);

signals:
    void valueChanged(qlonglong, qlonglong, qreal);

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    dtkSparseMatrixEditorCanvasPrivate *d;
};
